export const BLANK_PATH = '';
export const ROOT_PATH = '/';
export const CURRENT_PATH = '.';
export const PARENT_PATH = '..';

export const ROUTING = {
  AUTHENTICATION: 'auth',
  MAIN: 'main'
};

export const MAIN_ROUTING = {
  DOCTOR: 'doc',
  MAIN_DOC: 'main-doc',
};

export const MAIN_DOCTOR_ROUTING = {
  DOCTORS: 'doctors',
  REPORTS: 'reports',
};

export const REPORTS_ROUTING = {
  DISEASES: 'diseases',
};

export const DOCTOR_ROUTING = {
  PATIENTS: 'patients',
  HOSPITALIZATION: 'hospitalization',
  ARCHIVE: 'arch',
  ANALYSES: 'analyses',
};

export const PATIENTS_ROUTING = {
  SEARCH: 'search',
  CREATE: 'create',
  EDIT: 'edit',
};

export const HOSPITALIZATION_ROUTING = {
  CREATE: 'create',
  PATIENTS: 'patients',
};

export const ARCHIVE_ROUTING = {
  PATIENTS: 'patients',
};

export const ANALYSES_ROUTING = {
  PATIENTS: 'patients',
};
