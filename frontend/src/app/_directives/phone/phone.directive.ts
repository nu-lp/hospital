import {Directive, Input, OnDestroy, OnInit} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Subscription} from 'rxjs';

@Directive({
  selector: '[appPhone]'
})
export class PhoneDirective implements OnInit, OnDestroy {
  @Input() appPhone: FormControl;
  private changeSubscription: Subscription;
  private readonly PHONE_REGEX = /([0-9]{0,3})([0-9]{0,3})([0-9]{0,2})([0-9]{0,2})/;
  private readonly NON_DIGIT = /[^0-9]+/g;

  constructor() {
  }

  ngOnInit(): void {
    this.changeSubscription = this.appPhone.valueChanges.subscribe(val => this.modelChange(val));
  }

  ngOnDestroy(): void {
    this.changeSubscription.unsubscribe();
  }

  private modelChange(val: string) {
    val = val.replace(this.NON_DIGIT, '');
    const parts = this.PHONE_REGEX.exec(val);
    if (parts[1] !== '') {
      val = `(${parts[1]}`;
    }
    if (parts[2] !== '') {
      val = `${val}) ${parts[2]}`;
    }
    if (parts[3] !== '') {
      val = `${val}-${parts[3]}`;
    }
    if (parts[4] !== '') {
      val = `${val}-${parts[4]}`;
    }
    this.appPhone.setValue(val, {emitEvent: false});
  }
}
