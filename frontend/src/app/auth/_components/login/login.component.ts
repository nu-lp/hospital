import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../_services/auth.service';
import {first} from 'rxjs/operators';
import {Router} from '@angular/router';
import {ROOT_PATH, ROUTING} from '../../../globals';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  form: FormGroup;

  constructor(private authService: AuthService,
              private router: Router,
              private snackBar: MatSnackBar) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      username: new FormControl(null, [
        Validators.required,
        Validators.maxLength(30)
      ]),
      password: new FormControl(null, [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(20)
      ])
    });
  }

  getErrors = (controlName: string) => (this.form.get(controlName) as FormControl).errors;

  onSubmit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }

    this.authService.login(this.form.value.username, this.form.value.password)
      .pipe(first())
      .subscribe(
        user => this.router.navigate([ROOT_PATH, ROUTING.MAIN]),
        error => this.snackBar.open('Невірні дані для входу', 'Ок')
      );
  }

}
