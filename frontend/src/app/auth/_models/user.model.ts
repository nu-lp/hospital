export class User {
  token: string;
  firstName: string;
  middleName: string;
  lastName: string;
  roles: string[];

  constructor(params: User) {
    this.token = params.token;
    this.firstName = params.firstName;
    this.middleName = params.middleName;
    this.lastName = params.lastName;
    this.roles = params.roles;
  }
}
