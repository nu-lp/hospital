import {User} from '../_models/user.model';
import {Injectable} from '@angular/core';
import {Adapter} from '../../_helpers/adapter';

@Injectable({
  providedIn: 'root'
})
export class UserAdapter implements Adapter<User> {
  adapt(data: any): User {
    return {
      token: data.token,
      firstName: data.firstName,
      middleName: data.middleName,
      lastName: data.lastName,
      roles: data.roles,
    };
  }
}
