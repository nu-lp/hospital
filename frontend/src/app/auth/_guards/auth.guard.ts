import {Injectable} from '@angular/core';
import {CanLoad, Route, Router, UrlSegment} from '@angular/router';
import {Observable} from 'rxjs';
import {AuthService} from '../_services/auth.service';
import {ROOT_PATH, ROUTING} from '../../globals';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanLoad {
  constructor(private authService: AuthService,
              private router: Router) {
  }

  private static accessGranted(requiredRoles: string[], userRoles: string[]): boolean {
    for (const role of userRoles) {
      if (requiredRoles.includes(role)) {
        return true;
      }
    }
    return false;
  }

  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
    const user = this.authService.userValue;
    if (user) {
      if (!!route.data && !!route.data.roles) {
        return AuthGuard.accessGranted(route.data.roles, user.roles);
      }
      return true;
    }
    this.router.navigate([ROOT_PATH, ROUTING.AUTHENTICATION]);
    return false;
  }
}
