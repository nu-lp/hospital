import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BLANK_PATH} from '../globals';
import {LoginComponent} from './_components/login/login.component';


const routes: Routes = [
  {
    path: BLANK_PATH,
    component: LoginComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthRoutingModule {
}
