import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {BehaviorSubject, Observable} from 'rxjs';
import {User} from '../_models/user.model';
import {map} from 'rxjs/operators';
import {UserAdapter} from '../_adapters/user-adapter';
import {Router} from '@angular/router';
import {ROOT_PATH, ROUTING} from '../../globals';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private static readonly LOGIN_URL = `${environment.apiUrl}/login`;
  private static readonly USER_KEY = 'currentUser';

  user$: Observable<User>;
  private userSubject$: BehaviorSubject<User>;

  constructor(private http: HttpClient,
              private adapter: UserAdapter,
              private router: Router) {
    this.userSubject$ = new BehaviorSubject<User>(JSON.parse(localStorage.getItem(AuthService.USER_KEY)));
    this.user$ = this.userSubject$.asObservable();
  }

  get userValue(): User {
    return this.userSubject$.getValue();
  }

  login(username: string, password: string): Observable<User> {
    return this.http.post(AuthService.LOGIN_URL, {username, password})
      .pipe(map(user => {
        const model = this.adapter.adapt(user);
        localStorage.setItem(AuthService.USER_KEY, JSON.stringify(model));
        this.userSubject$.next(model);
        return model;
      }));
  }

  logout() {
    localStorage.removeItem(AuthService.USER_KEY);
    this.userSubject$.next(null);
    this.router.navigate([ROOT_PATH, ROUTING.AUTHENTICATION]);
  }
}
