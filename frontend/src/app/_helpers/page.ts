export class Page<T> {
  totalItems: number;
  items: T[];

  constructor(params: Page<T>) {
    this.totalItems = params.totalItems;
    this.items = params.items;
  }
}
