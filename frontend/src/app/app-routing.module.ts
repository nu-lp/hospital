import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BLANK_PATH, ROUTING} from './globals';
import {AuthGuard} from './auth/_guards/auth.guard';


const routes: Routes = [
  {
    path: BLANK_PATH,
    pathMatch: 'full',
    redirectTo: ROUTING.MAIN
  },
  {
    path: ROUTING.AUTHENTICATION,
    loadChildren: () => import('./auth/auth.module').then(m => m.AuthModule)
  },
  {
    path: ROUTING.MAIN,
    loadChildren: () => import('./main/main.module').then(m => m.MainModule),
    canLoad: [AuthGuard]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
