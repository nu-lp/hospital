import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-main',
  template: `
    <app-header></app-header>
    <div style="padding-top: 64px;">
      <router-outlet></router-outlet>
    </div>`,
  styles: []
})
export class MainComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
