import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BLANK_PATH, DOCTOR_ROUTING} from '../../globals';


const routes: Routes = [
  {
    path: BLANK_PATH,
    redirectTo: DOCTOR_ROUTING.HOSPITALIZATION,
    pathMatch: 'full'
  },
  {
    path: DOCTOR_ROUTING.PATIENTS,
    loadChildren: () => import('./patient/patient.module').then(m => m.PatientModule)
  },
  {
    path: DOCTOR_ROUTING.HOSPITALIZATION,
    loadChildren: () => import('./hospitalization/hospitalization.module').then(m => m.HospitalizationModule)
  },
  {
    path: DOCTOR_ROUTING.ARCHIVE,
    loadChildren: () => import('./arch/arch.module').then(m => m.ArchModule)
  },
  {
    path: DOCTOR_ROUTING.ANALYSES,
    loadChildren: () => import('./analyses/analyses.module').then(m => m.AnalysesModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorRoutingModule {
}
