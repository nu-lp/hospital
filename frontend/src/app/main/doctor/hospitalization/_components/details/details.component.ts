import {Component, ElementRef, OnInit, TemplateRef, ViewChild} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Patient} from '../../../_models/patient/patient.model';
import {Hospitalization} from '../../../_models/hospitalization';
import {DiagnosisType} from '../../../_models/diagnosis-type.enum';
import {MatDialog} from '@angular/material/dialog';
import {CreateDiaryDialogComponent} from '../create-diary-dialog/create-diary-dialog.component';
import {ComponentType} from '@angular/cdk/overlay';
import {DiaryService} from '../../../_services/diary.service';
import {DiagnosisService} from '../../../_services/diagnosis.service';
import {CreateDiagnosisDialogComponent} from '../create-diagnosis-dialog/create-diagnosis-dialog.component';
import {AnalysisService} from '../../../_services/analysis.service';
import {CreateAnalysisDialogComponent} from '../create-analysis-dialog/create-analysis-dialog.component';
import {CreateTreatmentDialogComponent} from '../create-treatment-dialog/create-treatment-dialog.component';
import {TreatmentService} from '../../../_services/treatment.service';
import {EpicrisisService} from '../../../_services/epicrisis.service';
import {CreateEpicrisisDialogComponent} from '../create-epicrisis-dialog/create-epicrisis-dialog.component';
import {MatSnackBar} from '@angular/material/snack-bar';
import {HOSPITALIZATION_ROUTING} from '../../../../../globals';
import {HospitalizationService} from '../../../_services/hospitalization.service';

@Component({
    selector: 'app-details',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {
    readonly DIALOG_MIN_WIDTH = '500px';
    patient: Patient;
    hospitalization: Hospitalization;

    @ViewChild('downloadPdfLink') downloadPdfLink: ElementRef;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private dialog: MatDialog,
                private snackBar: MatSnackBar,
                private diaryService: DiaryService,
                private diagnosisService: DiagnosisService,
                private analysisService: AnalysisService,
                private treatmentService: TreatmentService,
                private epicrisisService: EpicrisisService,
                private hospitalizationService: HospitalizationService) {
    }

    get showDiagnosis(): boolean {
        return this.hospitalization.diagnoses.every(d => d.type !== DiagnosisType.Final);
    }

    get showTreatment(): boolean {
        return !this.hospitalization.treatment &&
            this.hospitalization.diagnoses.some(d => d.type === DiagnosisType.Final);
    }

    get showEpicrisis(): boolean {
        return !!this.hospitalization.treatment &&
            this.hospitalization.diagnoses.some(d => d.type === DiagnosisType.Final);
    }

    ngOnInit(): void {
        this.patient = this.route.snapshot.data.patient;
        this.hospitalization = this.route.snapshot.data.hospitalization;
    }

    onDiaryClick() {
        const handler = (result: any) => this.diaryService.create(result).subscribe(
            d => {
                this.hospitalization.diaries.push(d);
                this.openSnackBar('Успішно додано', 'Oк');
            }
        );
        this.openDialog(CreateDiaryDialogComponent, handler);
    }

    onAnalysisClick() {
        const handler = (result: any) => this.analysisService.create(result).subscribe(
            a => {
                this.hospitalization.analyses.push(a);
                this.openSnackBar('Успішно додано', 'Oк');
            }
        );
        this.openDialog(CreateAnalysisDialogComponent, handler);
    }

    onDiagnosisClick() {
        const handler = (result: any) => this.diagnosisService.create(result).subscribe(
            d => {
                this.hospitalization.diagnoses.push(d);
                this.openSnackBar('Успішно додано', 'Oк');
            }
        );
        this.openDialog(CreateDiagnosisDialogComponent, handler);
    }

    onTreatmentClick() {
        const handler = (result: any) => this.treatmentService.create(result).subscribe(
            t => {
                this.hospitalization.treatment = t;
                this.openSnackBar('Успішно додано', 'Oк');
            });
        this.openDialog(CreateTreatmentDialogComponent, handler);
    }

    onEpicrisisClick() {
        const handler = (result: any) => this.epicrisisService.create(result).subscribe(
            e => {
                this.hospitalization.epicrisis = e;
                this.openSnackBar('Успішно виписано', 'Oк');
                this.getPdf(this.hospitalization.uuid);
                this.router.navigate([HOSPITALIZATION_ROUTING.PATIENTS], {relativeTo: this.route.parent});
            }
        );
        this.openDialog(CreateEpicrisisDialogComponent, handler);
    }

    openSnackBar(message: string, action: string) {
        this.snackBar.open(message, action);
    }

    getPdf(uuid: string) {
        this.hospitalizationService.getPdf(uuid).subscribe(data => this.downloadFile(data));
    }

    downloadFile(data: Blob) {
        const blob = new Blob([data], {type: 'application/pdf'});
        const url = window.URL.createObjectURL(blob);
        const link = this.downloadPdfLink.nativeElement;
        link.href = url;
        link.download = 'Історія хвороби.pdf';
        link.click();

        window.URL.revokeObjectURL(url);
    }

    private openDialog(componentOrTemplate: ComponentType<any> | TemplateRef<any>,
                       handler: (result: any) => void) {
        const dialogRef = this.dialog.open(componentOrTemplate, {
            width: '50%',
            minWidth: this.DIALOG_MIN_WIDTH,
            data: this.hospitalization.uuid,
            height: '90%',
        });
        dialogRef.afterClosed().subscribe(result => {
            if (!!result) {
                handler(result);
            }
        });
    }
}
