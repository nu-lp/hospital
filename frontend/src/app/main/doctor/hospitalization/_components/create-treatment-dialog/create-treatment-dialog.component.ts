import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-create-treatment-dialog',
  templateUrl: './create-treatment-dialog.component.html',
  styleUrls: ['./create-treatment-dialog.component.scss']
})
export class CreateTreatmentDialogComponent implements OnInit {
  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<CreateTreatmentDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public uuid: string) {
  }

  getErrors = (controlName: string) => this.form.get(controlName).errors;

  ngOnInit(): void {
    this.form = new FormGroup({
      hospitalizationUuid: new FormControl(this.uuid, Validators.required),
      prescribed: new FormControl(null, [Validators.required, Validators.maxLength(500)]),
      medicines: new FormControl(null, [Validators.maxLength(500)])
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    this.dialogRef.close(this.form.value);
  }
}
