import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  @Input() showTreatment: boolean;
  @Input() showDiagnosis: boolean;
  @Input() showEpicrisis: boolean;

  @Output() diaryClick = new EventEmitter();
  @Output() analysisClick = new EventEmitter();
  @Output() diagnosisClick = new EventEmitter();
  @Output() treatmentClick = new EventEmitter();
  @Output() epicrisisClick = new EventEmitter();

  constructor() {
  }

  ngOnInit(): void {
  }

}
