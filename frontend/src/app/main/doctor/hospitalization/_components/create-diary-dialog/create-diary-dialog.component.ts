import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {Condition, CONDITIONS} from '../../../_models/general-examination/condition.enum';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-create-diary-dialog',
  templateUrl: './create-diary-dialog.component.html',
  styleUrls: ['./create-diary-dialog.component.scss']
})
export class CreateDiaryDialogComponent implements OnInit, OnDestroy {
  readonly BLOOD_PRESSURE_PATTERN = /^([0-9]{1,3})\/([0-9]{1,3})$/m;
  form: FormGroup;
  bloodPressure: FormControl;
  CONDITIONS = Array.from(CONDITIONS);
  private bloodPressureSubscription: Subscription;

  constructor(public dialogRef: MatDialogRef<CreateDiaryDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public uuid: string) {
  }

  getErrors = (controlName: string) => this.form.get(controlName).errors;
  getArray = (controlName: string) => this.form.get(controlName) as FormArray;

  ngOnInit(): void {
    this.form = new FormGroup({
      hospitalizationUuid: new FormControl(this.uuid, Validators.required),
      condition: new FormControl(Condition.Satisfactory, Validators.required),
      temperature: new FormControl(null, Validators.min(30)),
      respiratoryRate: new FormControl(null, Validators.min(0)),
      heartRate: new FormControl(null, Validators.min(0)),
      bloodPressureDia: new FormControl(null, Validators.min(0)),
      bloodPressureSys: new FormControl(null, Validators.min(0)),
      complaints: new FormControl(null, Validators.maxLength(300)),
      examination: new FormControl(null, Validators.maxLength(500)),
      treatments: new FormArray([])
    });
    this.bloodPressure = new FormControl(null, Validators.pattern(this.BLOOD_PRESSURE_PATTERN));

    this.bloodPressureSubscription = this.bloodPressure.valueChanges.subscribe(val => {
      const r = this.BLOOD_PRESSURE_PATTERN.exec(val);
      if (!!r) {
        this.form.get('bloodPressureDia').setValue(r[1]);
        this.form.get('bloodPressureSys').setValue(r[2]);
      }
    });
  }

  ngOnDestroy(): void {
    this.bloodPressureSubscription.unsubscribe();
  }

  onSubmit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      this.bloodPressure.markAsTouched();
      return;
    }
    this.dialogRef.close(this.form.value);
  }

  addTreatment() {
    this.getArray('treatments').push(
      new FormControl(null, [Validators.required, Validators.maxLength(50)])
    );
  }

  removeTreatment() {
    const arr = this.getArray('treatments');
    arr.removeAt(arr.length - 1);
  }
}
