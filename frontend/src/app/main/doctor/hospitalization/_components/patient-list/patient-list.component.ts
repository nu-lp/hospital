import {Component, OnInit} from '@angular/core';
import {Patient} from '../../../_models/patient/patient.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.scss']
})
export class PatientListComponent implements OnInit {
  patients: Patient[];

  constructor(private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit(): void {
    this.patients = this.route.snapshot.data.patients;
  }

  onPatientClick(uuid: string) {
    this.router.navigate([uuid], {relativeTo: this.route.parent});
  }
}
