import {Component, OnInit, ViewChild} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {Patient} from '../../../_models/patient/patient.model';
import * as moment from 'moment';
import {Moment} from 'moment';
import {HospitalizationType} from '../../../_models/hospitalization-type.enum';
import {ActivatedRoute, Router} from '@angular/router';
import {MatDatepicker} from '@angular/material/datepicker';
import {HospitalizationService} from '../../../_services/hospitalization.service';
import {HOSPITALIZATION_ROUTING} from '../../../../../globals';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  @ViewChild('hospitalizationDateId', {static: true}) hospitalizationDatePicker: MatDatepicker<Moment>;
  @ViewChild('diseaseDateId', {static: true}) diseaseDatePicker: MatDatepicker<Moment>;

  form: FormGroup;
  generalDataControl: FormArray;

  patient: Patient;
  today = moment();

  readonly TYPES = {
    planned: HospitalizationType.Planned,
    urgent: HospitalizationType.Urgent
  };

  constructor(private route: ActivatedRoute,
              private service: HospitalizationService,
              private router: Router,
              private snackBar: MatSnackBar) {
  }

  private static buildForm(patientUuid: string): FormGroup {
    return new FormGroup({
      patientUuid: new FormControl(patientUuid, Validators.required),
      hospitalizationDate: new FormControl(moment(), Validators.required),
      diseaseDate: new FormControl(null, Validators.required),
      employment: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
      type: new FormControl(HospitalizationType.Planned, Validators.required),
      referral: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
      admittanceDiagnosis: new FormGroup({}),
      complaints: new FormArray([]),
      diseaseAnamnesis: new FormGroup({}),
      lifeAnamnesis: new FormGroup({}),
      epidemicAnamnesis: new FormGroup({}),
      allergicAnamnesis: new FormGroup({}),
      generalExamination: new FormGroup({}),
      detailedExaminations: new FormArray([]),
      preliminaryDiagnosis: new FormGroup({}),
      analyses: new FormArray([])
    });
  }

  getControl = (controlName: string) => this.form.get(controlName) as FormControl;
  getGroup = (controlName: string) => this.form.get(controlName) as FormGroup;
  getArray = (controlName: string) => this.form.get(controlName) as FormArray;
  getErrors = (controlName: string) => this.form.get(controlName).errors;

  ngOnInit(): void {
    this.patient = this.route.snapshot.data.patient;
    this.form = CreateComponent.buildForm(this.patient.uuid);
    this.buildGeneralDataControl();
  }

  onSubmit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    this.service.create(this.form.value).subscribe(
      () => this.router.navigate([HOSPITALIZATION_ROUTING.PATIENTS], {relativeTo: this.route.parent}),
      error => {
        this.snackBar.open('Пацієнт госпіталізований на даний момент', 'Ок');
      }
    );
  }

  private buildGeneralDataControl() {
    this.generalDataControl = new FormArray([
      this.getControl('patientUuid'),
      this.getControl('hospitalizationDate'),
      this.getControl('diseaseDate'),
      this.getControl('employment'),
      this.getControl('type'),
      this.getControl('referral')
    ]);
  }

}
