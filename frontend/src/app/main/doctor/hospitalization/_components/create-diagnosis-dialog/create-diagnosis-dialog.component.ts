import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-create-diagnosis-dialog',
  templateUrl: './create-diagnosis-dialog.component.html',
  styleUrls: ['./create-diagnosis-dialog.component.scss']
})
export class CreateDiagnosisDialogComponent implements OnInit {
  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<CreateDiagnosisDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public uuid: string) {
  }

  ngOnInit(): void {
    this.form = new FormGroup({
      hospitalizationUuid: new FormControl(this.uuid, Validators.required)
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    this.dialogRef.close(this.form.value);
  }

}
