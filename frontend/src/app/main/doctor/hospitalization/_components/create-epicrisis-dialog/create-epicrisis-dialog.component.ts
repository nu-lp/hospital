import {Component, Inject, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {EPICRISIS_TYPES} from '../../../_models/epicrisis-type.enum';
import {CONDITIONS} from '../../../_models/general-examination/condition.enum';

@Component({
  selector: 'app-create-epicrisis-dialog',
  templateUrl: './create-epicrisis-dialog.component.html',
  styleUrls: ['./create-epicrisis-dialog.component.scss']
})
export class CreateEpicrisisDialogComponent implements OnInit {
  readonly TYPES = Array.from(EPICRISIS_TYPES);
  readonly CONDITIONS = Array.from(CONDITIONS);
  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<CreateEpicrisisDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public uuid: string) {
  }

  getErrors = (controlName: string) => this.form.get(controlName).errors;

  ngOnInit(): void {
    this.form = new FormGroup({
      hospitalizationUuid: new FormControl(this.uuid, Validators.required),
      type: new FormControl(null, Validators.required),
      condition: new FormControl(null, Validators.required),
      recommendation: new FormControl(null, [Validators.required, Validators.maxLength(500)])
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    this.dialogRef.close(this.form.value);
  }
}
