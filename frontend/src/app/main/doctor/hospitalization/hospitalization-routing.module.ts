import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BLANK_PATH, HOSPITALIZATION_ROUTING} from '../../../globals';
import {CreateComponent} from './_components/create/create.component';
import {PatientResolver} from '../_resolvers/patient.resolver';
import {PatientListComponent} from './_components/patient-list/patient-list.component';
import {DoctorsPatientsResolver} from '../_resolvers/doctors-patients.resolver';
import {DetailsComponent} from './_components/details/details.component';
import {HospitalizationResolver} from '../_resolvers/hospitalization.resolver';


const routes: Routes = [
  {
    path: BLANK_PATH,
    redirectTo: HOSPITALIZATION_ROUTING.PATIENTS,
    pathMatch: 'full'
  },
  {
    path: `:id/${HOSPITALIZATION_ROUTING.CREATE}`,
    component: CreateComponent,
    resolve: {patient: PatientResolver}
  },
  {
    path: HOSPITALIZATION_ROUTING.PATIENTS,
    component: PatientListComponent,
    resolve: {patients: DoctorsPatientsResolver}
  },
  {
    path: ':id',
    component: DetailsComponent,
    resolve: {
      patient: PatientResolver,
      hospitalization: HospitalizationResolver
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HospitalizationRoutingModule {
}
