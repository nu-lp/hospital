import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {HospitalizationRoutingModule} from './hospitalization-routing.module';
import {CreateComponent} from './_components/create/create.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatStepperModule} from '@angular/material/stepper';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatInputModule} from '@angular/material/input';
import {MatRadioModule} from '@angular/material/radio';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatButtonModule} from '@angular/material/button';
import {CreateDiagnosisModule} from '../_components/create-diagnosis/create-diagnosis.module';
import {CreateComplaintsModule} from '../_components/create-complaints/create-complaints.module';
import {CreateAnamnesisModule} from '../_components/create-anamnesis/create-anamnesis.module';
import {CreateGeneralExaminationModule} from '../_components/create-general-examination/create-general-examination.module';
import {CreateDetailedExaminationModule} from '../_components/create-detailed-examination/create-detailed-examination.module';
import {CreateAnalysisModule} from '../_components/create-analysis/create-analysis.module';
import {PatientInfoModule} from '../_components/patient-info/patient-info.module';
import {PatientListComponent} from './_components/patient-list/patient-list.component';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {MatChipsModule} from '@angular/material/chips';
import {DetailsComponent} from './_components/details/details.component';
import {MatTabsModule} from '@angular/material/tabs';
import {HospitalizationInfoModule} from '../_components/hospitalization-info/hospitalization-info.module';
import {AnamnesisInfoModule} from '../_components/anamnesis-info/anamnesis-info.module';
import {GeneralExaminationInfoModule} from '../_components/general-examination-info/general-examination-info.module';
import {DetailedExaminationInfoModule} from '../_components/detailed-examination-info/detailed-examination-info.module';
import {DiaryInfoModule} from '../_components/diary-info/diary-info.module';
import {AnalysisInfoModule} from '../_components/analysis-info/analysis-info.module';
import {DiagnosisInfoModule} from '../_components/diagnosis-info/diagnosis-info.module';
import {TreatmentInfoModule} from '../_components/treatment-info/treatment-info.module';
import {ToolbarComponent} from './_components/toolbar/toolbar.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {CreateDiaryDialogComponent} from './_components/create-diary-dialog/create-diary-dialog.component';
import {MatDialogModule} from '@angular/material/dialog';
import {CreateDiagnosisDialogComponent} from './_components/create-diagnosis-dialog/create-diagnosis-dialog.component';
import {CreateAnalysisDialogComponent} from './_components/create-analysis-dialog/create-analysis-dialog.component';
import {CreateTreatmentDialogComponent} from './_components/create-treatment-dialog/create-treatment-dialog.component';
import {CreateEpicrisisDialogComponent} from './_components/create-epicrisis-dialog/create-epicrisis-dialog.component';


@NgModule({
  declarations: [
    CreateComponent,
    PatientListComponent,
    DetailsComponent,
    ToolbarComponent,
    CreateDiaryDialogComponent,
    CreateDiagnosisDialogComponent,
    CreateAnalysisDialogComponent,
    CreateTreatmentDialogComponent,
    CreateEpicrisisDialogComponent,
  ],
  imports: [
    CommonModule,
    HospitalizationRoutingModule,
    ReactiveFormsModule,
    MatStepperModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatInputModule,
    MatRadioModule,
    MatAutocompleteModule,
    MatButtonModule,
    CreateDiagnosisModule,
    CreateComplaintsModule,
    CreateAnamnesisModule,
    CreateGeneralExaminationModule,
    CreateDetailedExaminationModule,
    CreateAnalysisModule,
    PatientInfoModule,
    MatListModule,
    MatIconModule,
    MatChipsModule,
    MatTabsModule,
    HospitalizationInfoModule,
    AnamnesisInfoModule,
    GeneralExaminationInfoModule,
    DetailedExaminationInfoModule,
    DiaryInfoModule,
    AnalysisInfoModule,
    DiagnosisInfoModule,
    TreatmentInfoModule,
    MatToolbarModule,
    MatTooltipModule,
    MatDialogModule,
  ]
})
export class HospitalizationModule {
}
