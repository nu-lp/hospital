import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {AnalysesRoutingModule} from './analyses-routing.module';
import {PatientsSearchComponent} from './_components/patients-search/patients-search.component';
import {SearchBarModule} from '../_components/search-bar/search-bar.module';
import {PatientInfoModule} from '../_components/patient-info/patient-info.module';
import {MatButtonModule} from '@angular/material/button';
import {AnalysisListComponent} from './_components/analysis-list/analysis-list.component';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';
import {CreateAnalysisResultDialogComponent} from './_components/create-analysis-result-dialog/create-analysis-result-dialog.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDialogModule} from '@angular/material/dialog';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';


@NgModule({
  declarations: [PatientsSearchComponent, AnalysisListComponent, CreateAnalysisResultDialogComponent],
  imports: [
    CommonModule,
    AnalysesRoutingModule,
    SearchBarModule,
    PatientInfoModule,
    MatButtonModule,
    MatListModule,
    MatIconModule,
    ReactiveFormsModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule
  ]
})
export class AnalysesModule {
}
