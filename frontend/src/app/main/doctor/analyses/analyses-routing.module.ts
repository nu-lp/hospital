import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ANALYSES_ROUTING, BLANK_PATH} from '../../../globals';
import {PatientsSearchComponent} from './_components/patients-search/patients-search.component';
import {AnalysisListComponent} from './_components/analysis-list/analysis-list.component';
import {AnalysisListResolver} from '../_resolvers/analysis-list.resolver';


const routes: Routes = [
  {
    path: BLANK_PATH,
    pathMatch: 'full',
    redirectTo: ANALYSES_ROUTING.PATIENTS
  },
  {
    path: ANALYSES_ROUTING.PATIENTS,
    component: PatientsSearchComponent
  },
  {
    path: `${ANALYSES_ROUTING.PATIENTS}/:id`,
    component: AnalysisListComponent,
    resolve: {analyses: AnalysisListResolver}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnalysesRoutingModule {
}
