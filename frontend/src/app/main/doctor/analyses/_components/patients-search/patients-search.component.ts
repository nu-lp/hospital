import {Component, OnInit} from '@angular/core';
import {Patient} from '../../../_models/patient/patient.model';

@Component({
  selector: 'app-patients-search',
  templateUrl: './patients-search.component.html',
  styleUrls: ['./patients-search.component.scss']
})
export class PatientsSearchComponent implements OnInit {
  patient: Patient;

  constructor() {
  }

  ngOnInit(): void {
  }

}
