import {Component, OnInit} from '@angular/core';
import {Analysis} from '../../../_models/analysis';
import {ActivatedRoute} from '@angular/router';
import {FormGroup} from '@angular/forms';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CreateAnalysisResultDialogComponent} from '../create-analysis-result-dialog/create-analysis-result-dialog.component';
import {AnalysisService} from '../../../_services/analysis.service';

@Component({
  selector: 'app-analysis-list',
  templateUrl: './analysis-list.component.html',
  styleUrls: ['./analysis-list.component.scss']
})
export class AnalysisListComponent implements OnInit {
  readonly DIALOG_MIN_WIDTH = '500px';
  analyses: Analysis[];
  form: FormGroup;

  constructor(private route: ActivatedRoute,
              private dialog: MatDialog,
              private snackBar: MatSnackBar,
              private service: AnalysisService) {
  }

  ngOnInit(): void {
    this.analyses = this.route.snapshot.data.analyses;
  }

  addResult(index: number) {
    const dialogRef = this.dialog.open(CreateAnalysisResultDialogComponent, {
      width: '50%',
      minWidth: this.DIALOG_MIN_WIDTH,
      data: {uuid: this.analyses[index].uuid, type: this.analyses[index].type},
      height: '90%',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!!result) {
        this.service.addResult(result).subscribe(analysis => {
          this.snackBar.open('Результат було успішно додано', 'Ок');
          this.analyses = this.analyses.filter(a => a.uuid !== analysis.uuid);
        });
      }
    });
  }

}
