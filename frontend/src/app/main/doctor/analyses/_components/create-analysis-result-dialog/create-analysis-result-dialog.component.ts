import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {FormControl, FormGroup, Validators} from '@angular/forms';

interface AnalysisData {
  uuid: string;
  type: string;
}

@Component({
  selector: 'app-create-analysis-result-dialog',
  templateUrl: './create-analysis-result-dialog.component.html',
  styleUrls: ['./create-analysis-result-dialog.component.scss']
})
export class CreateAnalysisResultDialogComponent implements OnInit {
  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<CreateAnalysisResultDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public analysis: AnalysisData) {
  }

  getErrors = (controlName: string) => this.form.get(controlName).errors;

  ngOnInit(): void {
    this.form = new FormGroup({
      analysisUuid: new FormControl(this.analysis.uuid, Validators.required),
      description: new FormControl(null, [Validators.required, Validators.maxLength(500)]),
      conclusion: new FormControl(null, [Validators.required, Validators.maxLength(150)]),
    });
  }

  onSubmit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    this.dialogRef.close(this.form.value);
  }
}
