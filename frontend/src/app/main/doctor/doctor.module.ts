import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DoctorRoutingModule} from './doctor-routing.module';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {AddressAdapter} from './_adapters/patient/address-adapter';
import {PatientAdapter} from './_adapters/patient/patient-adapter';
import {PatientService} from './_services/patient.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {PatientResolver} from './_resolvers/patient.resolver';
import {MatStepperModule} from '@angular/material/stepper';
import {DoctorAdapter} from './_adapters/doctor-adapter';
import {ComplaintAdapter} from './_adapters/complaint-adapter';
import {DiagnosisAdapter} from './_adapters/diagnosis-adapter';
import {AnamnesisAdapter} from './_adapters/anamnesis-adapter';
import {GeneralExaminationAdapter} from './_adapters/general-examination-adapter';
import {DetailedExaminationAdapter} from './_adapters/detailed-examination-adapter';
import {AnalysisAdapter} from './_adapters/analysis-adapter';
import {HospitalizationAdapter} from './_adapters/hospitalization-adapter';
import {HospitalizationService} from './_services/hospitalization.service';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatRadioModule} from '@angular/material/radio';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {TreatmentAdapter} from './_adapters/treatment-adapter';
import {DiaryAdapter} from './_adapters/diary-adapter';
import {EpicrisisAdapter} from './_adapters/epicrisis-adapter';
import {AnalysisResultAdapter} from './_adapters/analysis-result-adapter';
import {DoctorsPatientsResolver} from './_resolvers/doctors-patients.resolver';
import {HospitalizationResolver} from './_resolvers/hospitalization.resolver';
import {DiaryService} from './_services/diary.service';
import {DiagnosisService} from './_services/diagnosis.service';
import {TreatmentService} from './_services/treatment.service';
import {EpicrisisService} from './_services/epicrisis.service';
import {HospitalizationListResolver} from './_resolvers/hospitalization-list.resolver';
import {AnalysisListResolver} from './_resolvers/analysis-list.resolver';
import {AnalysisService} from './_services/analysis.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DoctorRoutingModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    FormsModule,
    MatStepperModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatDividerModule,
    MatButtonModule,
    MatRadioModule,
    MatDatepickerModule,
  ],
  providers: [
    AddressAdapter,
    PatientAdapter,
    PatientService,
    PatientResolver,
    DoctorAdapter,
    ComplaintAdapter,
    DiagnosisAdapter,
    AnamnesisAdapter,
    GeneralExaminationAdapter,
    DetailedExaminationAdapter,
    AnalysisAdapter,
    HospitalizationAdapter,
    HospitalizationService,
    TreatmentAdapter,
    DiaryAdapter,
    EpicrisisAdapter,
    AnalysisResultAdapter,
    DoctorsPatientsResolver,
    HospitalizationResolver,
    DiaryService,
    DiagnosisService,
    TreatmentService,
    EpicrisisService,
    HospitalizationListResolver,
    AnalysisListResolver,
    AnalysisService,
  ]
})
export class DoctorModule {
}
