import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ARCHIVE_ROUTING, BLANK_PATH} from '../../../globals';
import {SearchPatientComponent} from './_components/search-patient/search-patient.component';
import {HospitalizationListComponent} from './_components/hospitalization-list/hospitalization-list.component';
import {PatientResolver} from '../_resolvers/patient.resolver';
import {HospitalizationListResolver} from '../_resolvers/hospitalization-list.resolver';


const routes: Routes = [
  {
    path: BLANK_PATH,
    pathMatch: 'full',
    redirectTo: ARCHIVE_ROUTING.PATIENTS
  },
  {
    path: ARCHIVE_ROUTING.PATIENTS,
    component: SearchPatientComponent
  },
  {
    path: `${ARCHIVE_ROUTING.PATIENTS}/:id`,
    component: HospitalizationListComponent,
    resolve: {patient: PatientResolver, hospitalizations: HospitalizationListResolver},
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArchRoutingModule {
}
