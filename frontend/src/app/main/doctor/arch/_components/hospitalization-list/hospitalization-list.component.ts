import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {Hospitalization} from '../../../_models/hospitalization';
import {ActivatedRoute} from '@angular/router';
import {DiagnosisType} from '../../../_models/diagnosis-type.enum';
import {Patient} from '../../../_models/patient/patient.model';
import {EPICRISIS_TYPES} from '../../../_models/epicrisis-type.enum';
import {CONDITIONS} from '../../../_models/general-examination/condition.enum';
import {HospitalizationService} from '../../../_services/hospitalization.service';

@Component({
    selector: 'app-hospitalization-list',
    templateUrl: './hospitalization-list.component.html',
    styleUrls: ['./hospitalization-list.component.scss']
})
export class HospitalizationListComponent implements OnInit {
    readonly EPICRISES = EPICRISIS_TYPES;
    readonly CONDITIONS = CONDITIONS;

    @ViewChild('downloadPdfLink') downloadPdfLink: ElementRef;

    hospitalizations: Hospitalization[];
    patient: Patient;

    constructor(private route: ActivatedRoute,
                private service: HospitalizationService) {
    }

    public getDisease(h: Hospitalization): string {
        const diag = h.diagnoses.find(d => d.type === DiagnosisType.Final);
        return !!diag ? diag.mainDisease : '';
    }

    ngOnInit(): void {
        this.hospitalizations = this.route.snapshot.data.hospitalizations;
        this.patient = this.route.snapshot.data.patient;
    }

    getPdf(uuid: string) {
        this.service.getPdf(uuid).subscribe(data => this.downloadFile(data));
    }

    downloadFile(data: Blob) {
        const blob = new Blob([data], {type: 'application/pdf'});
        const url = window.URL.createObjectURL(blob);
        const link = this.downloadPdfLink.nativeElement;
        link.href = url;
        link.download = 'Історія хвороби.pdf';
        link.click();

        window.URL.revokeObjectURL(url);
    }
}
