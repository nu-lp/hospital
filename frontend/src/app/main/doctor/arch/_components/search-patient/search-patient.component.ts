import {Component, OnInit} from '@angular/core';
import {Patient} from '../../../_models/patient/patient.model';

@Component({
  selector: 'app-search-patient',
  templateUrl: './search-patient.component.html',
  styleUrls: ['./search-patient.component.scss']
})
export class SearchPatientComponent implements OnInit {
  patient: Patient;

  constructor() {
  }

  ngOnInit(): void {
  }

}
