import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ArchRoutingModule} from './arch-routing.module';
import {SearchPatientComponent} from './_components/search-patient/search-patient.component';
import {SearchBarModule} from '../_components/search-bar/search-bar.module';
import {PatientInfoModule} from '../_components/patient-info/patient-info.module';
import {MatButtonModule} from '@angular/material/button';
import {HospitalizationListComponent} from './_components/hospitalization-list/hospitalization-list.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {HospitalizationInfoModule} from '../_components/hospitalization-info/hospitalization-info.module';
import {MatDividerModule} from '@angular/material/divider';
import {AnamnesisInfoModule} from '../_components/anamnesis-info/anamnesis-info.module';
import {GeneralExaminationInfoModule} from '../_components/general-examination-info/general-examination-info.module';
import {DetailedExaminationInfoModule} from '../_components/detailed-examination-info/detailed-examination-info.module';
import {AnalysisInfoModule} from '../_components/analysis-info/analysis-info.module';
import {DiaryInfoModule} from '../_components/diary-info/diary-info.module';
import {DiagnosisInfoModule} from '../_components/diagnosis-info/diagnosis-info.module';
import {TreatmentInfoModule} from '../_components/treatment-info/treatment-info.module';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [
    SearchPatientComponent,
    HospitalizationListComponent,
  ],
  imports: [
    CommonModule,
    ArchRoutingModule,
    SearchBarModule,
    PatientInfoModule,
    MatButtonModule,
    MatExpansionModule,
    HospitalizationInfoModule,
    MatDividerModule,
    AnamnesisInfoModule,
    GeneralExaminationInfoModule,
    DetailedExaminationInfoModule,
    AnalysisInfoModule,
    DiaryInfoModule,
    DiagnosisInfoModule,
    TreatmentInfoModule,
    MatIconModule
  ]
})
export class ArchModule {
}
