import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Hospitalization} from '../_models/hospitalization';
import {HospitalizationService} from '../_services/hospitalization.service';
import {Observable} from 'rxjs';

@Injectable()
export class HospitalizationListResolver implements Resolve<Hospitalization[]> {
  constructor(private service: HospitalizationService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<Hospitalization[]> | Promise<Hospitalization[]> | Hospitalization[] {
    const patientUuid = route.paramMap.get('id');

    return this.service.getAllByPatientUuid(patientUuid);
  }
}
