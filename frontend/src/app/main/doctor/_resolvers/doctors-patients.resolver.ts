import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Patient} from '../_models/patient/patient.model';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {PatientService} from '../_services/patient.service';

@Injectable()
export class DoctorsPatientsResolver implements Resolve<Patient[]> {

  constructor(private service: PatientService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Patient[]> | Promise<Patient[]> | Patient[] {
    return this.service.getDoctorsPatients();
  }
}
