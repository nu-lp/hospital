import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Patient} from '../_models/patient/patient.model';
import {Observable} from 'rxjs';
import {PatientService} from '../_services/patient.service';

@Injectable()
export class PatientResolver implements Resolve<Patient> {

  constructor(private service: PatientService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Patient> | Promise<Patient> | Patient {
    const uuid = route.paramMap.get('id');
    return this.service.getByUuid(uuid);
  }
}
