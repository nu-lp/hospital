import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {Analysis} from '../_models/analysis';
import {AnalysisService} from '../_services/analysis.service';

@Injectable()
export class AnalysisListResolver implements Resolve<Analysis[]> {
  constructor(private service: AnalysisService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<Analysis[]> | Promise<Analysis[]> | Analysis[] {
    const patientUuid = route.paramMap.get('id');


    return this.service.getAll(patientUuid);
  }
}
