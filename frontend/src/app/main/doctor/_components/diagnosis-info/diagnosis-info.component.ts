import {Component, Input, OnInit} from '@angular/core';
import {Diagnosis} from '../../_models/diagnosis';
import {DIAGNOSIS_TYPES} from '../../_models/diagnosis-type.enum';

@Component({
  selector: 'app-diagnosis-info',
  templateUrl: './diagnosis-info.component.html',
  styleUrls: ['./diagnosis-info.component.scss']
})
export class DiagnosisInfoComponent implements OnInit {
  readonly TYPES = DIAGNOSIS_TYPES;
  @Input() info: Diagnosis;

  constructor() {
  }

  ngOnInit(): void {
  }

}
