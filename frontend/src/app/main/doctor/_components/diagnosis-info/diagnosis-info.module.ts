import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DiagnosisInfoComponent} from './diagnosis-info.component';


@NgModule({
  declarations: [DiagnosisInfoComponent],
  imports: [CommonModule],
  exports: [DiagnosisInfoComponent]
})
export class DiagnosisInfoModule {
}
