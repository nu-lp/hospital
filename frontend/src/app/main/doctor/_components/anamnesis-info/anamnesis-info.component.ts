import {Component, Input, OnInit} from '@angular/core';
import {Anamnesis} from '../../_models/anamnesis';
import {ANAMNESIS_TYPES} from '../../_models/anamnesis-type.enum';

@Component({
  selector: 'app-anamnesis-info',
  templateUrl: './anamnesis-info.component.html',
  styleUrls: ['./anamnesis-info.component.scss']
})
export class AnamnesisInfoComponent implements OnInit {
  readonly TYPES = ANAMNESIS_TYPES;
  @Input() anamnesis: Anamnesis;

  constructor() {
  }

  ngOnInit(): void {
  }

}
