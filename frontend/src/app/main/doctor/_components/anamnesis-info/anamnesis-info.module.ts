import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AnamnesisInfoComponent} from './anamnesis-info.component';


@NgModule({
  declarations: [AnamnesisInfoComponent],
  imports: [CommonModule],
  exports: [AnamnesisInfoComponent]
})
export class AnamnesisInfoModule {
}
