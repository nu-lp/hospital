import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateGeneralExaminationComponent} from './create-general-examination.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatRadioModule} from '@angular/material/radio';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';


@NgModule({
  declarations: [
    CreateGeneralExaminationComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatRadioModule,
    MatFormFieldModule,
    MatInputModule
  ],
  exports: [
    CreateGeneralExaminationComponent
  ]
})
export class CreateGeneralExaminationModule {
}
