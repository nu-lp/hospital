import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormControl, FormGroup, Validators} from '@angular/forms';
import {CONDITIONS} from '../../_models/general-examination/condition.enum';
import {CONSCIOUSNESSES} from '../../_models/general-examination/consciousness.enum';
import {CONSTITUIONS} from '../../_models/general-examination/constitution.enum';
import {TEMPERATURES} from '../../_models/general-examination/temperature.enum';
import {TONES} from '../../_models/general-examination/tone.enum';

@Component({
  selector: 'app-create-general-examination',
  templateUrl: './create-general-examination.component.html',
  styleUrls: ['./create-general-examination.component.scss']
})
export class CreateGeneralExaminationComponent implements OnInit {
  readonly CONDITIONS = Array.from(CONDITIONS);
  readonly CONSCIOUSNESSES = Array.from(CONSCIOUSNESSES);
  readonly CONSTITUTIONS = Array.from(CONSTITUIONS);
  readonly TEMPERATURES = Array.from(TEMPERATURES);
  readonly TONES = Array.from(TONES);

  @Input() formGroup: FormGroup;

  constructor() {
  }

  private static buildForm(): { [p: string]: AbstractControl } {
    return {
      condition: new FormControl(null, Validators.required),
      consciousness: new FormControl(null, Validators.required),
      position: new FormControl(null, [Validators.required, Validators.maxLength(20)]),
      isBodyProportional: new FormControl(null, Validators.required),
      constitution: new FormControl(null, Validators.required),
      height: new FormControl(null, Validators.min(0)),
      weight: new FormControl(null, Validators.min(0)),
      tone: new FormControl(null),
      temperature: new FormControl(null, Validators.required)
    };
  }

  getErrors = (controlName: string) => this.formGroup.get(controlName).errors;

  ngOnInit(): void {
    const form = CreateGeneralExaminationComponent.buildForm();
    for (const f of Object.keys(form)) {
      this.formGroup.addControl(f, form[f]);
    }
  }

}
