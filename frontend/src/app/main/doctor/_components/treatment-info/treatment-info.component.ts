import {Component, Input, OnInit} from '@angular/core';
import {Treatment} from '../../_models/treatment';

@Component({
  selector: 'app-treatment-info',
  templateUrl: './treatment-info.component.html',
  styleUrls: ['./treatment-info.component.scss']
})
export class TreatmentInfoComponent implements OnInit {
  @Input() info: Treatment;

  constructor() {
  }

  ngOnInit(): void {
  }

}
