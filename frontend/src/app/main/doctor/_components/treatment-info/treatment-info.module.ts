import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TreatmentInfoComponent} from './treatment-info.component';
import {MatListModule} from '@angular/material/list';


@NgModule({
  declarations: [TreatmentInfoComponent],
  exports: [
    TreatmentInfoComponent
  ],
  imports: [
    CommonModule,
    MatListModule
  ]
})
export class TreatmentInfoModule {
}
