import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateComplaintsComponent} from './create-complaints.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {DateInputModule} from '../../../_components/date-input/date-input.module';


@NgModule({
  declarations: [
    CreateComplaintsComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatDividerModule,
    MatButtonModule,
    MatIconModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    DateInputModule
  ],
  exports: [
    CreateComplaintsComponent
  ]
})
export class CreateComplaintsModule {
}
