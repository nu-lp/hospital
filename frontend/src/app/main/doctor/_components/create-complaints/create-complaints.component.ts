import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {Observable, of} from 'rxjs';

@Component({
    selector: 'app-create-complaints',
    templateUrl: './create-complaints.component.html',
    styleUrls: ['./create-complaints.component.scss']
})
export class CreateComplaintsComponent implements OnInit {
    @Input() formArray: FormArray;
    today = moment();
    organSystems: Observable<string[]>;

    constructor() {
    }

    get controls(): FormGroup[] {
        return this.formArray.controls.map(c => c as FormGroup);
    }

    private static buildComplaintGroup(): FormGroup {
        return new FormGroup({
            startDate: new FormControl(null, Validators.required),
            description: new FormControl(null, [Validators.required, Validators.maxLength(200)]),
            organSystem: new FormControl(null, Validators.required)
        });
    }

    getErrors = (index: number, controlName: string) => this.formArray.at(index).get(controlName).errors;
    getControl = (index: number, controlName: string) => this.formArray.at(index).get(controlName) as FormControl;

    ngOnInit(): void {
        this.formArray.push(CreateComplaintsComponent.buildComplaintGroup());
        this.organSystems = of([
            'Система органів дихання',
            'Серцево-судинна система',
            'Система органів травлення',
            'Сечовидільна система',
            'Ендокринна система',
            'Нервова система',
        ]);
    }

    addComplaint() {
        this.formArray.push(CreateComplaintsComponent.buildComplaintGroup());
    }

    removeComplaint(idx: number) {
        this.formArray.removeAt(idx);
    }

}
