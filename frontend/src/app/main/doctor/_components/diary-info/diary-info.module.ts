import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DiaryInfoComponent} from './diary-info.component';
import {MatTableModule} from '@angular/material/table';
import {MatSortModule} from '@angular/material/sort';
import {MatPaginatorModule} from '@angular/material/paginator';


@NgModule({
  declarations: [DiaryInfoComponent],
  imports: [
    CommonModule,
    MatTableModule,
    MatSortModule,
    MatPaginatorModule,
  ],
  exports: [DiaryInfoComponent]
})
export class DiaryInfoModule {
}
