import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Diary} from '../../_models/diary';
import {CONDITIONS} from '../../_models/general-examination/condition.enum';
import {MatTableDataSource} from '@angular/material/table';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';

@Component({
  selector: 'app-diary-info',
  templateUrl: './diary-info.component.html',
  styleUrls: ['./diary-info.component.scss']
})
export class DiaryInfoComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;

  @Input() diaries: Diary[];
  datasource: MatTableDataSource<Diary>;
  readonly CONDITIONS = CONDITIONS;
  readonly displayedColumns: string[] = [
    'date',
    'condition',
    'temperature',
    'respiratoryRate',
    'bloodPressure',
    'complaints',
    'treatments'
  ];

  constructor() {
  }

  ngOnInit(): void {
    this.datasource = new MatTableDataSource(this.diaries);
    this.datasource.paginator = this.paginator;
    this.datasource.sort = this.sort;
  }

}
