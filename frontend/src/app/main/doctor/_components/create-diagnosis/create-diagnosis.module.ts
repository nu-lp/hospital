import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateDiagnosisComponent} from './create-diagnosis.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';


@NgModule({
  declarations: [
    CreateDiagnosisComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatDatepickerModule
  ],
  exports: [
    CreateDiagnosisComponent
  ]
})
export class CreateDiagnosisModule {
}
