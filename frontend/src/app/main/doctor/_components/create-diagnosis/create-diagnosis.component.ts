import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import * as moment from 'moment';
import {Moment} from 'moment';
import {MatDatepicker} from '@angular/material/datepicker';

@Component({
  selector: 'app-create-diagnosis',
  templateUrl: './create-diagnosis.component.html',
  styleUrls: ['./create-diagnosis.component.scss']
})
export class CreateDiagnosisComponent implements OnInit {
  @ViewChild(MatDatepicker, {static: true}) datepicker: MatDatepicker<Moment>;

  @Input() formGroup: FormGroup;
  today = moment();

  constructor() {
  }

  ngOnInit(): void {
    this.buildForm();
  }

  getErrors = (controlName: string) => this.formGroup.get(controlName).errors;

  private buildForm() {
    this.formGroup.addControl('description', new FormControl(null,
      [Validators.required, Validators.maxLength(4_000)]
    ));
    this.formGroup.addControl('date', new FormControl(null, Validators.required));
    this.formGroup.addControl('mainDisease', new FormControl(null,
      [Validators.required, Validators.maxLength(200)]
    ));
    this.formGroup.addControl('complicationDisease', new FormControl(null, Validators.maxLength(200)));
    this.formGroup.addControl('concomitantDisease', new FormControl(null, Validators.maxLength(200)));
  }
}
