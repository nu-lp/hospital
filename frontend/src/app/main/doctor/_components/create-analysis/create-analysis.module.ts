import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateAnalysisComponent} from './create-analysis/create-analysis.component';
import {CreateAnalysisListComponent} from './create-analysis-list/create-analysis-list.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';


@NgModule({
  declarations: [CreateAnalysisComponent, CreateAnalysisListComponent],
  exports: [
    CreateAnalysisListComponent,
    CreateAnalysisComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule
  ]
})
export class CreateAnalysisModule {
}
