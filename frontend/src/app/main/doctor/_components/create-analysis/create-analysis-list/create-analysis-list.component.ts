import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';

@Component({
    selector: 'app-create-analysis-list',
    templateUrl: './create-analysis-list.component.html',
    styleUrls: ['./create-analysis-list.component.scss']
})
export class CreateAnalysisListComponent implements OnInit {
    @Input() formArray: FormArray;

    constructor() {
    }

    get controls(): FormGroup[] {
        return this.formArray.controls.map(c => c as FormGroup);
    }

    ngOnInit(): void {
    }

    addAnalysis() {
        this.formArray.push(new FormGroup({}));
    }

    removeAnalysis(idx: number) {
        this.formArray.removeAt(idx);
    }

}
