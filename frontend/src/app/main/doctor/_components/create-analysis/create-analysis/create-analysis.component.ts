import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-create-analysis',
  templateUrl: './create-analysis.component.html',
  styleUrls: ['./create-analysis.component.scss']
})
export class CreateAnalysisComponent implements OnInit {
  @Input() formGroup: FormGroup;

  constructor() {
  }

  ngOnInit(): void {
    this.buildForm();
  }

  getErrors = (controlName: string) => this.formGroup.get(controlName).errors;

  private buildForm() {
    this.formGroup.addControl('type', new FormControl(null, [Validators.required, Validators.maxLength(150)]));
  }

}
