import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DetailedExaminationInfoComponent} from './detailed-examination-info.component';


@NgModule({
  declarations: [DetailedExaminationInfoComponent],
  exports: [
    DetailedExaminationInfoComponent
  ],
  imports: [
    CommonModule
  ]
})
export class DetailedExaminationInfoModule {
}
