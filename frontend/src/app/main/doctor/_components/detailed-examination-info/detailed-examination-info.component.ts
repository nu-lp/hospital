import {Component, Input, OnInit} from '@angular/core';
import {DetailedExamination} from '../../_models/detailed-examination';

@Component({
  selector: 'app-detailed-examination-info',
  templateUrl: './detailed-examination-info.component.html',
  styleUrls: ['./detailed-examination-info.component.scss']
})
export class DetailedExaminationInfoComponent implements OnInit {
  @Input() info: DetailedExamination;

  constructor() {
  }

  ngOnInit(): void {
  }

}
