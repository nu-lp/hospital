import {Component, Input, OnInit} from '@angular/core';
import {Patient} from '../../_models/patient/patient.model';

@Component({
  selector: 'app-patient-info',
  templateUrl: './patient-info.component.html',
  styleUrls: ['./patient-info.component.scss']
})
export class PatientInfoComponent implements OnInit {
  @Input() patient: Patient;

  constructor() {
  }

  ngOnInit(): void {
  }

}
