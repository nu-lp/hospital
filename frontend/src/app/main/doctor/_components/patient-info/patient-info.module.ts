import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PatientInfoComponent} from './patient-info.component';


@NgModule({
  declarations: [PatientInfoComponent],
  imports: [CommonModule],
  exports: [PatientInfoComponent]
})
export class PatientInfoModule {
}
