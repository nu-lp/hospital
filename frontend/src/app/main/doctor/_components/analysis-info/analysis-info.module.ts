import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AnalysisInfoComponent} from './analysis-info.component';


@NgModule({
  declarations: [AnalysisInfoComponent],
  imports: [CommonModule],
  exports: [AnalysisInfoComponent]
})
export class AnalysisInfoModule {
}
