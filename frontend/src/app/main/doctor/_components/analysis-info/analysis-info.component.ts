import {Component, Input, OnInit} from '@angular/core';
import {Analysis} from '../../_models/analysis';

@Component({
  selector: 'app-analysis-info',
  templateUrl: './analysis-info.component.html',
  styleUrls: ['./analysis-info.component.scss']
})
export class AnalysisInfoComponent implements OnInit {
  @Input() analysis: Analysis;

  constructor() {
  }

  get docInfo(): string {
    const d = this.analysis.result.doctor;
    return `${d.lastName} ${d.firstName} ${d.middleName}, ${d.specialization}`;
  }

  ngOnInit(): void {
  }

}
