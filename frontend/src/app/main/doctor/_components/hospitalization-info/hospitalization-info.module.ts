import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HospitalizationInfoComponent} from './hospitalization-info.component';
import {MatListModule} from '@angular/material/list';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [HospitalizationInfoComponent],
  imports: [CommonModule, MatListModule, MatIconModule],
  exports: [HospitalizationInfoComponent]
})
export class HospitalizationInfoModule {
}
