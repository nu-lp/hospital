import {Component, Input, OnInit} from '@angular/core';
import {Hospitalization} from '../../_models/hospitalization';
import {HOSPITALIZATION_TYPES} from '../../_models/hospitalization-type.enum';

@Component({
  selector: 'app-hospitalization-info',
  templateUrl: './hospitalization-info.component.html',
  styleUrls: ['./hospitalization-info.component.scss']
})
export class HospitalizationInfoComponent implements OnInit {
  readonly TYPES = HOSPITALIZATION_TYPES;

  @Input() hospitalization: Hospitalization;

  constructor() {
  }

  ngOnInit(): void {
  }

}
