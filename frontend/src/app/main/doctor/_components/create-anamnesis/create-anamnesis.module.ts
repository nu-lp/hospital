import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateAnamnesisComponent} from './create-anamnesis.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';


@NgModule({
  declarations: [
    CreateAnamnesisComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule
  ],
  exports: [
    CreateAnamnesisComponent
  ]
})
export class CreateAnamnesisModule {
}
