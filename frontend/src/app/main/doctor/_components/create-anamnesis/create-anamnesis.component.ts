import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-create-anamnesis',
  templateUrl: './create-anamnesis.component.html',
  styleUrls: ['./create-anamnesis.component.scss'],
})
export class CreateAnamnesisComponent implements OnInit {
  @Input() formGroup: FormGroup;

  constructor() {
  }

  ngOnInit(): void {
    this.formGroup.addControl('description',
      new FormControl(null, [Validators.required, Validators.maxLength(3000)])
    );
  }

  getErrors = (controlName: string) => (this.formGroup.get(controlName) as FormControl).errors;

}
