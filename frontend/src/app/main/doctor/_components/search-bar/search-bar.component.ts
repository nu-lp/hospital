import {Component, EventEmitter, OnDestroy, OnInit, Output} from '@angular/core';
import {Patient} from '../../_models/patient/patient.model';
import {EMPTY, Observable, Subject, Subscription} from 'rxjs';
import {PatientService} from '../../_services/patient.service';
import {debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss']
})
export class SearchBarComponent implements OnInit, OnDestroy {
  // ms
  private static readonly SEARCH_AFTER = 1000;
  private static readonly FULL_NAME_RGX = /^\s*(\S*)\s*(\S*)\s*(\S*).*$/;

  @Output() patientSelect = new EventEmitter<Patient>();

  searchParam$ = new Subject<string>();
  filteredValues: Observable<Patient[]>;
  value: string;

  private searchParamSubscription: Subscription;

  constructor(private service: PatientService) {
  }

  ngOnInit(): void {
    this.searchParamSubscription = this.searchParam$.pipe(
      debounceTime(SearchBarComponent.SEARCH_AFTER),
      distinctUntilChanged(),
      switchMap(param => {
        this.searchPatients(param);
        return EMPTY;
      })
    ).subscribe();
  }

  ngOnDestroy(): void {
    this.searchParamSubscription.unsubscribe();
  }

  clear() {
    this.value = null;
    this.filteredValues = null;
    this.patientSelect.emit(null);
  }

  private searchPatients(fullName: string) {

    const parts = SearchBarComponent.FULL_NAME_RGX.exec(fullName);
    const lastName = parts[1];
    const firstName = parts[2];
    const middleName = parts[3];

    const isParamsEmpty = lastName.length + firstName.length + middleName.length === 0;
    this.filteredValues = isParamsEmpty ? null : this.service.search(lastName, firstName, middleName);
  }
}
