import {Component, Input, OnInit} from '@angular/core';
import {GeneralExamination} from '../../_models/general-examination';
import {CONDITIONS} from '../../_models/general-examination/condition.enum';
import {CONSCIOUSNESSES} from '../../_models/general-examination/consciousness.enum';
import {CONSTITUIONS} from '../../_models/general-examination/constitution.enum';
import {TONES} from '../../_models/general-examination/tone.enum';
import {TEMPERATURES} from '../../_models/general-examination/temperature.enum';

@Component({
  selector: 'app-general-examination-info',
  templateUrl: './general-examination-info.component.html',
  styleUrls: ['./general-examination-info.component.scss']
})
export class GeneralExaminationInfoComponent implements OnInit {
  readonly CONDITIONS = CONDITIONS;
  readonly CONSCIOUSNESSES = CONSCIOUSNESSES;
  readonly CONSTITUTIONS = CONSTITUIONS;
  readonly TONES = TONES;
  readonly TEMPERATURES = TEMPERATURES;

  @Input() info: GeneralExamination;

  constructor() {
  }

  ngOnInit(): void {
  }

}
