import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GeneralExaminationInfoComponent} from './general-examination-info.component';


@NgModule({
  declarations: [GeneralExaminationInfoComponent],
  exports: [
    GeneralExaminationInfoComponent
  ],
  imports: [
    CommonModule
  ]
})
export class GeneralExaminationInfoModule {
}
