import {Component, Input, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Observable, of} from 'rxjs';

@Component({
  selector: 'app-create-detailed-examination',
  templateUrl: './create-detailed-examination.component.html',
  styleUrls: ['./create-detailed-examination.component.scss']
})
export class CreateDetailedExaminationComponent implements OnInit {
  @Input() formGroup: FormGroup;
  organSystems: Observable<string[]>;

  constructor() {
  }

  ngOnInit(): void {
    this.buildForm();
    this.organSystems = of([
      'Система органів дихання',
      'Серцево-судинна система',
      'Система органів травлення',
      'Сечовидільна система',
      'Ендокринна система',
      'Нервова система',
    ]);
  }

  getErrors = (controlName: string) => this.formGroup.get(controlName).errors;

  private buildForm() {
    this.formGroup.addControl('description', new FormControl(null, [Validators.required, Validators.maxLength(5000)]));
    this.formGroup.addControl('organSystem', new FormControl(null, Validators.required));
  }


}
