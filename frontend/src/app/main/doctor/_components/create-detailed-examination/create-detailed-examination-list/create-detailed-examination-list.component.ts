import {Component, Input, OnInit} from '@angular/core';
import {FormArray, FormGroup} from '@angular/forms';

@Component({
    selector: 'app-create-detailed-examination-list',
    templateUrl: './create-detailed-examination-list.component.html',
    styleUrls: ['./create-detailed-examination-list.component.scss']
})
export class CreateDetailedExaminationListComponent implements OnInit {
    @Input() formArray: FormArray;

    constructor() {
    }

    get controls(): FormGroup[] {
        return this.formArray.controls.map(c => c as FormGroup);
    }

    ngOnInit(): void {
    }

    addExamination() {
        this.formArray.push(new FormGroup({}));
    }

    removeExamination(i: number) {
        this.formArray.removeAt(i);
    }
}
