import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CreateDetailedExaminationComponent} from './create-detailed-examination/create-detailed-examination.component';
import {CreateDetailedExaminationListComponent} from './create-detailed-examination-list/create-detailed-examination-list.component';
import {ReactiveFormsModule} from '@angular/forms';
import {MatDividerModule} from '@angular/material/divider';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';


@NgModule({
  declarations: [CreateDetailedExaminationComponent, CreateDetailedExaminationListComponent],
  exports: [
    CreateDetailedExaminationListComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatDividerModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatButtonModule,
    MatIconModule
  ]
})
export class CreateDetailedExaminationModule {
}
