export class DetailedExamination {
  description: string;
  organSystem: string;

  constructor(params: DetailedExamination) {
    this.description = params.description;
    this.organSystem = params.organSystem;
  }
}
