export enum DiagnosisType {
  Preliminary = 'PRELIMINARY',
  Final = 'FINAL',
  Admittance = 'ADMITTANCE',
}

export const DIAGNOSIS_TYPES = new Map<DiagnosisType, string>([
  [DiagnosisType.Preliminary, 'Попередній діагноз'],
  [DiagnosisType.Final, 'Заключний діагноз'],
  [DiagnosisType.Admittance, 'Діагноз при поступленні']
]);
