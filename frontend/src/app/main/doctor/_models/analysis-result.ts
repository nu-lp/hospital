import {Moment} from 'moment';
import {Doctor} from './doctor';

export class AnalysisResult {
  description: string;
  conclusion: string;
  date: Moment;
  doctor: Doctor;

  constructor(params: AnalysisResult) {
    this.description = params.description;
    this.conclusion = params.conclusion;
    this.date = params.date;
    this.doctor = params.doctor;
  }
}
