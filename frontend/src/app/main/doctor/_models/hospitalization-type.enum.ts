export enum HospitalizationType {
  Planned = 'PLANNED',
  Urgent = 'URGENT',
}

export const HOSPITALIZATION_TYPES = new Map<HospitalizationType, string>([
  [HospitalizationType.Planned, 'Планова'],
  [HospitalizationType.Urgent, 'Ургентна']
]);
