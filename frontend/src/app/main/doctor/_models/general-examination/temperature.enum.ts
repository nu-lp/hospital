export enum Temperature {
  Reduced = 'REDUCED',
  Normal = 'NORMAL',
  Subfebrile = 'SUBFEBRILE',
  Febrile = 'FEBRILE',
  Pyretic = 'PYRETIC',
  Hyperpyrexia = 'HYPERPYREXIA'
}

export const TEMPERATURES = new Map<Temperature, string>([
  [Temperature.Reduced, 'Знижена'],
  [Temperature.Normal, 'Нормальна'],
  [Temperature.Subfebrile, 'Субфебрильна'],
  [Temperature.Febrile, 'Фебрильна'],
  [Temperature.Pyretic, 'Піретична'],
  [Temperature.Hyperpyrexia, 'Гіперпіретична'],
]);
