export enum Condition {
  Satisfactory = 'SATISFACTORY',
  Moderate = 'MODERATE',
  Serious = 'SERIOUS'
}

export const CONDITIONS: Map<Condition, string> = new Map<Condition, string>([
  [Condition.Satisfactory, 'Задовільний'],
  [Condition.Moderate, 'Середньої тяжкості'],
  [Condition.Serious, 'Тяжкий'],
]);
