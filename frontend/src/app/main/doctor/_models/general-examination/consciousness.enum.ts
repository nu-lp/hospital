export enum Consciousness {
  Clear = 'CLEAR',
  Confused = 'CONFUSED',
  Stupor = 'STUPOR',
  Sopor = 'SOPOR',
  Coma = 'COMA',
  Delirium = 'DELIRIUM',
  Hallucinations = 'HALLUCINATIONS'
}

export const CONSCIOUSNESSES = new Map<Consciousness, string>([
  [Consciousness.Clear, 'Ясна'],
  [Consciousness.Confused, 'Сплутана'],
  [Consciousness.Stupor, 'Ступор'],
  [Consciousness.Sopor, 'Сопор'],
  [Consciousness.Coma, 'Кома'],
  [Consciousness.Delirium, 'Марення'],
  [Consciousness.Hallucinations, 'Галюцинації'],
]);
