export enum Tone {
  Increased = 'INCREASED',
  Preserved = 'PRESERVED',
  Reduced = 'REDUCED'
}

export const TONES = new Map<Tone, string>([
  [Tone.Increased, 'Підвищений'],
  [Tone.Preserved, 'Збережений'],
  [Tone.Reduced, 'Понижений']
]);
