export enum Constitution {
  Normostenic = 'NORMOSTENIC',
  Hyperstenic = 'HYPERSTENIC',
  Asthenic = 'ASTHENIC'
}

export const CONSTITUIONS = new Map<Constitution, string>([
  [Constitution.Normostenic, 'Нормостенічна'],
  [Constitution.Hyperstenic, 'Гіперстенічна'],
  [Constitution.Asthenic, 'Астенічна'],
]);
