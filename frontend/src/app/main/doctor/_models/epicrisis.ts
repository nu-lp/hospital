import {EpicrisisType} from './epicrisis-type.enum';
import {Condition} from './general-examination/condition.enum';

export class Epicrisis {
  type: EpicrisisType;
  condition: Condition;
  recommendation: string;

  constructor(params: Epicrisis) {
    this.type = params.type;
    this.condition = params.condition;
    this.recommendation = params.recommendation;
  }
}
