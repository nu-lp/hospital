export class Complaint {
  startDate: Date;
  description: string;
  organSystem: string;

  constructor(params: Complaint) {
    this.startDate = params.startDate;
    this.description = params.description;
    this.organSystem = params.organSystem;
  }
}
