import {Moment} from 'moment';
import {Condition} from './general-examination/condition.enum';

export class Diary {
  date: Moment;
  condition: Condition;
  temperature: number;
  respiratoryRate: number;
  heartRate: number;
  bloodPressureDia: number;
  bloodPressureSys: number;
  complaints: string;
  treatments: string[];

  constructor(params: Diary) {
    this.date = params.date;
    this.condition = params.condition;
    this.temperature = params.temperature;
    this.respiratoryRate = params.respiratoryRate;
    this.heartRate = params.heartRate;
    this.bloodPressureDia = params.bloodPressureDia;
    this.bloodPressureSys = params.bloodPressureSys;
    this.complaints = params.complaints;
    this.treatments = params.treatments;
  }
}
