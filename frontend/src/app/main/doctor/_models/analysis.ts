import {AnalysisResult} from './analysis-result';

export class Analysis {
  uuid: string;
  type: string;
  result?: AnalysisResult;

  constructor(params: Analysis) {
    this.uuid = params.uuid;
    this.type = params.type;
    this.result = params.result;
  }
}
