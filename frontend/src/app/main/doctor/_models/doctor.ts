export class Doctor {
  uuid: string;
  firstName: string;
  lastName: string;
  middleName?: string;
  phone: string;
  specialization: string;

  constructor(params: Doctor) {
    this.uuid = params.uuid;
    this.firstName = params.firstName;
    this.lastName = params.lastName;
    this.middleName = params.middleName;
    this.phone = params.phone;
    this.specialization = params.specialization;
  }
}
