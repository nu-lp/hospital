export enum AnamnesisType {
  Disease = 'DISEASE',
  Life = 'LIFE',
  Epidemic = 'EPIDEMIC',
  Allergic = 'ALLERGIC',
}

export const ANAMNESIS_TYPES = new Map<AnamnesisType, string>([
  [AnamnesisType.Disease, 'Анамнез хвороби'],
  [AnamnesisType.Life, 'Анамнез життя'],
  [AnamnesisType.Epidemic, 'Епідеміологічний анамнез'],
  [AnamnesisType.Allergic, 'Алергологічний анамнез']
]);
