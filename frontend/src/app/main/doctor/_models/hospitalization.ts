import {HospitalizationType} from './hospitalization-type.enum';
import {Doctor} from './doctor';
import {Diagnosis} from './diagnosis';
import {Complaint} from './complaint';
import {Anamnesis} from './anamnesis';
import {GeneralExamination} from './general-examination';
import {DetailedExamination} from './detailed-examination';
import {Analysis} from './analysis';
import {Moment} from 'moment';
import {Treatment} from './treatment';
import {Diary} from './diary';
import {Epicrisis} from './epicrisis';

export class Hospitalization {
  uuid: string;
  hospitalizationDate: Moment;
  diseaseDate: Moment;
  extractDate?: Moment;
  employment: string;
  type: HospitalizationType;
  referral: string;
  doctor: Doctor;
  diagnoses: Diagnosis[];
  complaints: Complaint[];
  anamneses: Anamnesis[];
  generalExamination: GeneralExamination;
  detailedExaminations: DetailedExamination[];
  analyses: Analysis[];
  treatment?: Treatment;
  diaries: Diary[];
  epicrisis?: Epicrisis;

  constructor(params: Hospitalization) {
    this.uuid = params.uuid;
    this.hospitalizationDate = params.hospitalizationDate;
    this.diseaseDate = params.diseaseDate;
    this.extractDate = params.extractDate;
    this.employment = params.employment;
    this.type = params.type;
    this.referral = params.referral;
    this.doctor = params.doctor;
    this.diagnoses = params.diagnoses;
    this.complaints = params.complaints;
    this.anamneses = params.anamneses;
    this.generalExamination = params.generalExamination;
    this.detailedExaminations = params.detailedExaminations;
    this.analyses = params.analyses;
    this.treatment = params.treatment;
    this.diaries = params.diaries;
    this.epicrisis = params.epicrisis;
  }
}
