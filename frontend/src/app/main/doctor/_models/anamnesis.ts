import {AnamnesisType} from './anamnesis-type.enum';

export class Anamnesis {
  type: AnamnesisType;
  description: string;

  constructor(params: Anamnesis) {
    this.type = params.type;
    this.description = params.description;
  }
}
