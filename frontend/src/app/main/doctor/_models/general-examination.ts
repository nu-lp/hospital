import {Condition} from './general-examination/condition.enum';
import {Consciousness} from './general-examination/consciousness.enum';
import {Constitution} from './general-examination/constitution.enum';
import {Tone} from './general-examination/tone.enum';
import {Temperature} from './general-examination/temperature.enum';

export class GeneralExamination {
  condition: Condition;
  consciousness: Consciousness;
  position: string;
  isBodyProportional: boolean;
  constitution: Constitution;
  height?: number;
  weight?: number;
  tone?: Tone;
  temperature: Temperature;

  constructor(params: GeneralExamination) {
    this.condition = params.condition;
    this.consciousness = params.consciousness;
    this.position = params.position;
    this.isBodyProportional = params.isBodyProportional;
    this.constitution = params.constitution;
    this.height = params.height;
    this.weight = params.weight;
    this.tone = params.tone;
    this.temperature = params.temperature;
  }
}
