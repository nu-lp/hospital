export class Address {
  houseNumber: number;
  zipCode: string;
  street: string;
  city: string;

  constructor(params: Address) {
    this.houseNumber = params.houseNumber;
    this.zipCode = params.zipCode;
    this.street = params.street;
    this.city = params.city;
  }
}
