import {Address} from './address.model';
import {Moment} from 'moment';

export class Patient {
  uuid: string;
  lastName: string;
  firstName: string;
  middleName: string;
  birthday: Moment;
  phone: string;
  homePhone: string;
  isMale: boolean;
  address: Address;

  constructor(params: Patient) {
    this.uuid = params.uuid;
    this.lastName = params.lastName;
    this.firstName = params.firstName;
    this.middleName = params.middleName;
    this.birthday = params.birthday;
    this.phone = params.phone;
    this.homePhone = params.homePhone;
    this.isMale = params.isMale;
    this.address = params.address;
  }
}
