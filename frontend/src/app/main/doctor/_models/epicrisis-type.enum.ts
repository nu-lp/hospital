export enum EpicrisisType {
  Extract = 'EXTRACT',
  Redirection = 'REDIRECTION',
  Death = 'DEATH'
}

export const EPICRISIS_TYPES = new Map<EpicrisisType, string>([
  [EpicrisisType.Extract, 'Виписний'],
  [EpicrisisType.Redirection, 'Перенаправлення'],
  [EpicrisisType.Death, 'Посмертний']
]);
