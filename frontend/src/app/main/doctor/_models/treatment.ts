export class Treatment {
  prescribed: string;
  medicines: string;

  constructor(params: Treatment) {
    this.prescribed = params.prescribed;
    this.medicines = params.medicines;
  }
}
