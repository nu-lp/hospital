import {DiagnosisType} from './diagnosis-type.enum';
import {Moment} from 'moment';

export class Diagnosis {
  type: DiagnosisType;
  description: string;
  date: Moment;
  mainDisease: string;
  complicationDisease?: string;
  concomitantDisease?: string;

  constructor(params: Diagnosis) {
    this.type = params.type;
    this.description = params.description;
    this.date = params.date;
    this.mainDisease = params.mainDisease;
    this.complicationDisease = params.complicationDisease;
    this.concomitantDisease = params.concomitantDisease;
  }
}
