import {Injectable} from '@angular/core';
import {Doctor} from '../_models/doctor';
import {Adapter} from '../../../_helpers/adapter';

@Injectable()
export class DoctorAdapter implements Adapter<Doctor> {
  adapt(data: any): Doctor {
    return {
      uuid: data.uuid,
      firstName: data.firstName,
      lastName: data.lastName,
      middleName: data.middleName,
      phone: data.phone,
      specialization: data.specialization
    };
  }
}
