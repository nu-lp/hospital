import {Injectable} from '@angular/core';
import {Address} from '../../_models/patient/address.model';
import {Adapter} from '../../../../_helpers/adapter';

@Injectable()
export class AddressAdapter implements Adapter<Address> {
  adapt(data: any): Address {
    return {
      houseNumber: data.houseNumber,
      city: data.city,
      street: data.street,
      zipCode: data.zipCode
    };
  }
}
