import {Injectable} from '@angular/core';
import {Patient} from '../../_models/patient/patient.model';
import {AddressAdapter} from './address-adapter';
import {Adapter} from '../../../../_helpers/adapter';

@Injectable()
export class PatientAdapter implements Adapter<Patient> {
  constructor(private addressAdapter: AddressAdapter) {
  }

  adapt(data: any): Patient {
    return {
      uuid: data.uuid,
      lastName: data.lastName,
      firstName: data.firstName,
      middleName: data.middleName,
      isMale: data.isMale,
      birthday: data.birthday,
      phone: data.phone,
      homePhone: data.homePhone,
      address: this.addressAdapter.adapt(data.address)
    };
  }
}
