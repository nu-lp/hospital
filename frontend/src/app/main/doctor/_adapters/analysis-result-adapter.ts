import {Injectable} from '@angular/core';
import {AnalysisResult} from '../_models/analysis-result';
import {DoctorAdapter} from './doctor-adapter';
import {Adapter} from '../../../_helpers/adapter';

@Injectable()
export class AnalysisResultAdapter implements Adapter<AnalysisResult> {
  constructor(private doctorAdapter: DoctorAdapter) {
  }

  adapt(data: any): AnalysisResult {
    return {
      description: data.description,
      conclusion: data.conclusion,
      date: data.date,
      doctor: this.doctorAdapter.adapt(data.doctor)
    };
  }
}
