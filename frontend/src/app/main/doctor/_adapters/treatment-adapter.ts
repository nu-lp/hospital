import {Adapter} from '../../../_helpers/adapter';
import {Treatment} from '../_models/treatment';

export class TreatmentAdapter implements Adapter<Treatment> {
  adapt(data: any): Treatment {
    return {
      prescribed: data.prescribed,
      medicines: data.medicines
    };
  }
}
