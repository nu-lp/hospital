import {Injectable} from '@angular/core';
import {Analysis} from '../_models/analysis';
import {AnalysisResultAdapter} from './analysis-result-adapter';
import {Adapter} from '../../../_helpers/adapter';

@Injectable()
export class AnalysisAdapter implements Adapter<Analysis> {

  constructor(private analysisResultAdapter: AnalysisResultAdapter) {
  }

  adapt(data: any): Analysis {
    return {
      uuid: data.uuid,
      type: data.type,
      result: data.result ? this.analysisResultAdapter.adapt(data.result) : undefined
    };
  }
}
