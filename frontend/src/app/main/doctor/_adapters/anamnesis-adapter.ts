import {Injectable} from '@angular/core';
import {Anamnesis} from '../_models/anamnesis';
import {Adapter} from '../../../_helpers/adapter';

@Injectable()
export class AnamnesisAdapter implements Adapter<Anamnesis> {
  adapt(data: any): Anamnesis {
    return {
      type: data.type,
      description: data.description
    };
  }
}
