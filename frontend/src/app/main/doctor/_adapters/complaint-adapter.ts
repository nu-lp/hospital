import {Injectable} from '@angular/core';
import {Complaint} from '../_models/complaint';
import {Adapter} from '../../../_helpers/adapter';

@Injectable()
export class ComplaintAdapter implements Adapter<Complaint> {
  adapt(data: any): Complaint {
    return {
      startDate: data.startDate,
      description: data.description,
      organSystem: data.organSystem
    };
  }
}
