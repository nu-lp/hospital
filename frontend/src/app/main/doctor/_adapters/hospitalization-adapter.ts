import {Injectable} from '@angular/core';
import {Hospitalization} from '../_models/hospitalization';
import {DoctorAdapter} from './doctor-adapter';
import {DiagnosisAdapter} from './diagnosis-adapter';
import {ComplaintAdapter} from './complaint-adapter';
import {AnamnesisAdapter} from './anamnesis-adapter';
import {GeneralExaminationAdapter} from './general-examination-adapter';
import {DetailedExaminationAdapter} from './detailed-examination-adapter';
import {AnalysisAdapter} from './analysis-adapter';
import {Adapter} from '../../../_helpers/adapter';
import {TreatmentAdapter} from './treatment-adapter';
import {DiaryAdapter} from './diary-adapter';
import {EpicrisisAdapter} from './epicrisis-adapter';

@Injectable()
export class HospitalizationAdapter implements Adapter<Hospitalization> {
  constructor(private doctorAdapter: DoctorAdapter,
              private diagnosisAdapter: DiagnosisAdapter,
              private complaintAdapter: ComplaintAdapter,
              private anamnesisAdapter: AnamnesisAdapter,
              private generalExaminationAdapter: GeneralExaminationAdapter,
              private detailedExaminationAdapter: DetailedExaminationAdapter,
              private analysisAdapter: AnalysisAdapter,
              private treatmentAdapter: TreatmentAdapter,
              private diaryAdapter: DiaryAdapter,
              private epicrisisAdapter: EpicrisisAdapter) {
  }


  adapt(data: any): Hospitalization {
    return {
      uuid: data.uuid,
      hospitalizationDate: data.hospitalizationDate,
      diseaseDate: data.diseaseDate,
      extractDate: data.extractDate,
      employment: data.employment,
      type: data.type,
      referral: data.referral,
      doctor: this.doctorAdapter.adapt(data.doctor),
      diagnoses: data.diagnoses.map(d => this.diagnosisAdapter.adapt(d)),
      complaints: data.complaints.map(c => this.complaintAdapter.adapt(c)),
      anamneses: data.anamneses.map(a => this.anamnesisAdapter.adapt(a)),
      generalExamination: this.generalExaminationAdapter.adapt(data.generalExamination),
      detailedExaminations: data.detailedExaminations.map(d => this.detailedExaminationAdapter.adapt(d)),
      analyses: data.analyses.map(a => this.analysisAdapter.adapt(a)),
      treatment: data.treatment ? this.treatmentAdapter.adapt(data.treatment) : undefined,
      diaries: data.diaries.map(d => this.diaryAdapter.adapt(d)),
      epicrisis: data.epicrisis ? this.epicrisisAdapter.adapt(data.epicrisis) : undefined
    };
  }
}
