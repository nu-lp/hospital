import {Adapter} from '../../../_helpers/adapter';
import {Epicrisis} from '../_models/epicrisis';

export class EpicrisisAdapter implements Adapter<Epicrisis> {
  adapt(data: any): Epicrisis {
    return {
      type: data.type,
      condition: data.condition,
      recommendation: data.recommendation
    };
  }
}
