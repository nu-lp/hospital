import {Injectable} from '@angular/core';
import {DetailedExamination} from '../_models/detailed-examination';
import {Adapter} from '../../../_helpers/adapter';

@Injectable()
export class DetailedExaminationAdapter implements Adapter<DetailedExamination> {
  adapt(data: any): DetailedExamination {
    return {
      description: data.description,
      organSystem: data.organSystem
    };
  }
}
