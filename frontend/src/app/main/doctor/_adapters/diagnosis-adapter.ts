import {Injectable} from '@angular/core';
import {Diagnosis} from '../_models/diagnosis';
import {Adapter} from '../../../_helpers/adapter';

@Injectable()
export class DiagnosisAdapter implements Adapter<Diagnosis> {
  adapt(data: any): Diagnosis {
    return {
      type: data.type,
      description: data.description,
      date: data.date,
      mainDisease: data.mainDisease,
      complicationDisease: data.complicationDisease,
      concomitantDisease: data.concomitantDisease
    };
  }
}
