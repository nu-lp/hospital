import {Injectable} from '@angular/core';
import {GeneralExamination} from '../_models/general-examination';
import {Adapter} from '../../../_helpers/adapter';

@Injectable()
export class GeneralExaminationAdapter implements Adapter<GeneralExamination> {
  adapt(data: any): GeneralExamination {
    return {
      condition: data.condition,
      consciousness: data.consciousness,
      position: data.position,
      isBodyProportional: data.isBodyProportional,
      constitution: data.constitution,
      height: data.height,
      weight: data.weight,
      tone: data.tone,
      temperature: data.temperature
    };
  }
}
