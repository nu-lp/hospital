import {Adapter} from '../../../_helpers/adapter';
import {Diary} from '../_models/diary';

export class DiaryAdapter implements Adapter<Diary> {
  adapt(data: any): Diary {
    return {
      date: data.date,
      condition: data.condition,
      temperature: data.temperature,
      respiratoryRate: data.respiratoryRate,
      heartRate: data.heartRate,
      bloodPressureDia: data.bloodPressureDia,
      bloodPressureSys: data.bloodPressureSys,
      complaints: data.complaints,
      treatments: data.treatments.map(t => t as string)
    };
  }
}
