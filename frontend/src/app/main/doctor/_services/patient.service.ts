import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {PatientAdapter} from '../_adapters/patient/patient-adapter';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Patient} from '../_models/patient/patient.model';

@Injectable()
export class PatientService {
  private readonly URL = {
    SEARCH: `${environment.apiUrl}/patients/findByName`,
    GET: (uuid: string) => `${environment.apiUrl}/patients/${uuid}`,
    CREATE: `${environment.apiUrl}/patients`,
    UPDATE: `${environment.apiUrl}/patients`,
    GET_DOC_PATIENTS: `${environment.apiUrl}/doctors/patients`
  };

  constructor(private adapter: PatientAdapter,
              private http: HttpClient) {
  }

  search(lastName: string, firstName: string, middleName: string): Observable<Patient[]> {
    const MAX_LENGTH = 45;
    return this.http.get(this.URL.SEARCH, {
      params: {
        fName: firstName.slice(0, MAX_LENGTH),
        mName: middleName.slice(0, MAX_LENGTH),
        lName: lastName.slice(0, MAX_LENGTH)
      }
    }).pipe(
      map((data: any[]) => data.map(p => this.adapter.adapt(p)))
    );
  }

  create(patient: any): Observable<Patient> {
    return this.http.post(this.URL.CREATE, patient).pipe(
      map((data: any) => this.adapter.adapt(data))
    );
  }

  update(patient: any): Observable<Patient> {
    return this.http.put(this.URL.CREATE, patient).pipe(
      map((data: any) => this.adapter.adapt(data))
    );
  }

  getByUuid(uuid: string): Observable<Patient> {
    return this.http.get(this.URL.GET(uuid)).pipe(
      map(data => this.adapter.adapt(data))
    );
  }

  getDoctorsPatients(): Observable<Patient[]> {
    return this.http.get(this.URL.GET_DOC_PATIENTS).pipe(
      map((data: any[]) => data.map(p => this.adapter.adapt(p)))
    );
  }
}
