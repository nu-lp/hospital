import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {DiaryAdapter} from '../_adapters/diary-adapter';
import {Diary} from '../_models/diary';

@Injectable()
export class DiaryService {
  private readonly URL = {
    CREATE: `${environment.apiUrl}/diary`,
  };


  constructor(private adapter: DiaryAdapter,
              private http: HttpClient) {
  }

  create(value: any): Observable<Diary> {
    return this.http.post(this.URL.CREATE, value).pipe(
      map(h => this.adapter.adapt(h))
    );
  }
}
