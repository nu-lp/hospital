import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {TreatmentAdapter} from '../_adapters/treatment-adapter';
import {Treatment} from '../_models/treatment';

@Injectable()
export class TreatmentService {
  private readonly URL = {
    CREATE: `${environment.apiUrl}/treatments`,
  };


  constructor(private adapter: TreatmentAdapter,
              private http: HttpClient) {
  }

  create(value: any): Observable<Treatment> {
    return this.http.post(this.URL.CREATE, value).pipe(
      map(h => this.adapter.adapt(h))
    );
  }
}
