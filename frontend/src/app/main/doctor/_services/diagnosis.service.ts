import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {DiagnosisAdapter} from '../_adapters/diagnosis-adapter';
import {Diagnosis} from '../_models/diagnosis';

@Injectable()
export class DiagnosisService {
  private readonly URL = {
    CREATE: `${environment.apiUrl}/diagnoses`,
  };


  constructor(private adapter: DiagnosisAdapter,
              private http: HttpClient) {
  }

  create(value: any): Observable<Diagnosis> {
    return this.http.post(this.URL.CREATE, value).pipe(
      map(h => this.adapter.adapt(h))
    );
  }
}
