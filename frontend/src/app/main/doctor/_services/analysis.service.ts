import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {AnalysisAdapter} from '../_adapters/analysis-adapter';
import {Analysis} from '../_models/analysis';

@Injectable()
export class AnalysisService {
  private readonly URL = {
    CREATE: `${environment.apiUrl}/analyses`,
    GET_ALL: (hospitalizationUuid: string) => `${environment.apiUrl}/patients/${hospitalizationUuid}/analyses`,
    ADD_RESULT: `${environment.apiUrl}/analyses`,
  };


  constructor(private adapter: AnalysisAdapter,
              private http: HttpClient) {
  }

  create(value: any): Observable<Analysis> {
    return this.http.post(this.URL.CREATE, value).pipe(
      map(h => this.adapter.adapt(h))
    );
  }

  getAll(patientUuid: string): Observable<Analysis[]> {
    return this.http.get(this.URL.GET_ALL(patientUuid)).pipe(
      map((data: any[]) => data.map(a => this.adapter.adapt(a)))
    );
  }

  addResult(value: any): Observable<Analysis> {
    return this.http.patch(this.URL.CREATE, value).pipe(
      map(h => this.adapter.adapt(h))
    );
  }
}
