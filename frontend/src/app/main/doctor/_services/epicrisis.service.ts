import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {EpicrisisAdapter} from '../_adapters/epicrisis-adapter';
import {Epicrisis} from '../_models/epicrisis';

@Injectable()
export class EpicrisisService {
  private readonly URL = {
    CREATE: `${environment.apiUrl}/epicrises`,
  };


  constructor(private adapter: EpicrisisAdapter,
              private http: HttpClient) {
  }

  create(value: any): Observable<Epicrisis> {
    return this.http.post(this.URL.CREATE, value).pipe(
      map(h => this.adapter.adapt(h))
    );
  }
}
