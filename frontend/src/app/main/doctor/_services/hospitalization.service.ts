import {Injectable} from '@angular/core';
import {HospitalizationAdapter} from '../_adapters/hospitalization-adapter';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Hospitalization} from '../_models/hospitalization';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';

@Injectable()
export class HospitalizationService {
  private readonly URL = {
    CREATE: `${environment.apiUrl}/hospitalizations`,
    GET: (patientUuid: string) => `${environment.apiUrl}/patients/${patientUuid}/hospitalization`,
    GET_PDF: (hospitalizationUuid: string) => `${environment.apiUrl}/hospitalizations/${hospitalizationUuid}/pdf`,
    GET_ALL: (patientUuid: string) => `${environment.apiUrl}/patients/${patientUuid}/hospitalizations`,
  };


  constructor(private adapter: HospitalizationAdapter,
              private http: HttpClient) {
  }

  create(value: any): Observable<Hospitalization> {
    return this.http.post(this.URL.CREATE, value).pipe(
      map(h => this.adapter.adapt(h))
    );
  }

  getByPatientUuid(patientUuid: string): Observable<Hospitalization> {
    return this.http.get(this.URL.GET(patientUuid)).pipe(
        map(h => this.adapter.adapt(h))
    );
  }

  getAllByPatientUuid(patientUuid: string): Observable<Hospitalization[]> {
    return this.http.get(this.URL.GET_ALL(patientUuid)).pipe(
        map((data: any[]) => data.map(h => this.adapter.adapt(h)))
    );
  }

  getPdf(hospitalizationUuid: string): Observable<Blob> {
    return this.http.get(this.URL.GET_PDF(hospitalizationUuid), {responseType: 'blob'});
  }
}
