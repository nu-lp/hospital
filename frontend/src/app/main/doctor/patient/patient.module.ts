import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PatientRoutingModule} from './patient-routing.module';
import {SearchPatientComponent} from './_components/search-patient/search-patient.component';
import {MatButtonModule} from '@angular/material/button';
import {PatientInfoModule} from '../_components/patient-info/patient-info.module';
import {SearchBarModule} from '../_components/search-bar/search-bar.module';
import {CreateComponent} from './_components/create/create.component';
import {MatDividerModule} from '@angular/material/divider';
import {ReactiveFormsModule} from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {PhoneModule} from '../../../_directives/phone/phone.module';
import {MatRadioModule} from '@angular/material/radio';
import {MatInputModule} from '@angular/material/input';


@NgModule({
  declarations: [
    SearchPatientComponent,
    CreateComponent,
  ],
  imports: [
    CommonModule,
    PatientRoutingModule,
    MatButtonModule,
    PatientInfoModule,
    SearchBarModule,
    MatDividerModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatDatepickerModule,
    PhoneModule,
    MatRadioModule,
    MatInputModule,
  ],
  providers: []
})
export class PatientModule {
}
