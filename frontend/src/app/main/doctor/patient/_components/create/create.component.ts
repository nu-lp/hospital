import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDatepicker} from '@angular/material/datepicker';
import * as moment from 'moment';
import {Moment} from 'moment';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {PatientService} from '../../../_services/patient.service';
import {Patient} from '../../../_models/patient/patient.model';
import {DOCTOR_ROUTING, HOSPITALIZATION_ROUTING} from '../../../../../globals';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  @ViewChild(MatDatepicker, {static: true}) datepicker: MatDatepicker<Moment>;
  maxBirthday = moment();
  form: FormGroup;
  private editMode: boolean;

  constructor(private service: PatientService,
              private router: Router,
              private route: ActivatedRoute) {
  }

  private static buildForm(editMode: boolean, p: Patient): FormGroup {
    const form = new FormGroup({
      uuid: new FormControl({value: null, disabled: !editMode}),
      lastName: new FormControl({value: null, disabled: editMode}, [Validators.required, Validators.maxLength(45)]),
      firstName: new FormControl({value: null, disabled: editMode}, [Validators.required, Validators.maxLength(45)]),
      middleName: new FormControl({value: null, disabled: editMode}, [Validators.maxLength(45)]),
      birthday: new FormControl({value: null, disabled: editMode}, [Validators.required]),
      phone: new FormControl(null, [Validators.required, Validators.minLength(15), Validators.maxLength(15)]),
      homePhone: new FormControl(null, [Validators.required, Validators.minLength(15), Validators.maxLength(15)]),
      isMale: new FormControl({value: true, disabled: editMode}, [Validators.required]),
      address: new FormGroup({
        houseNumber: new FormControl(null, [Validators.required, Validators.min(1)]),
        zipCode: new FormControl(null, [Validators.required, Validators.maxLength(5)]),
        street: new FormControl(null, [Validators.required, Validators.maxLength(45)]),
        city: new FormControl(null, [Validators.required, Validators.maxLength(45)]),
      })
    });
    if (!!p) {
      form.setValue(p);
    }
    return form;
  }

  ngOnInit(): void {
    this.editMode = this.route.snapshot.data.editMode || false;
    const patient = this.route.snapshot.data.patient;
    this.form = CreateComponent.buildForm(this.editMode, patient);
  }

  onSubmit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    if (this.editMode) {
      this.service.update(this.form.value).subscribe(p => this.navigateToHospitalization(p.uuid));
    } else {
      this.service.create(this.form.value).subscribe(p => this.navigateToHospitalization(p.uuid));
    }
  }

  getErrors = (controlName: string) => (this.form.get(controlName) as FormControl).errors;

  getControl = (controlName: string) => (this.form.get(controlName) as FormControl);

  private navigateToHospitalization(patientUuid: string) {
    this.router.navigate(
      [DOCTOR_ROUTING.HOSPITALIZATION, patientUuid, HOSPITALIZATION_ROUTING.CREATE],
      {relativeTo: this.route.parent.parent}
    );
  }
}
