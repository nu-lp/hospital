import {Component, OnInit} from '@angular/core';
import {DOCTOR_ROUTING, HOSPITALIZATION_ROUTING, PARENT_PATH, PATIENTS_ROUTING} from '../../../../../globals';
import {Patient} from '../../../_models/patient/patient.model';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-search-patient',
  templateUrl: './search-patient.component.html',
  styleUrls: ['./search-patient.component.scss']
})
export class SearchPatientComponent implements OnInit {
  readonly PARENT = PARENT_PATH;
  readonly PATIENT = PATIENTS_ROUTING;
  patient: Patient;

  constructor(private router: Router,
              private route: ActivatedRoute) {
  }

  ngOnInit(): void {
  }

  navToHospitalization() {
    this.router.navigate(
      [DOCTOR_ROUTING.HOSPITALIZATION, this.patient.uuid, HOSPITALIZATION_ROUTING.CREATE],
      {relativeTo: this.route.parent.parent}
    );
  }
}
