import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BLANK_PATH, PATIENTS_ROUTING} from '../../../globals';
import {SearchPatientComponent} from './_components/search-patient/search-patient.component';
import {CreateComponent} from './_components/create/create.component';
import {PatientResolver} from '../_resolvers/patient.resolver';


const routes: Routes = [
  {
    path: BLANK_PATH,
    redirectTo: PATIENTS_ROUTING.SEARCH,
    pathMatch: 'full'
  },
  {
    path: PATIENTS_ROUTING.SEARCH,
    component: SearchPatientComponent
  },
  {
    path: PATIENTS_ROUTING.CREATE,
    component: CreateComponent
  },
  {
    path: `:id/${PATIENTS_ROUTING.EDIT}`,
    component: CreateComponent,
    data: {editMode: true},
    resolve: {patient: PatientResolver}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientRoutingModule {
}
