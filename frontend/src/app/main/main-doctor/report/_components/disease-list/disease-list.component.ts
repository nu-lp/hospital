import {Component, OnInit} from '@angular/core';
import {DiseasesInRegion} from '../../../_models/diseases-in-region';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-disease-list',
  templateUrl: './disease-list.component.html',
  styleUrls: ['./disease-list.component.scss']
})
export class DiseaseListComponent implements OnInit {
  displayedColumns: string[] = ['city', 'disease', 'count'];
  data: DiseasesInRegion[];

  constructor(private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.data = this.route.snapshot.data.diseases;
  }

}
