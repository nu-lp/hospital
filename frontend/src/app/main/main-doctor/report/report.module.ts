import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {ReportRoutingModule} from './report-routing.module';
import {DiseaseListComponent} from './_components/disease-list/disease-list.component';
import {MatTableModule} from '@angular/material/table';


@NgModule({
  declarations: [DiseaseListComponent],
  imports: [
    CommonModule,
    ReportRoutingModule,
    MatTableModule
  ]
})
export class ReportModule {
}
