import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BLANK_PATH, REPORTS_ROUTING} from '../../../globals';
import {DiseaseListComponent} from './_components/disease-list/disease-list.component';
import {DiseaseListResolver} from '../_resolvers/disease-list-resolver';


const routes: Routes = [
  {
    path: BLANK_PATH,
    pathMatch: 'full',
    redirectTo: REPORTS_ROUTING.DISEASES
  },
  {
    path: REPORTS_ROUTING.DISEASES,
    component: DiseaseListComponent,
    resolve: {diseases: DiseaseListResolver}
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReportRoutingModule {
}
