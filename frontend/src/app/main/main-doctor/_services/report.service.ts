import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {DiseasesInRegionAdapter} from '../_adapters/diseases-in-region-adapter';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {DiseasesInRegion} from '../_models/diseases-in-region';
import {map} from 'rxjs/operators';

@Injectable()
export class ReportService {
  readonly URL = {
    DISEASES: `${environment.apiUrl}/reports/diseasesInRegion`
  };

  constructor(private http: HttpClient,
              private diseasesInRegionAdapter: DiseasesInRegionAdapter) {
  }

  getDiseasesInRegion(): Observable<DiseasesInRegion[]> {
    return this.http.get(this.URL.DISEASES).pipe(
      map((data: any[]) => data.map(d => this.diseasesInRegionAdapter.adapt(d)))
    );
  }
}
