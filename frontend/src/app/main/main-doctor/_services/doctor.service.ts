import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {EmployeeDoctorAdapter} from '../_adapters/employee-doctor-adapter';
import {Observable} from 'rxjs';
import {EmployeeDoctor} from '../_models/employee-doctor';
import {map} from 'rxjs/operators';
import {Page} from '../../../_helpers/page';

@Injectable()
export class DoctorService {
  private readonly URL = {
    CREATE: `${environment.apiUrl}/doctors`,
    GET_ALL: `${environment.apiUrl}/doctors`,
    CHANGE_MAIN: (uuid: string) => `${environment.apiUrl}/doctors/${uuid}/mainDoc`,
    CHANGE_ACTIVE: (uuid: string) => `${environment.apiUrl}/doctors/${uuid}/active`,
  };

  constructor(private http: HttpClient,
              private adapter: EmployeeDoctorAdapter) {
  }

  create(value: any): Observable<EmployeeDoctor> {
    return this.http.post(this.URL.CREATE, value).pipe(
      map(data => this.adapter.adapt(data))
    );
  }

  getAll(page?: string, size?: string): Observable<Page<EmployeeDoctor>> {
    const params = (!!page && !!size) ? {p: page, s: size} : null;
    return this.http.get(this.URL.GET_ALL, {
      params
    }).pipe(
      map((data: any) => {
        return {
          totalItems: data.totalItems,
          items: data.items.map(i => this.adapter.adapt(i))
        };
      })
    );
  }

  setMainDoc(val: boolean, uuid: string): Observable<any> {
    return this.http.get(this.URL.CHANGE_MAIN(uuid), {
      params: {val: (val as any as string)}
    });
  }

  setActive(val: boolean, uuid: string): Observable<any> {
    return this.http.get(this.URL.CHANGE_ACTIVE(uuid), {
      params: {val: (val as any as string)}
    });
  }

}
