import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BLANK_PATH} from '../../../globals';
import {ManageComponent} from './_components/manage/manage.component';
import {DoctorsResolver} from '../_resolvers/doctors-resolver';


const routes: Routes = [
  {
    path: BLANK_PATH,
    component: ManageComponent,
    resolve: {doctors: DoctorsResolver},
    runGuardsAndResolvers: 'paramsOrQueryParamsChange'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorRoutingModule {
}
