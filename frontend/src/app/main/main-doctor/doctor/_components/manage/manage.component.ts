import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import {CreateDialogComponent} from '../create-dialog/create-dialog.component';
import {DoctorService} from '../../../_services/doctor.service';
import {EmployeeDoctor} from '../../../_models/employee-doctor';
import {MatPaginator} from '@angular/material/paginator';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-manage',
  templateUrl: './manage.component.html',
  styleUrls: ['./manage.component.scss']
})
export class ManageComponent implements OnInit, OnDestroy {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  displayedColumns: string[] = ['fullName', 'phone', 'specialization', 'username', 'isMainDoc', 'active'];
  data: EmployeeDoctor[];
  totalItems = 0;
  readonly DIALOG_MIN_WIDTH = '500px';
  private subscription: Subscription;

  constructor(
    private dialog: MatDialog,
    private snackBar: MatSnackBar,
    private service: DoctorService,
    private route: ActivatedRoute,
    private router: Router
  ) {
  }

  ngOnInit(): void {
    this.subscription = this.route.data.subscribe(data => {
      this.data = data.doctors.items;
      this.totalItems = data.doctors.totalItems;
    });

    this.paginator.page.subscribe(page => {
      this.router.navigate([], {
        relativeTo: this.route,
        queryParams: {
          page: page.pageIndex,
          size: page.pageSize
        },
        queryParamsHandling: 'merge'
      });
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  createDoc(): void {
    const dialogRef = this.dialog.open(CreateDialogComponent, {
      width: '50%',
      minWidth: this.DIALOG_MIN_WIDTH,
      height: '90%',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (!!result) {
        this.service.create(result).subscribe(doc => {
          console.log(doc);
          this.snackBar.open('Успішно призначено лікаря', 'Ок');
        });
      }
    });
  }

  onMainDocChange(val: boolean, uuid: string) {
    this.service.setMainDoc(val, uuid).subscribe();
  }

  onSetActive(val: boolean, uuid: string) {
    this.service.setActive(val, uuid).subscribe();
  }
}
