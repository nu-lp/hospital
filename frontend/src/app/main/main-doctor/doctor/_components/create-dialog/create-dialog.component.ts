import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-create-dialog',
  templateUrl: './create-dialog.component.html',
  styleUrls: ['./create-dialog.component.scss']
})
export class CreateDialogComponent implements OnInit {
  form: FormGroup;

  constructor(public dialogRef: MatDialogRef<CreateDialogComponent>) {
  }

  getErrors = (controlName: string) => (this.form.get(controlName) as FormControl).errors;
  getControl = (controlName: string) => (this.form.get(controlName) as FormControl);

  ngOnInit(): void {
    this.form = new FormGroup({
      lastName: new FormControl(null, [Validators.required, Validators.maxLength(45)]),
      firstName: new FormControl(null, [Validators.required, Validators.maxLength(45)]),
      middleName: new FormControl(null, [Validators.maxLength(45)]),
      phone: new FormControl(null, [Validators.required, Validators.minLength(15), Validators.maxLength(15)]),
      // TODO async validator on existing
      username: new FormControl(null, [Validators.required, Validators.minLength(4), Validators.maxLength(30)]),
      password: new FormControl(null, [Validators.required, Validators.minLength(6), Validators.maxLength(20)]),
      confirmPassword: new FormControl(null),
      specialization: new FormControl(null, [Validators.required, Validators.maxLength(100)]),
      isMainDoc: new FormControl(false)
    }, this.checkPasswords);
  }

  onSubmit() {
    if (this.form.invalid) {
      this.form.markAllAsTouched();
      return;
    }
    const val = this.form.value;
    val.confirmPassword = undefined;
    this.dialogRef.close(val);
  }

  checkPasswords(group: FormGroup) {
    const pass = group.get('password').value;
    const confirmPass = group.get('confirmPassword').value;
    if (pass !== confirmPass) {
      group.get('confirmPassword').setErrors({passwordsNotSame: true});
    }
    return pass === confirmPass ? null : {passwordsNotSame: true};
  }
}
