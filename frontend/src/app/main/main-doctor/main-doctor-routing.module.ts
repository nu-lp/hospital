import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BLANK_PATH, MAIN_DOCTOR_ROUTING} from '../../globals';


const routes: Routes = [
  {
    path: BLANK_PATH,
    pathMatch: 'full',
    redirectTo: MAIN_DOCTOR_ROUTING.DOCTORS
  },
  {
    path: MAIN_DOCTOR_ROUTING.DOCTORS,
    loadChildren: () => import('./doctor/doctor.module').then(m => m.DoctorModule)
  },
  {
    path: MAIN_DOCTOR_ROUTING.REPORTS,
    loadChildren: () => import('./report/report.module').then(m => m.ReportModule)
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainDoctorRoutingModule {
}
