export class DiseasesInRegion {
  city: string;
  disease: string;
  count: number;

  constructor(params: DiseasesInRegion) {
    this.city = params.city;
    this.disease = params.disease;
    this.count = params.count;
  }
}
