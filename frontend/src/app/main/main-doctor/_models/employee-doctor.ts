import {Moment} from 'moment';

export class EmployeeDoctor {
  uuid: string;
  firstName: string;
  lastName: string;
  middleName: string;
  phone: string;
  specialization: string;
  isMainDoc: boolean;
  username: string;
  createTime: Moment;
  lastLoginTime: Moment;
  active: boolean;

  constructor(params: EmployeeDoctor) {
    this.uuid = params.uuid;
    this.firstName = params.firstName;
    this.lastName = params.lastName;
    this.middleName = params.middleName;
    this.phone = params.phone;
    this.specialization = params.specialization;
    this.isMainDoc = params.isMainDoc;
    this.username = params.username;
    this.createTime = params.createTime;
    this.lastLoginTime = params.lastLoginTime;
    this.active = params.active;
  }
}
