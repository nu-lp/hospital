import {Injectable} from '@angular/core';
import {Adapter} from '../../../_helpers/adapter';
import {EmployeeDoctor} from '../_models/employee-doctor';

@Injectable()
export class EmployeeDoctorAdapter implements Adapter<EmployeeDoctor> {
  adapt(data: any): EmployeeDoctor {
    return {
      uuid: data.uuid,
      firstName: data.firstName,
      lastName: data.lastName,
      middleName: data.middleName,
      phone: data.phone,
      specialization: data.specialization,
      isMainDoc: data.isMainDoc,
      username: data.username,
      createTime: data.createTime,
      lastLoginTime: data.lastLoginTime,
      active: data.active,
    };
  }
}
