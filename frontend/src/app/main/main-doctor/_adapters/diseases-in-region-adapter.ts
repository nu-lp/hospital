import {Injectable} from '@angular/core';
import {Adapter} from '../../../_helpers/adapter';
import {DiseasesInRegion} from '../_models/diseases-in-region';

@Injectable()
export class DiseasesInRegionAdapter implements Adapter<DiseasesInRegion> {
  adapt(data: any): DiseasesInRegion {
    return {
      city: data.city,
      disease: data.disease,
      count: data.count
    };
  }
}
