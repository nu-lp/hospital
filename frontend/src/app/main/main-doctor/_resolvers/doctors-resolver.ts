import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Page} from '../../../_helpers/page';
import {EmployeeDoctor} from '../_models/employee-doctor';
import {Observable} from 'rxjs';
import {DoctorService} from '../_services/doctor.service';

@Injectable()
export class DoctorsResolver implements Resolve<Page<EmployeeDoctor>> {
  constructor(private service: DoctorService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<Page<EmployeeDoctor>> | Promise<Page<EmployeeDoctor>> | Page<EmployeeDoctor> {
    const page = route.queryParamMap.get('page');
    const size = route.queryParamMap.get('size');

    return this.service.getAll(page, size);
  }

}
