import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {DiseasesInRegion} from '../_models/diseases-in-region';
import {ReportService} from '../_services/report.service';

@Injectable()
export class DiseaseListResolver implements Resolve<DiseasesInRegion[]> {
  constructor(private service: ReportService) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
    Observable<DiseasesInRegion[]> | Promise<DiseasesInRegion[]> | DiseasesInRegion[] {
    return this.service.getDiseasesInRegion();
  }
}
