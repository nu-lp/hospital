import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {MainDoctorRoutingModule} from './main-doctor-routing.module';
import {DoctorService} from './_services/doctor.service';
import {EmployeeDoctorAdapter} from './_adapters/employee-doctor-adapter';
import {DoctorsResolver} from './_resolvers/doctors-resolver';
import {ReportService} from './_services/report.service';
import {DiseaseListResolver} from './_resolvers/disease-list-resolver';
import {DiseasesInRegionAdapter} from './_adapters/diseases-in-region-adapter';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MainDoctorRoutingModule,
  ],
  providers: [
    DoctorService,
    EmployeeDoctorAdapter,
    DoctorsResolver,
    ReportService,
    DiseaseListResolver,
    DiseasesInRegionAdapter,
  ]
})
export class MainDoctorModule {
}
