import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BLANK_PATH, MAIN_ROUTING} from '../globals';
import {MainComponent} from './main.component';
import {AuthGuard} from '../auth/_guards/auth.guard';


const routes: Routes = [
  {
    path: BLANK_PATH,
    redirectTo: MAIN_ROUTING.DOCTOR,
    pathMatch: 'full'
  },
  {
    path: MAIN_ROUTING.DOCTOR,
    component: MainComponent,
    loadChildren: () => import('./doctor/doctor.module').then(m => m.DoctorModule),
    canLoad: [AuthGuard],
    data: {roles: ['ROLE_DOCTOR']}
  },
  {
    path: MAIN_ROUTING.MAIN_DOC,
    component: MainComponent,
    loadChildren: () => import('./main-doctor/main-doctor.module').then(m => m.MainDoctorModule),
    canLoad: [AuthGuard],
    data: {roles: ['ROLE_MAIN_DOCTOR']}
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MainRoutingModule {
}
