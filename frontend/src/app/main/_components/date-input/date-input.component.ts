import {Component, Input, OnInit, ViewChild} from '@angular/core';
import {Moment} from 'moment';
import {MatDatepicker} from '@angular/material/datepicker';
import {FormControl} from '@angular/forms';

@Component({
  selector: 'app-date-input',
  templateUrl: './date-input.component.html',
  styleUrls: ['./date-input.component.scss']
})
export class DateInputComponent implements OnInit {
  @ViewChild(MatDatepicker, {static: true}) datepicker: MatDatepicker<Moment>;

  @Input() label: string;
  @Input() min: Moment;
  @Input() max: Moment;
  @Input() control: FormControl;

  constructor() {
  }

  ngOnInit(): void {
  }

}
