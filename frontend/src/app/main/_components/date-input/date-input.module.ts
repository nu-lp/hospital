import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DateInputComponent} from './date-input.component';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';


@NgModule({
  declarations: [
    DateInputComponent
  ],
  imports: [
    CommonModule,
    MatFormFieldModule,
    MatDatepickerModule,
    ReactiveFormsModule,
    MatInputModule
  ],
  exports: [
    DateInputComponent
  ]
})
export class DateInputModule {
}
