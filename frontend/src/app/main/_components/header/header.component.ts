import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../../auth/_services/auth.service';
import {User} from '../../../auth/_models/user.model';
import {DOCTOR_ROUTING, MAIN_DOCTOR_ROUTING, MAIN_ROUTING, ROOT_PATH, ROUTING} from '../../../globals';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  readonly ROUTING = ROUTING;
  readonly MAIN_ROUTING = MAIN_ROUTING;
  readonly DOCTOR_ROUTING = DOCTOR_ROUTING;
  readonly MAIN_DOCTOR = MAIN_DOCTOR_ROUTING;
  readonly ROOT_PATH = ROOT_PATH;

  fullName: string;

  constructor(private authService: AuthService) {
  }

  private static getFullName(user: User): string {
    const lName = user.lastName;
    const fName = user.firstName;
    const mName = user.middleName;

    return `${lName} ${fName} ${mName}`;
  }

  ngOnInit(): void {
    this.fullName = HeaderComponent.getFullName(this.authService.userValue);
  }

  logout() {
    this.authService.logout();
  }

  showBtn(role: string): boolean {
    const user = this.authService.userValue;
    return user.roles.includes(role);
  }
}
