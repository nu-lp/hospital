package ua.lpnu.hospital.api.dto.doctor;

import lombok.Data;

@Data
public class DoctorDto {
    private String uuid;
    private String firstName;
    private String lastName;
    private String middleName;
    private String phone;
    private String specialization;
}
