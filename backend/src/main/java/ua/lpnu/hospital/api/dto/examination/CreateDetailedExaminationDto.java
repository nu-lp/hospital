package ua.lpnu.hospital.api.dto.examination;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class CreateDetailedExaminationDto {
    @NotEmpty
    @Size(max = 5000)
    private String description;
    @NotEmpty
    private String organSystem;
}
