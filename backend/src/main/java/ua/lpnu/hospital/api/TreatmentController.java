package ua.lpnu.hospital.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ua.lpnu.hospital.api.dto.treatment.CreateTreatmentDto;
import ua.lpnu.hospital.api.dto.treatment.TreatmentDto;
import ua.lpnu.hospital.services.TreatmentService;

import javax.validation.Valid;

@RestController
@RequestMapping("/treatments")
@Validated
public class TreatmentController {
    private final TreatmentService service;

    @Autowired
    public TreatmentController(TreatmentService service) {
        this.service = service;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TreatmentDto createFinalDiagnosis(@RequestBody
                                             @Valid CreateTreatmentDto dto,
                                             Authentication authentication) {
        String doctorUuid = (String) authentication.getDetails();

        return service.create(doctorUuid, dto);
    }
}
