package ua.lpnu.hospital.api.dto.auth;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class LoggedInUserDto {
    private String token;
    private String firstName;
    private String middleName;
    private String lastName;
    private List<String> roles;
}
