package ua.lpnu.hospital.api.dto.examination;

import lombok.Data;

@Data
public class DetailedExaminationDto {
    private String description;
    private String organSystem;
}
