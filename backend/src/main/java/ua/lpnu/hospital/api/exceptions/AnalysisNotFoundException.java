package ua.lpnu.hospital.api.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class AnalysisNotFoundException extends RuntimeException {
    public AnalysisNotFoundException() {
        super();
    }

    public AnalysisNotFoundException(String message) {
        super(message);
    }
}
