package ua.lpnu.hospital.api.dto.analysis;

import lombok.Data;
import ua.lpnu.hospital.api.validators.Uuid;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class CreateAnalysisDto {
    @Uuid
    private String hospitalizationUuid;
    @NotEmpty
    @Size(max = 150)
    private String type;
}
