package ua.lpnu.hospital.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ua.lpnu.hospital.api.dto.diary.CreateDiaryDto;
import ua.lpnu.hospital.api.dto.diary.DiaryDto;
import ua.lpnu.hospital.services.DiaryService;

import javax.validation.Valid;

@RestController
@RequestMapping("/diary")
@Validated
public class DiaryController {
    private final DiaryService service;

    @Autowired
    public DiaryController(DiaryService service) {
        this.service = service;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DiaryDto createFinalDiagnosis(@RequestBody
                                         @Valid CreateDiaryDto dto,
                                         Authentication authentication) {
        String doctorUuid = (String) authentication.getDetails();

        return service.create(doctorUuid, dto);
    }
}
