package ua.lpnu.hospital.api.dto.diary;

import lombok.Data;
import ua.lpnu.hospital.api.validators.Uuid;
import ua.lpnu.hospital.db.models.examination.Condition;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;
import java.util.List;

@Data
public class CreateDiaryDto {
    @NotEmpty
    @Uuid
    private String hospitalizationUuid;
    @NotNull
    private Condition condition;
    @Positive
    private Float temperature;
    @Positive
    private Integer respiratoryRate;
    @Positive
    private Integer heartRate;
    @Positive
    private Integer bloodPressureDia;
    @Positive
    private Integer bloodPressureSys;
    @Size(max = 300)
    private String complaints;
    @Size(max = 500)
    private String examination;
    @NotNull
    private List<String> treatments;
}
