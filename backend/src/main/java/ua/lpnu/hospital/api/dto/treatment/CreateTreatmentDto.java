package ua.lpnu.hospital.api.dto.treatment;

import lombok.Data;
import ua.lpnu.hospital.api.validators.Uuid;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class CreateTreatmentDto {
    @NotEmpty
    @Uuid
    private String hospitalizationUuid;
    @NotEmpty
    @Size(max = 500)
    private String prescribed;
    @Size(max = 500)
    private String medicines;
}
