package ua.lpnu.hospital.api.dto.anamnesis;

import lombok.Data;
import ua.lpnu.hospital.db.models.AnamnesisType;

@Data
public class AnamnesisDto {
    private AnamnesisType type;
    private String description;
}
