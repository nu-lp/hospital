package ua.lpnu.hospital.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ua.lpnu.hospital.api.dto.analysis.AnalysisDto;
import ua.lpnu.hospital.api.dto.analysis.CreateAnalysisDto;
import ua.lpnu.hospital.api.dto.analysis.CreateAnalysisResultDto;
import ua.lpnu.hospital.services.AnalysisService;

import javax.validation.Valid;

@RestController
@RequestMapping("/analyses")
@Validated
public class AnalysisController {
    private final AnalysisService service;

    @Autowired
    public AnalysisController(AnalysisService service) {
        this.service = service;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AnalysisDto createAnalysis(@RequestBody
                                      @Valid CreateAnalysisDto dto,
                                      Authentication authentication) {
        String doctorUuid = (String) authentication.getDetails();
        return service.createAnalysis(doctorUuid, dto);
    }

    @PatchMapping
    @ResponseStatus(HttpStatus.OK)
    public AnalysisDto createResult(@RequestBody
                                    @Valid CreateAnalysisResultDto dto,
                                    Authentication authentication) {
        String doctorUuid = (String) authentication.getDetails();
        return service.createResult(doctorUuid, dto);
    }
}
