package ua.lpnu.hospital.api.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Data
public class AddressDto {
    @NotNull
    @Positive
    private Integer houseNumber;
    @NotEmpty
    @Size(max = 5)
    private String zipCode;
    @NotEmpty
    @Size(max = 45)
    private String street;
    @NotEmpty
    @Size(max = 45)
    private String city;
}
