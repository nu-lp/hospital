package ua.lpnu.hospital.api.dto.doctor;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CreateDoctorDto {
    @NotEmpty
    @Size(max = 45)
    private String firstName;
    @NotEmpty
    @Size(max = 45)
    private String lastName;
    @NotEmpty
    @Size(max = 45)
    private String middleName;
    @NotEmpty
    @Size(min = 15, max = 15)
    private String phone;
    @NotEmpty
    @Size(max = 100)
    private String specialization;
    @NotEmpty
    @Size(max = 30)
    private String username;
    @NotEmpty
    @Size(min = 6, max = 20)
    private String password;
    @NotNull
    private Boolean isMainDoc;
}
