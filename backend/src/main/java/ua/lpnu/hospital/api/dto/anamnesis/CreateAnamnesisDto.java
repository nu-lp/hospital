package ua.lpnu.hospital.api.dto.anamnesis;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class CreateAnamnesisDto {
    @NotEmpty
    @Size(max = 3000)
    private String description;
}
