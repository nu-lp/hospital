package ua.lpnu.hospital.api.dto.epicrisis;

import lombok.Data;
import ua.lpnu.hospital.db.models.EpicrisisType;
import ua.lpnu.hospital.db.models.examination.Condition;

@Data
public class EpicrisisDto {
    private EpicrisisType type;
    private Condition condition;
    private String recommendation;
}
