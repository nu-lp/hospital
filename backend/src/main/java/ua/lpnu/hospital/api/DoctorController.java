package ua.lpnu.hospital.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ua.lpnu.hospital.api.dto.PageDto;
import ua.lpnu.hospital.api.dto.doctor.CreateDoctorDto;
import ua.lpnu.hospital.api.dto.doctor.EmployeeDoctorDto;
import ua.lpnu.hospital.api.dto.patient.PatientDto;
import ua.lpnu.hospital.api.validators.Uuid;
import ua.lpnu.hospital.services.DoctorService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/doctors")
@Validated
public class DoctorController {
    private final DoctorService service;

    @Autowired
    public DoctorController(DoctorService service) {
        this.service = service;
    }

    @GetMapping("/patients")
    @ResponseStatus(HttpStatus.OK)
    public List<PatientDto> getPatients(Authentication authentication) {
        String doctorUuid = (String) authentication.getDetails();
        return service.getPatients(doctorUuid);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public EmployeeDoctorDto create(@RequestBody
                                    @Valid CreateDoctorDto dto) {
        return service.createDoctor(dto);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public PageDto<EmployeeDoctorDto> getAll(
            @RequestParam(value = "p", required = false, defaultValue = "0") Integer pageIndex,
            @RequestParam(value = "s", required = false, defaultValue = "10") Integer pageSize
    ) {
        return service.getAll(pageIndex, pageSize);
    }

    @GetMapping("/{uuid}/mainDoc")
    @ResponseStatus(HttpStatus.OK)
    public void changeMainDocStatus(@PathVariable("uuid")
                                    @Uuid String docUuid,
                                    @RequestParam(value = "val") Boolean value) {
        service.changeMainDocStatus(docUuid, value);
    }

    @GetMapping("/{uuid}/active")
    @ResponseStatus(HttpStatus.OK)
    public void changeActiveStatus(@PathVariable("uuid")
                                   @Uuid String docUuid,
                                   @RequestParam(value = "val") Boolean value) {
        service.changeActive(docUuid, value);
    }
}
