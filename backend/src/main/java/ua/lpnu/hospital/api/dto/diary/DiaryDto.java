package ua.lpnu.hospital.api.dto.diary;

import lombok.Data;
import ua.lpnu.hospital.db.models.examination.Condition;

import java.time.LocalDateTime;
import java.util.List;

@Data
public class DiaryDto {
    private LocalDateTime date;
    private Condition condition;
    private Float temperature;
    private Integer respiratoryRate;
    private Integer heartRate;
    private Integer bloodPressureDia;
    private Integer bloodPressureSys;
    private String complaints;
    private String examination;
    private List<String> treatments;
}
