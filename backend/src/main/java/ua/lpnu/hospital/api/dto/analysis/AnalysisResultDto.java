package ua.lpnu.hospital.api.dto.analysis;

import lombok.Data;
import ua.lpnu.hospital.api.dto.doctor.DoctorDto;

import java.time.LocalDateTime;

@Data
public class AnalysisResultDto {
    private String description;
    private String conclusion;
    private LocalDateTime date;
    private DoctorDto doctor;
}
