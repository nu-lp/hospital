package ua.lpnu.hospital.api.dto.hospitalization;

import lombok.Data;
import ua.lpnu.hospital.api.dto.analysis.AnalysisDto;
import ua.lpnu.hospital.api.dto.anamnesis.AnamnesisDto;
import ua.lpnu.hospital.api.dto.complaint.ComplaintDto;
import ua.lpnu.hospital.api.dto.diagnosis.DiagnosisDto;
import ua.lpnu.hospital.api.dto.diary.DiaryDto;
import ua.lpnu.hospital.api.dto.doctor.DoctorDto;
import ua.lpnu.hospital.api.dto.epicrisis.EpicrisisDto;
import ua.lpnu.hospital.api.dto.examination.DetailedExaminationDto;
import ua.lpnu.hospital.api.dto.examination.GeneralExaminationDto;
import ua.lpnu.hospital.api.dto.treatment.TreatmentDto;
import ua.lpnu.hospital.db.models.HospitalizationType;

import java.time.LocalDate;
import java.util.List;

@Data
public class HospitalizationDto {
    private String uuid;
    private LocalDate hospitalizationDate;
    private LocalDate diseaseDate;
    private LocalDate extractDate;
    private String employment;
    private HospitalizationType type;
    private String referral;
    private DoctorDto doctor;
    private List<DiagnosisDto> diagnoses;
    private List<ComplaintDto> complaints;
    private List<AnamnesisDto> anamneses;
    private GeneralExaminationDto generalExamination;
    private List<DetailedExaminationDto> detailedExaminations;
    private List<AnalysisDto> analyses;
    private TreatmentDto treatment;
    private List<DiaryDto> diaries;
    private EpicrisisDto epicrisis;
}
