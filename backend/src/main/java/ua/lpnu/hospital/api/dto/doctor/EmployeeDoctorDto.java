package ua.lpnu.hospital.api.dto.doctor;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class EmployeeDoctorDto {
    private String uuid;
    private String firstName;
    private String lastName;
    private String middleName;
    private String phone;
    private String specialization;
    private Boolean isMainDoc;
    private String username;
    private LocalDateTime createTime;
    private LocalDateTime lastLoginTime;
    private Boolean active;
}
