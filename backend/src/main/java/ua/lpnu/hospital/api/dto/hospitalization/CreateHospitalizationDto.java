package ua.lpnu.hospital.api.dto.hospitalization;

import lombok.Data;
import ua.lpnu.hospital.api.dto.analysis.CreateAnalysisDto;
import ua.lpnu.hospital.api.dto.anamnesis.CreateAnamnesisDto;
import ua.lpnu.hospital.api.dto.complaint.CreateComplaintDto;
import ua.lpnu.hospital.api.dto.diagnosis.CreateDiagnosisDto;
import ua.lpnu.hospital.api.dto.examination.CreateDetailedExaminationDto;
import ua.lpnu.hospital.api.dto.examination.GeneralExaminationDto;
import ua.lpnu.hospital.api.validators.Uuid;
import ua.lpnu.hospital.db.models.HospitalizationType;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Data
public class CreateHospitalizationDto {
    @NotEmpty
    @Uuid
    private String patientUuid;
    @NotNull
    @PastOrPresent
    private LocalDate hospitalizationDate;
    @NotNull
    @PastOrPresent
    private LocalDate diseaseDate;
    @NotEmpty
    @Size(max = 100)
    private String employment;
    @NotNull
    private HospitalizationType type;
    @NotEmpty
    @Size(max = 100)
    private String referral;
    @NotNull
    @Valid
    private CreateDiagnosisDto admittanceDiagnosis;
    @NotNull
    @Valid
    private CreateDiagnosisDto preliminaryDiagnosis;
    @NotEmpty
    @Valid
    private List<CreateComplaintDto> complaints;
    @NotNull
    @Valid
    private CreateAnamnesisDto diseaseAnamnesis;
    @NotNull
    @Valid
    private CreateAnamnesisDto lifeAnamnesis;
    @NotNull
    @Valid
    private CreateAnamnesisDto epidemicAnamnesis;
    @NotNull
    @Valid
    private CreateAnamnesisDto allergicAnamnesis;
    @NotNull
    @Valid
    private GeneralExaminationDto generalExamination;
    @NotNull
    @Valid
    private List<CreateDetailedExaminationDto> detailedExaminations;
    @NotNull
    @Valid
    private List<CreateAnalysisDto> analyses;
}
