package ua.lpnu.hospital.api.dto.auth;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class AuthUserDto {
    @NotEmpty
    @Size(max = 30)
    private String username;
    @NotEmpty
    @Size(min = 6, max = 20)
    private String password;
}
