package ua.lpnu.hospital.api.dto.complaint;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
public class CreateComplaintDto {
    @NotNull
    @PastOrPresent
    private LocalDate startDate;
    @NotEmpty
    @Size(max = 200)
    private String description;
    @NotEmpty
    private String organSystem;
}
