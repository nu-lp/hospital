package ua.lpnu.hospital.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ua.lpnu.hospital.db.models.DiseasesInRegionView;
import ua.lpnu.hospital.services.ReportService;

import java.util.List;

@RestController
@RequestMapping("/reports")
@Validated
public class ReportController {
    private final ReportService service;

    @Autowired
    public ReportController(ReportService service) {
        this.service = service;
    }


    @GetMapping("/diseasesInRegion")
    @ResponseStatus(HttpStatus.OK)
    public List<DiseasesInRegionView> diseasesInRegion() {
        return service.getDiseasesInRegionReport();
    }
}
