package ua.lpnu.hospital.api.dto.examination;

import lombok.Data;
import ua.lpnu.hospital.db.models.examination.*;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Data
public class GeneralExaminationDto {
    @NotNull
    private Condition condition;
    @NotNull
    private Consciousness consciousness;
    @NotEmpty
    @Size(max = 20)
    private String position;
    @NotNull
    private Boolean isBodyProportional;
    @NotNull
    private Constitution constitution;
    @Positive
    private Float height;
    @Positive
    private Float weight;
    private Tone tone;
    @NotNull
    private Temperature temperature;
}
