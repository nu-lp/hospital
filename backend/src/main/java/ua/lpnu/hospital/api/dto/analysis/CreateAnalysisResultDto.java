package ua.lpnu.hospital.api.dto.analysis;

import lombok.Data;
import ua.lpnu.hospital.api.validators.Uuid;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class CreateAnalysisResultDto {
    @NotEmpty
    @Uuid
    private String analysisUuid;
    @NotEmpty
    @Size(max = 500)
    private String description;
    @NotEmpty
    @Size(max = 150)
    private String conclusion;
}
