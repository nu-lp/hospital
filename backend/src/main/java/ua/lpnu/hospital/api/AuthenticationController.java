package ua.lpnu.hospital.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ua.lpnu.hospital.api.dto.auth.AuthUserDto;
import ua.lpnu.hospital.api.dto.auth.LoggedInUserDto;
import ua.lpnu.hospital.db.models.auth.JwtUser;
import ua.lpnu.hospital.services.impl.JwtUserDetailsService;
import ua.lpnu.hospital.utils.JwtTokenUtil;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Set;

@RestController
@Validated
public class AuthenticationController {
    private final AuthenticationManager authManager;
    private final JwtUserDetailsService userDetailsService;
    private final JwtTokenUtil tokenUtil;

    @Autowired
    public AuthenticationController(AuthenticationManager authManager,
                                    JwtUserDetailsService userDetailsService,
                                    JwtTokenUtil tokenUtil) {
        this.authManager = authManager;
        this.userDetailsService = userDetailsService;
        this.tokenUtil = tokenUtil;
    }

    @PostMapping("/login")
    @ResponseStatus(HttpStatus.OK)
    public LoggedInUserDto authenticate(@RequestBody
                                        @Valid AuthUserDto authDto) {
        authManager.authenticate(new UsernamePasswordAuthenticationToken(
                authDto.getUsername(),
                authDto.getPassword()
        ));

        final JwtUser user = userDetailsService.loadUserByUsername(authDto.getUsername());
        final String token = tokenUtil.generateToken(user);
        final Set<String> roles = AuthorityUtils.authorityListToSet(user.getAuthorities());

        return new LoggedInUserDto(
                token,
                user.getFirstName(),
                user.getMiddleName(),
                user.getLastName(),
                new ArrayList<>(roles)
        );
    }
}
