package ua.lpnu.hospital.api.dto.patient;

import lombok.Data;
import ua.lpnu.hospital.api.dto.AddressDto;
import ua.lpnu.hospital.api.validators.Uuid;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class UpdatePatientDto {
    @NotEmpty
    @Uuid
    private String uuid;
    @NotEmpty
    @Size(min = 15, max = 15)
    private String phone;
    @NotEmpty
    @Size(min = 15, max = 15)
    private String homePhone;
    @NotNull
    @Valid
    private AddressDto address;
}
