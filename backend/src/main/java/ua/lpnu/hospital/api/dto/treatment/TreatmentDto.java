package ua.lpnu.hospital.api.dto.treatment;

import lombok.Data;

@Data
public class TreatmentDto {
    private String prescribed;
    private String medicines;
}
