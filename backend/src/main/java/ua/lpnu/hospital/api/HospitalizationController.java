package ua.lpnu.hospital.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ua.lpnu.hospital.api.dto.hospitalization.CreateHospitalizationDto;
import ua.lpnu.hospital.api.dto.hospitalization.HospitalizationDto;
import ua.lpnu.hospital.api.validators.Uuid;
import ua.lpnu.hospital.services.HospitalizationService;

import javax.validation.Valid;

@RestController
@RequestMapping("/hospitalizations")
@Validated
public class HospitalizationController {
    private final HospitalizationService service;

    @Autowired
    public HospitalizationController(HospitalizationService service) {
        this.service = service;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public HospitalizationDto createHospitalization(@RequestBody
                                                    @Valid CreateHospitalizationDto dto,
                                                    Authentication authentication) {
        String docUuid = (String) authentication.getDetails();
        return service.createHospitalization(docUuid, dto);
    }

    @GetMapping(value = "/{uuid}/pdf",
            produces = MediaType.APPLICATION_PDF_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public @ResponseBody
    ResponseEntity<byte[]> generatePdf(@PathVariable("uuid") @Uuid String uuid) {
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"Medicine_history.pdf\"")
                .header(HttpHeaders.EXPIRES, "0")
                .header(HttpHeaders.CACHE_CONTROL, "must-revalidate, post-check=0, pre-check=0")
                .header(HttpHeaders.PRAGMA, "public")
                .body(service.generateReport(uuid));
    }
}
