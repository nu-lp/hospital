package ua.lpnu.hospital.api.dto.patient;

import lombok.Data;
import ua.lpnu.hospital.api.dto.AddressDto;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
public class CreatePatientDto {
    @NotEmpty
    @Size(max = 45)
    private String firstName;
    @NotEmpty
    @Size(max = 45)
    private String lastName;
    @Size(max = 45)
    private String middleName;
    @NotNull
    @PastOrPresent
    private LocalDate birthday;
    @NotEmpty
    @Size(min = 15, max = 15)
    private String phone;
    @NotEmpty
    @Size(min = 15, max = 15)
    private String homePhone;
    @NotNull
    private Boolean isMale;
    @NotNull
    @Valid
    private AddressDto address;
}
