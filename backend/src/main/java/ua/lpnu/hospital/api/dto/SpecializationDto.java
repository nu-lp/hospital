package ua.lpnu.hospital.api.dto;

import lombok.Data;

@Data
public class SpecializationDto {
    private Long id;
    private String name;
}
