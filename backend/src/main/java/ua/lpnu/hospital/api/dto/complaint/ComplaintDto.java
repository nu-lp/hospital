package ua.lpnu.hospital.api.dto.complaint;

import lombok.Data;

import java.time.LocalDate;

@Data
public class ComplaintDto {
    private LocalDate startDate;
    private String description;
    private String organSystem;
}
