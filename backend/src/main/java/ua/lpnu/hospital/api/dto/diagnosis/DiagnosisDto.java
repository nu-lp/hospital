package ua.lpnu.hospital.api.dto.diagnosis;

import lombok.Data;
import ua.lpnu.hospital.db.models.DiagnosisType;

import java.time.LocalDate;

@Data
public class DiagnosisDto {
    private DiagnosisType type;
    private String description;
    private LocalDate date;
    private String mainDisease;
    private String complicationDisease;
    private String concomitantDisease;
}
