package ua.lpnu.hospital.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ua.lpnu.hospital.api.dto.analysis.AnalysisDto;
import ua.lpnu.hospital.api.dto.hospitalization.HospitalizationDto;
import ua.lpnu.hospital.api.dto.patient.CreatePatientDto;
import ua.lpnu.hospital.api.dto.patient.PatientDto;
import ua.lpnu.hospital.api.dto.patient.UpdatePatientDto;
import ua.lpnu.hospital.api.validators.Uuid;
import ua.lpnu.hospital.services.PatientService;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.List;

@RestController
@RequestMapping("/patients")
@Validated
public class PatientController {
    private final PatientService service;

    @Autowired
    public PatientController(PatientService service) {
        this.service = service;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<PatientDto> getPatients() {
        return service.getAll();
    }

    @GetMapping("/findByName")
    @ResponseStatus(HttpStatus.OK)
    public List<PatientDto> findPatientsByName(
            @RequestParam(name = "fName", required = false)
            @Size(max = 45) String firstName,
            @RequestParam(name = "mName", required = false)
            @Size(max = 45) String middleName,
            @RequestParam(name = "lName", required = false)
            @Size(max = 45) String lastName
    ) {
        return service.findAllByName(lastName, firstName, middleName);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.OK)
    public PatientDto createPatient(@RequestBody
                                    @Valid CreatePatientDto patientDto) {
        return service.createPatient(patientDto);
    }

    @PutMapping
    @ResponseStatus(HttpStatus.OK)
    public PatientDto updatePatient(@RequestBody
                                    @Valid UpdatePatientDto patientDto) {
        return service.updatePatient(patientDto);
    }

    @GetMapping("/{uuid}/hospitalization")
    @ResponseStatus(HttpStatus.OK)
    public HospitalizationDto getHospitalization(@PathVariable("uuid")
                                                 @Uuid String patientUuid,
                                                 Authentication authentication) {
        String doctorUuid = (String) authentication.getDetails();
        return service.getHospitalization(patientUuid, doctorUuid);
    }

    @GetMapping("/{uuid}/hospitalizations")
    @ResponseStatus(HttpStatus.OK)
    public List<HospitalizationDto> getHospitalizations(@PathVariable("uuid")
                                                        @Uuid String patientUuid) {
        return service.getHospitalizations(patientUuid);
    }

    @GetMapping("/{uuid}/analyses")
    @ResponseStatus(HttpStatus.OK)
    public List<AnalysisDto> getPendingAnalyses(@PathVariable("uuid")
                                                @Uuid String patientUuid) {
        return service.getPendingAnalyses(patientUuid);
    }

    @GetMapping("/{uuid}")
    public PatientDto getPatient(@PathVariable("uuid")
                                 @NotEmpty
                                 @Uuid String uuid) {
        return service.getByUuid(uuid);
    }


    @PutMapping("/{uuid}")
    public PatientDto updatePatient(@PathVariable("uuid")
                                    @NotEmpty
                                    @Uuid String uuid,
                                    @RequestBody
                                    @Valid CreatePatientDto patientDto) {
        return service.updatePatient(patientDto, uuid);
    }
}
