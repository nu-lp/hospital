package ua.lpnu.hospital.api.dto.analysis;

import lombok.Data;

@Data
public class AnalysisDto {
    private String uuid;
    private String type;
    private AnalysisResultDto result;
}
