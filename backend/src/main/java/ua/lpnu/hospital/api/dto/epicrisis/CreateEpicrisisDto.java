package ua.lpnu.hospital.api.dto.epicrisis;

import lombok.Data;
import ua.lpnu.hospital.api.validators.Uuid;
import ua.lpnu.hospital.db.models.EpicrisisType;
import ua.lpnu.hospital.db.models.examination.Condition;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CreateEpicrisisDto {
    @NotEmpty
    @Uuid
    private String hospitalizationUuid;
    @NotNull
    private EpicrisisType type;
    @NotNull
    private Condition condition;
    @NotEmpty
    @Size(max = 500)
    private String recommendation;
}
