package ua.lpnu.hospital.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ua.lpnu.hospital.api.dto.epicrisis.CreateEpicrisisDto;
import ua.lpnu.hospital.api.dto.epicrisis.EpicrisisDto;
import ua.lpnu.hospital.services.EpicrisisService;

import javax.validation.Valid;

@RestController
@RequestMapping("/epicrises")
@Validated
public class EpicrisisController {
    private final EpicrisisService service;

    @Autowired
    public EpicrisisController(EpicrisisService service) {
        this.service = service;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EpicrisisDto createFinalDiagnosis(@RequestBody
                                             @Valid CreateEpicrisisDto dto,
                                             Authentication authentication) {
        String doctorUuid = (String) authentication.getDetails();
        return service.createEpicrisis(doctorUuid, dto);
    }
}
