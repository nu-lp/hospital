package ua.lpnu.hospital.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ua.lpnu.hospital.api.dto.diagnosis.CreateDiagnosisDto;
import ua.lpnu.hospital.api.dto.diagnosis.DiagnosisDto;
import ua.lpnu.hospital.services.DiagnosisService;

import javax.validation.Valid;

@RestController
@RequestMapping("/diagnoses")
@Validated
public class DiagnosisController {
    private final DiagnosisService service;

    @Autowired
    public DiagnosisController(DiagnosisService service) {
        this.service = service;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public DiagnosisDto createFinalDiagnosis(@RequestBody
                                             @Valid CreateDiagnosisDto dto,
                                             Authentication authentication) {
        String doctorUuid = (String) authentication.getDetails();
        return service.createFinalDiagnosis(doctorUuid, dto);
    }
}
