package ua.lpnu.hospital.api.dto.diagnosis;

import lombok.Data;
import ua.lpnu.hospital.api.validators.Uuid;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Data
public class CreateDiagnosisDto {
    @Uuid
    private String hospitalizationUuid;
    @NotEmpty
    @Size(max = 4000)
    private String description;
    @NotNull
    private LocalDate date;
    @NotEmpty
    @Size(max = 200)
    private String mainDisease;
    @Size(max = 200)
    private String complicationDisease;
    @Size(max = 200)
    private String concomitantDisease;
}
