package ua.lpnu.hospital.api.dto.patient;

import lombok.Data;
import ua.lpnu.hospital.api.dto.AddressDto;

import java.time.LocalDate;

@Data
public class PatientDto {
    private String uuid;
    private String firstName;
    private String lastName;
    private String middleName;
    private LocalDate birthday;
    private String phone;
    private String homePhone;
    private Boolean isMale;
    private AddressDto address;
}
