package ua.lpnu.hospital.api.dto;

import lombok.Data;

@Data
public class OrganSystemDto {
    private Long id;
    private String name;
}
