package ua.lpnu.hospital.dataload;

import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.db.models.Doctor;
import ua.lpnu.hospital.db.models.Specialization;
import ua.lpnu.hospital.db.models.auth.Role;
import ua.lpnu.hospital.db.models.auth.User;
import ua.lpnu.hospital.db.models.auth.UserRole;
import ua.lpnu.hospital.db.repos.DoctorRepository;
import ua.lpnu.hospital.db.repos.RoleRepository;

import java.util.*;

@Component
@Slf4j
public class FakeDoctor {
    private static final int DOCTORS = 50;
    private final Random random = new Random();
    private final DoctorRepository doctorRepository;
    private final PasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final Faker faker;

    @Autowired
    public FakeDoctor(DoctorRepository doctorRepository,
                      PasswordEncoder passwordEncoder,
                      RoleRepository roleRepository,
                      Faker faker) {
        this.doctorRepository = doctorRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.faker = faker;
    }

    public List<Doctor> fake(List<Specialization> specializations) {
        Role role = roleRepository.findByName(UserRole.ROLE_DOCTOR)
                .orElseThrow(NoSuchElementException::new);
        Role roleMain = roleRepository.findByName(UserRole.ROLE_MAIN_DOCTOR)
                .orElseThrow(NoSuchElementException::new);
        List<Doctor> doctors = new LinkedList<>();
        List<String> usernames = fakeUsernames(DOCTORS);

        System.out.println("----------------------------GENERATED DOCTORS----------------------------");
        for (int i = 0; i < DOCTORS; i++) {
            doctors.add(fakeDoctor(
                    specializations.get(random.nextInt(specializations.size())),
                    role,
                    roleMain,
                    usernames.get(i)
            ));
        }
        System.out.println("----------------------------GENERATED DOCTORS----------------------------");

        log.info("{} - generated doctors", doctors.size());

        return doctorRepository.saveAll(doctors);
    }

    private Doctor fakeDoctor(Specialization specialization, Role doctorRole, Role roleMain, String username) {
        String password = faker.internet().password(6, 15);
        boolean isMain = faker.bool().bool();

        System.out.printf("login: %s password: %s", username, password);
        if (isMain) {
            System.out.print("\tmain");
        }
        System.out.println();

        User user = new User();
        user.setUsername(username);
        user.setPasswordHash(passwordEncoder.encode(password));
        user.setIsActive(true);
        user.getRoles().add(doctorRole);
        if (isMain) {
            user.getRoles().add(roleMain);
        }

        Doctor doc = new Doctor();
        boolean isMale = faker.bool().bool();
        doc.setFirstName(fakeFirstName(isMale));
        doc.setLastName(fakeLastName(isMale));
        doc.setMiddleName(fakeMiddleName(isMale));
        doc.setPhone(faker.phoneNumber().phoneNumber());
        doc.setSpecialization(specialization);
        doc.setUser(user);

        return doc;
    }

    private List<String> fakeUsernames(int n) {
        Set<String> usernames = new HashSet<>();
        while (usernames.size() < n) {
            usernames.add(faker.name().username());
        }
        return List.of(usernames.toArray(new String[0]));
    }

    private String fakeFirstName(boolean isMale) {
        return faker.resolve(isMale ? "name.male_first_name" : "name.female_first_name");
    }

    private String fakeLastName(boolean isMale) {
        return faker.resolve(isMale ? "name.male_last_name" : "name.female_last_name");
    }

    private String fakeMiddleName(boolean isMale) {
        return faker.resolve(isMale ? "name.male_middle_name" : "name.female_middle_name");
    }
}
