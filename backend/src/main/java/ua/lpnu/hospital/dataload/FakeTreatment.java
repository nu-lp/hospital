package ua.lpnu.hospital.dataload;

import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.models.Treatment;
import ua.lpnu.hospital.db.repos.TreatmentRepository;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Component
@Slf4j
public class FakeTreatment {
    private static final List<String> TREATMENTS = List.of(
            "Режим: постільний",
            "Інгаляції",
            "-",
            "Дієта: голод"
    );
    private static final int MEDICINES = 5;
    private final Random r = new Random();
    private final Faker faker;
    private final TreatmentRepository treatmentRepository;

    @Autowired
    public FakeTreatment(Faker faker,
                         TreatmentRepository treatmentRepository) {
        this.faker = faker;
        this.treatmentRepository = treatmentRepository;
    }

    public List<Treatment> fake(List<Hospitalization> hospitalizations) {
        List<Treatment> treatments = hospitalizations.stream()
                .map(this::fakeTreatment)
                .collect(Collectors.toList());

        log.info("{} - generated treatments", treatments.size());
        return treatmentRepository.saveAll(treatments);
    }

    private Treatment fakeTreatment(Hospitalization hospitalization) {
        Treatment treatment = new Treatment();
        treatment.setPrescribed(TREATMENTS.get(r.nextInt(TREATMENTS.size())));
        treatment.setHospitalization(hospitalization);
        StringBuilder medicines = new StringBuilder();
        for (int i = 0; i < MEDICINES; i++) {
            medicines.append(faker.medical().medicineName()).append("\n");
        }
        treatment.setMedicines(medicines.toString());

        return treatment;
    }
}
