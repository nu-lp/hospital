package ua.lpnu.hospital.dataload;

import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import ua.lpnu.hospital.db.models.Employee;
import ua.lpnu.hospital.db.models.auth.Role;
import ua.lpnu.hospital.db.models.auth.User;
import ua.lpnu.hospital.db.models.auth.UserRole;
import ua.lpnu.hospital.db.repos.RoleRepository;
import ua.lpnu.hospital.db.repos.UserRepository;
import ua.lpnu.hospital.properties.UserProperties;

import java.util.List;
import java.util.Locale;
import java.util.NoSuchElementException;

@Component
public class SetupDataLoader implements CommandLineRunner {
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserProperties mainAdmin;

    @Autowired
    public SetupDataLoader(RoleRepository roleRepository,
                           UserRepository userRepository,
                           PasswordEncoder passwordEncoder,
                           UserProperties mainAdmin) {
        this.roleRepository = roleRepository;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.mainAdmin = mainAdmin;
    }

    @Bean
    public Faker faker() {
        return new Faker(new Locale("uk"));
    }

    @Override
    public void run(String... args) {
        setupAdmin();
    }

    @Transactional
    void setupAdmin() {
        if (userRepository.existsByUsername(mainAdmin.getUsername())) {
            return;
        }

        Role adminRole = roleRepository
                .findByName(UserRole.ROLE_ADMIN)
                .orElseThrow(NoSuchElementException::new);

        Employee admin = new Employee();
        admin.setFirstName(mainAdmin.getFirstName());
        admin.setMiddleName(mainAdmin.getMiddleName());
        admin.setLastName(mainAdmin.getLastName());
        admin.setPhone(mainAdmin.getPhone());

        User user = new User();
        user.setUsername(mainAdmin.getUsername());
        user.setPasswordHash(passwordEncoder.encode(mainAdmin.getPassword()));
        user.setIsActive(true);
        user.setRoles(List.of(adminRole));
        user.setEmployee(admin);

        admin.setUser(user);

        userRepository.save(user);
    }
}
