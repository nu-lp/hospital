package ua.lpnu.hospital.dataload;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.db.models.Analysis;
import ua.lpnu.hospital.db.models.AnalysisResult;
import ua.lpnu.hospital.db.models.Doctor;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.repos.AnalysisRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Component
@Slf4j
public class FakeAnalysis {
    private static final List<String> TYPES = List.of(
            "Загальний аналіз сечі",
            "ФГДС",
            "ЕКГ",
            "Флюорографія",
            "Загальнийаналіз  крові",
            "Комп’ютерна томографія"
    );
    private static final List<String> DESCRIPTIONS = List.of(
            "Кількість-180,0мл. \nколір -світложовтий \nпрозорість-прозора \nпитома вага – 1010 \nреакція - сл.лужна" +
                    "\n цукор, білок не виявлені. \nМікроскопічне дослідження- еритроцити відсутні, лейкоцити-0-2 в " +
                    "полі зору, епітелій плоский 0-1 в полі зору. ",
            "оглянуті стравохід, шлунок, 12пала кишка. В шлунку мало секреторної рідини, слизова оболонка атрофічна, " +
                    "складки згладжені.12 пала кишка без особливостей",
            "вольтаж достатній, ритм синусовий, правильний, електрична вісь – не відхилена. кут α 60. RR-0,78\". " +
                    "ЧСС-79 за 1 хв.  PQ-0,34\",  QRS-0,08\", QT-0,34\", СП-44%",
            "-",
            "Еритроцити 4,5 Т/л (4,0—5,1 Т/л)\nГемоглобін 135 г/л (130—160 г/л)\nКолірний показник 0,9 (0,85—1,05)\n" +
                    "Тромбоцити 280 Г/л (180—320 Г/л)\nЛейкоцити 9,6 Г/л(4,0—9,0 Г/л)\nШО Е 4мм/го",
            "Хронічний калькульозний холецистит. Візуалізуються ізоденсивні камені з периферичним нашаруванням кальцію"
    );
    private static final List<String> CONCLUSIONS = List.of(
            "-",
            "хронічний атрофічний гастрит",
            "в нормі",
            "серце і легені без патологічних змін",
            "-",
            "-"
    );
    private static final int ANALYSES = 3;
    private final AnalysisRepository analysisRepository;
    private final Random r = new Random();

    @Autowired
    public FakeAnalysis(AnalysisRepository analysisRepository) {
        this.analysisRepository = analysisRepository;
    }

    public List<Analysis> fake(List<Hospitalization> hospitalizations, List<Doctor> doctors) {
        List<Analysis> analyses = hospitalizations.stream()
                .map(h -> fakeAnalyses(h, doctors))
                .flatMap(List::stream)
                .collect(Collectors.toList());

        log.info("{} - generated analyses", analyses.size());
        return analysisRepository.saveAll(analyses);
    }

    private List<Analysis> fakeAnalyses(Hospitalization hospitalization, List<Doctor> doctors) {
        List<Analysis> analyses = new LinkedList<>();
        for (int i = 0; i < ANALYSES; i++) {
            int idx = r.nextInt(TYPES.size());
            Analysis analysis = new Analysis();
            analysis.setType(TYPES.get(idx));
            analysis.setHospitalization(hospitalization);

            if (r.nextBoolean()) {
                AnalysisResult result = new AnalysisResult();
                result.setDescription(DESCRIPTIONS.get(idx));
                result.setConclusion(CONCLUSIONS.get(idx));
                result.setAnalysis(analysis);
                result.setDoctor(doctors.get(r.nextInt(doctors.size())));

                analysis.setResult(result);
            }

            analyses.add(analysis);
        }

        return analyses;
    }
}
