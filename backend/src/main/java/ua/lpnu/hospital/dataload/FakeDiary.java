package ua.lpnu.hospital.dataload;

import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.db.models.Diary;
import ua.lpnu.hospital.db.models.DiaryTreatment;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.models.examination.Condition;
import ua.lpnu.hospital.db.repos.DiaryRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Component
@Slf4j
public class FakeDiary {
    private static final int DIARIES = 10;
    private static final int TREATMENTS = 2;
    private static final Condition[] CONDITIONS = Condition.values();
    private final Random r = new Random();
    private final Faker faker;
    private final DiaryRepository diaryRepository;

    @Autowired
    public FakeDiary(Faker faker,
                     DiaryRepository diaryRepository) {
        this.faker = faker;
        this.diaryRepository = diaryRepository;
    }

    public List<Diary> fake(List<Hospitalization> hospitalizations) {
        List<Diary> diaries = hospitalizations.stream()
                .map(this::fakeDiary)
                .flatMap(List::stream)
                .collect(Collectors.toList());

        log.info("{} - generated diaries", diaries.size());
        return diaryRepository.saveAll(diaries);
    }

    private List<Diary> fakeDiary(Hospitalization hospitalization) {
        List<Diary> diaries = new LinkedList<>();
        for (int i = 0; i < DIARIES; i++) {
            Diary diary = new Diary();
            diary.setCondition(CONDITIONS[r.nextInt(CONDITIONS.length)]);
            diary.setTemperature((float) faker.number().randomDouble(1, 35, 40));
            diary.setRespiratoryRate(faker.number().numberBetween(12, 20));
            diary.setHeartRate(faker.number().numberBetween(55, 100));
            diary.setBloodPressureDia(faker.number().numberBetween(110, 130));
            diary.setBloodPressureSys(faker.number().numberBetween(70, 90));
            diary.setComplaints("-");
            diary.setExamination("-");
            diary.setHospitalization(hospitalization);
            diary.setTreatments(fakeDiaryTreatment(diary));

            diaries.add(diary);
        }

        return diaries;
    }

    private List<DiaryTreatment> fakeDiaryTreatment(Diary diary) {
        List<DiaryTreatment> diaryTreatments = new LinkedList<>();
        for (int i = 0; i < TREATMENTS; i++) {
            DiaryTreatment treatment = new DiaryTreatment();
            treatment.setDiary(diary);
            treatment.setDescription(faker.medical().medicineName());

            diaryTreatments.add(treatment);
        }

        return diaryTreatments;
    }
}
