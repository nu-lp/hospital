package ua.lpnu.hospital.dataload;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.db.models.*;
import ua.lpnu.hospital.db.models.address.Address;
import ua.lpnu.hospital.db.models.examination.DetailedExamination;
import ua.lpnu.hospital.db.models.examination.GeneralExamination;
import ua.lpnu.hospital.db.repos.OrganSystemRepository;

import java.util.List;

@Order(1)
@Component
public class FakeData implements CommandLineRunner {
    private final OrganSystemRepository organSystemRepository;
    private final FakeAddress fakeAddress;
    private final FakePatient fakePatient;
    private final FakeSpecialization fakeSpecialization;
    private final FakeDoctor fakeDoctor;
    private final FakeGeneralExamination fakeGeneralExamination;
    private final FakeHospitalization fakeHospitalization;
    private final FakeAnamneses fakeAnamneses;
    private final FakeDiagnoses fakeDiagnoses;
    private final FakeEpicrisis fakeEpicrisis;
    private final FakeAnalysis fakeAnalysis;
    private final FakeComplaint fakeComplaint;
    private final FakeDetailedExamination fakeDetailedExamination;
    private final FakeTreatment fakeTreatment;
    private final FakeDiary fakeDiary;

    @Autowired
    public FakeData(OrganSystemRepository organSystemRepository,
                    FakeAddress fakeAddress,
                    FakePatient fakePatient,
                    FakeSpecialization fakeSpecialization,
                    FakeDoctor fakeDoctor,
                    FakeGeneralExamination fakeGeneralExamination,
                    FakeHospitalization fakeHospitalization,
                    FakeAnamneses fakeAnamneses,
                    FakeDiagnoses fakeDiagnoses,
                    FakeEpicrisis fakeEpicrisis,
                    FakeAnalysis fakeAnalysis,
                    FakeComplaint fakeComplaint,
                    FakeDetailedExamination fakeDetailedExamination,
                    FakeTreatment fakeTreatment,
                    FakeDiary fakeDiary) {
        this.organSystemRepository = organSystemRepository;
        this.fakeAddress = fakeAddress;
        this.fakePatient = fakePatient;
        this.fakeSpecialization = fakeSpecialization;
        this.fakeDoctor = fakeDoctor;
        this.fakeGeneralExamination = fakeGeneralExamination;
        this.fakeHospitalization = fakeHospitalization;
        this.fakeAnamneses = fakeAnamneses;
        this.fakeDiagnoses = fakeDiagnoses;
        this.fakeEpicrisis = fakeEpicrisis;
        this.fakeAnalysis = fakeAnalysis;
        this.fakeComplaint = fakeComplaint;
        this.fakeDetailedExamination = fakeDetailedExamination;
        this.fakeTreatment = fakeTreatment;
        this.fakeDiary = fakeDiary;
    }

    @Override
    public void run(String... args) {
        if (args.length == 0 || !args[0].equals("fake")) {
            return;
        }
        fakeData();
    }

    public void fakeData() {
        List<OrganSystem> organSystems = organSystemRepository.findAll();
        List<Address> addresses = fakeAddress.fake();
        List<Patient> patients = fakePatient.fake(addresses);
        List<Specialization> specializations = fakeSpecialization.fake();
        List<Doctor> doctors = fakeDoctor.fake(specializations);
        List<Hospitalization> hospitalizations = fakeHospitalization.fake(patients, doctors);
        List<GeneralExamination> generalExaminations = fakeGeneralExamination.fake(hospitalizations);
        List<Anamnesis> anamneses = fakeAnamneses.fake(hospitalizations);
        List<Diagnosis> diagnoses = fakeDiagnoses.fake(hospitalizations);
        List<Epicrisis> epicrises = fakeEpicrisis.fake(hospitalizations);
        List<Analysis> analyses = fakeAnalysis.fake(hospitalizations, doctors);
        List<Complaint> complaints = fakeComplaint.fake(hospitalizations, organSystems);
        List<DetailedExamination> detailedExaminations = fakeDetailedExamination.fake(hospitalizations, organSystems);
        List<Treatment> treatments = fakeTreatment.fake(hospitalizations);
        List<Diary> diaries = fakeDiary.fake(hospitalizations);
    }
}
