package ua.lpnu.hospital.dataload;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.db.models.Epicrisis;
import ua.lpnu.hospital.db.models.EpicrisisType;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.models.examination.Condition;
import ua.lpnu.hospital.db.repos.EpicrisisRepository;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Component
@Slf4j
public class FakeEpicrisis {
    private static final List<String> RECOMMENDATIONS = List.of(
            "Хворий виписаний додому. Рекомендовано кинути курити, обмежити вживання алкоголю, не допускати " +
                    "переохолоджень, зменшити фізичне навантаження.",
            "На тлі проведеного лікування стан хворого покращився, зменшилася лущення висипань, зменшився свербіж в" +
                    " області висипань. Рекомендовано продовжити призначене лікування.",
            "Стан пацієнта прогресивно погіршувався, приєдналися явища нестабільної гемодинаміки, яка вимагала " +
                    "високих доз вазопресорів. 06.03.18 р о 16-30 наступила смерть.",
            "Термін використання знімного протеза від 1 року до 2 років при первинному протезуванні та виконанні " +
                    "і виконанні порад і настанов лікаря."
    );
    private static final Condition[] CONDITIONS = Condition.values();
    private static final EpicrisisType[] EPICRISIS_TYPES = EpicrisisType.values();
    private final Random r = new Random();
    private final EpicrisisRepository epicrisisRepository;

    @Autowired
    public FakeEpicrisis(EpicrisisRepository epicrisisRepository) {
        this.epicrisisRepository = epicrisisRepository;
    }

    public List<Epicrisis> fake(List<Hospitalization> hospitalizations) {
        List<Epicrisis> epicrises = hospitalizations.stream()
                .filter(h -> h.getExtractDate() != null)
                .map(this::fakeEpicrisis)
                .collect(Collectors.toList());

        log.info("{} - generated epicrises", epicrises.size());
        return epicrisisRepository.saveAll(epicrises);
    }

    private Epicrisis fakeEpicrisis(Hospitalization hospitalization) {
        Epicrisis epicrisis = new Epicrisis();
        epicrisis.setType(EPICRISIS_TYPES[r.nextInt(EPICRISIS_TYPES.length)]);
        epicrisis.setCondition(CONDITIONS[r.nextInt(CONDITIONS.length)]);
        epicrisis.setRecommendation(RECOMMENDATIONS.get(r.nextInt(RECOMMENDATIONS.size())));
        epicrisis.setHospitalization(hospitalization);

        return epicrisis;
    }
}
