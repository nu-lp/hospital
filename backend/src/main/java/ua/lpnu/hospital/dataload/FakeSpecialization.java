package ua.lpnu.hospital.dataload;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.db.models.Specialization;
import ua.lpnu.hospital.db.repos.SpecializationRepository;

import java.util.List;
import java.util.stream.Collectors;

@Component
@Slf4j
public class FakeSpecialization {
    private static final List<String> SPECIALIZATION_NAMES = List.of(
            "акушер-гінеколог",
            "алерголог",
            "анестезіолог",
            "бактеріолог",
            "вірусолог",
            "гастроентеролог",
            "гематолог",
            "генетик",
            "геріатр",
            "гінеколог-онколог",
            "дерматовенеролог",
            "дієтолог",
            "ендокринолог",
            "ендоскопіст",
            "епідеміолог",
            "імунолог",
            "інфекціоніст",
            "кардіолог",
            "лаборант",
            "нарколог",
            "невропатолог",
            "нефролог",
            "нейрохірург",
            "онколог",
            "ортопед-травматолог",
            "отоларинголог",
            "офтальмолог",
            "паразитолог",
            "педіатр",
            "психіатр",
            "стоматолог",
            "сурдолог",
            "терапевт",
            "токсиколог",
            "уролог",
            "ультразвукової діагностики",
            "фтизіатр",
            "хірург"
    );
    private final SpecializationRepository specializationRepository;


    @Autowired
    public FakeSpecialization(SpecializationRepository specializationRepository) {
        this.specializationRepository = specializationRepository;
    }

    public List<Specialization> fake() {
        List<Specialization> specializations = SPECIALIZATION_NAMES.stream()
                .map(this::fakeSpecialization)
                .collect(Collectors.toList());
        log.info("{} - generated specializations", specializations.size());

        return specializationRepository.saveAll(specializations);
    }

    private Specialization fakeSpecialization(String name) {
        Specialization s = new Specialization();
        s.setName(name);

        return s;
    }
}
