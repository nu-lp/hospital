package ua.lpnu.hospital.dataload;

import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.db.models.Doctor;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.models.HospitalizationType;
import ua.lpnu.hospital.db.models.Patient;
import ua.lpnu.hospital.db.repos.HospitalizationRepository;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@Component
@Slf4j
public class FakeHospitalization {
    private static final int HOSPITALIZATIONS = 4;
    private static final List<String> EMPLOYMENTS = List.of(
            "Студент",
            "Агент морський",
            "Агент податковий",
            "Агроном",
            "Аеролог",
            "Вибірник каменів",
            "Винороб",
            "Випалювач матеріалів",
            "Ворожка",
            "Вставник камер",
            "Газозварник",
            "Генеральний конструктор",
            "Географ",
            "Гідрограф",
            "Експерт-кінолог",
            "Електромеханік з ліфтів",
            "Електромеханік судновий",
            "Електрослюсар з проходки",
            "Інженер з охорони праці",
            "Історик",
            "Камбузник",
            "Капітан",
            "Машиніст прибиральних машин",
            "Монтер колії",
            "Начальник слідчої частини",
            "Носій",
            "Оббивальник",
            "Оброблювач води",
            "Прасувальник",
            "Практичний психолог",
            "Продюсер",
            "Розкатник тканини",
            "Скляр",
            "Соціолог",
            "Сріблильник",
            "Суфлер",
            "Технік",
            "Токар",
            "Трунар",
            "Учений секретар",
            "Хімік",
            "Хмеляр",
            "Юрист",
            "Штурман"
    );
    private final Random r = new Random();
    private final Faker faker;
    private final HospitalizationRepository hospitalizationRepository;

    @Autowired
    public FakeHospitalization(Faker faker,
                               HospitalizationRepository hospitalizationRepository) {
        this.faker = faker;
        this.hospitalizationRepository = hospitalizationRepository;
    }

    public List<Hospitalization> fake(List<Patient> patients, List<Doctor> doctors) {
        List<Hospitalization> hospitalizations = new LinkedList<>();

        for (Patient p : patients) {
            for (int i = 0; i < r.nextInt(HOSPITALIZATIONS) + 1; i++) {
                hospitalizations.add(fakeHospitalization(
                        p,
                        doctors.get(r.nextInt(doctors.size())),
                        true
                ));
            }
            if (r.nextBoolean()) {
                hospitalizations.add(fakeHospitalization(
                        p,
                        doctors.get(r.nextInt(doctors.size())),
                        false
                ));
            }
        }

        log.info("{} - generated hospitalizations", hospitalizations.size());
        return hospitalizationRepository.saveAll(hospitalizations);
    }

    private Hospitalization fakeHospitalization(Patient patient, Doctor doctor, boolean extracted) {
        Hospitalization hospitalization = new Hospitalization();
        Date hospitalizationDate = faker.date().past(1825, TimeUnit.DAYS);
        Date extractDate = faker.date().future(1800, TimeUnit.DAYS, hospitalizationDate);
        Date diseaseDate = faker.date().past(60, TimeUnit.DAYS, hospitalizationDate);
        hospitalization.setHospitalizationDate(new java.sql.Date(
                hospitalizationDate.getTime()
        ).toLocalDate());
        hospitalization.setDiseaseDate(new java.sql.Date(
                diseaseDate.getTime()
        ).toLocalDate());
        if (extracted) {
            hospitalization.setExtractDate(new java.sql.Date(
                    extractDate.getTime()
            ).toLocalDate());
        }
        hospitalization.setEmployment(EMPLOYMENTS.get(r.nextInt(EMPLOYMENTS.size())));
        hospitalization.setType(faker.bool().bool() ? HospitalizationType.PLANNED : HospitalizationType.URGENT);
        hospitalization.setReferral(faker.name().fullName());
        hospitalization.setPatient(patient);
        hospitalization.setDoctor(doctor);

        return hospitalization;
    }
}
