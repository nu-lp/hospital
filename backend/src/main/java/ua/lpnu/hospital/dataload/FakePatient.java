package ua.lpnu.hospital.dataload;

import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.db.models.Patient;
import ua.lpnu.hospital.db.models.address.Address;
import ua.lpnu.hospital.db.repos.PatientRepository;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

@Component
@Slf4j
public class FakePatient {
    private static final int PATIENTS = 1000;
    private final Random random = new Random();
    private final Faker faker;
    private final PatientRepository patientRepository;

    @Autowired
    public FakePatient(Faker faker,
                       PatientRepository patientRepository
    ) {
        this.faker = faker;
        this.patientRepository = patientRepository;
    }

    public List<Patient> fake(List<Address> addresses) {
        List<Patient> patients = new LinkedList<>();
        for (int i = 0; i < PATIENTS; i++) {
            patients.add(fakePatient(
                    addresses.get(random.nextInt(addresses.size()))
            ));
        }
        patients = patientRepository.saveAll(patients);
        log.info("{} - patients generated", patients.size());

        return patients;
    }

    private Patient fakePatient(Address address) {
        Patient patient = new Patient();
        boolean isMale = faker.bool().bool();
        patient.setFirstName(fakeFirstName(isMale));
        patient.setLastName(fakeLastName(isMale));
        patient.setMiddleName(fakeMiddleName(isMale));
        patient.setPhone(faker.phoneNumber().phoneNumber());
        patient.setBirthday(new java.sql.Date(
                faker.date().birthday(5, 85).getTime()
        ).toLocalDate());
        patient.setHomePhone(faker.phoneNumber().phoneNumber());
        patient.setIsMale(isMale);
        patient.setAddress(address);

        return patient;
    }

    private String fakeFirstName(boolean isMale) {
        return faker.resolve(isMale ? "name.male_first_name" : "name.female_first_name");
    }

    private String fakeLastName(boolean isMale) {
        return faker.resolve(isMale ? "name.male_last_name" : "name.female_last_name");
    }

    private String fakeMiddleName(boolean isMale) {
        return faker.resolve(isMale ? "name.male_middle_name" : "name.female_middle_name");
    }
}
