package ua.lpnu.hospital.dataload;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.models.examination.*;
import ua.lpnu.hospital.db.repos.GeneralExaminationRepository;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Component
@Slf4j
public class FakeGeneralExamination {
    private static final Condition[] CONDITIONS = Condition.values();
    private static final Consciousness[] CONSCIOUSNESSES = Consciousness.values();
    private static final String[] POSITIONS = {"активне", "пасивне", "лежаче"};
    private static final Constitution[] CONSTITUTIONS = Constitution.values();
    private static final Tone[] TONES = Tone.values();
    private static final Temperature[] TEMPERATURES = Temperature.values();
    private final Random r = new Random();
    private final GeneralExaminationRepository generalExaminationRepository;


    @Autowired
    public FakeGeneralExamination(GeneralExaminationRepository generalExaminationRepository) {
        this.generalExaminationRepository = generalExaminationRepository;
    }

    public List<GeneralExamination> fake(List<Hospitalization> hospitalizations) {

        List<GeneralExamination> generalExaminations = hospitalizations.stream()
                .map(this::fakeGeneralExamination)
                .collect(Collectors.toList());

        log.info("{} - generated general examinations", generalExaminations.size());

        return generalExaminationRepository.saveAll(generalExaminations);
    }

    private GeneralExamination fakeGeneralExamination(Hospitalization hospitalization) {
        GeneralExamination ge = new GeneralExamination();
        ge.setCondition(CONDITIONS[r.nextInt(CONDITIONS.length)]);
        ge.setConsciousness(CONSCIOUSNESSES[r.nextInt(CONSCIOUSNESSES.length)]);
        ge.setPosition(POSITIONS[r.nextInt(POSITIONS.length)]);
        ge.setIsBodyProportional(r.nextBoolean());
        ge.setConstitution(CONSTITUTIONS[r.nextInt(CONSTITUTIONS.length)]);
        ge.setHeight(r.nextInt(70) + 120f);
        ge.setWeight(r.nextInt(50) + 40f);
        ge.setTemperature(TEMPERATURES[r.nextInt(TEMPERATURES.length)]);
        ge.setTone(TONES[r.nextInt(TONES.length)]);
        ge.setHospitalization(hospitalization);

        return ge;
    }
}
