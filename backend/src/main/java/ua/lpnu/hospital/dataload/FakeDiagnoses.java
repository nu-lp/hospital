package ua.lpnu.hospital.dataload;

import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.db.models.Diagnosis;
import ua.lpnu.hospital.db.models.DiagnosisType;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.repos.DiagnosisRepository;

import java.time.ZoneId;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@Slf4j
public class FakeDiagnoses {
    private static final List<String> DIAGNOSES = List.of(
            "Артеріальна гіпертензія – підвищення систолічного артеріального тиску (САТ) до 140 мм рт.ст. i вище " +
                    "або діастолічного артеріального тиску (ДАТ) до 90 мм рт.ст. i вище, якщо таке підвищення є " +
                    "стабільним, тобто підтверджується при повторних вимірюваннях артеріального тиску (AT) (не менш, " +
                    "ніж 2-3 рази у різні дні протягом 4 тижнів).",
            "Ревматоїдний артрит (РА) – аутоімунне системне захворювання сполучної тканини з прогресуючим ураженням" +
                    " суглобів ерозивно-деструктивним процесом.",
            "Цироз печінки (ЦП) – хронічне прогресуюче дифузне захворювання печінки, яке характеризується порушенням" +
                    " архітектоніки печінки, та її судинної системи, розвитком портальної гіпертензії та " +
                    "печінково-клітинної недо-статності.",
            "Гіпотиреоз – клінічний синдром, обумовлений тривалою стійкою недостатністю гормонів щитоподібної залози, " +
                    "або зниженням ефекту їх дії на тканинному рівні.",
            "Виразкова хвороба шлунка, активна фаза, середнього ступеня тяжкості, гостра виразка шлунка, асоційована" +
                    " з НР, з вираженим больовим та диспептичним синдромами.",
            "Виразкова хвороба 12палої кишки, активна фаза, середньоїтяжкості, хронічна рецидивуюча виразка цибулини " +
                    "12палої кишки з вираженим больовим і диспептичним синдромами, асоційована з НР.",
            "Жовчно – кам’яна хвороба. Хронічний калькульозний холецистит в фазі загострення."
    );
    private final Random random = new Random();
    private final Faker faker;
    private final DiagnosisRepository diagnosisRepository;

    @Autowired
    public FakeDiagnoses(Faker faker,
                         DiagnosisRepository diagnosisRepository) {
        this.faker = faker;
        this.diagnosisRepository = diagnosisRepository;
    }

    public List<Diagnosis> fake(List<Hospitalization> hospitalizations) {
        List<Diagnosis> diagnoses = hospitalizations.stream()
                .map(this::fakeDiagnoses)
                .flatMap(List::stream)
                .collect(Collectors.toList());

        log.info("{} - generated diagnoses", diagnoses.size());
        return diagnosisRepository.saveAll(diagnoses);
    }

    private List<Diagnosis> fakeDiagnoses(Hospitalization hospitalization) {
        Date hospitalizationDate = Date.from(
                hospitalization.getHospitalizationDate().atStartOfDay(ZoneId.systemDefault()).toInstant()
        );
        List<Diagnosis> diagnoses = new LinkedList<>();
        for (DiagnosisType d : DiagnosisType.values()) {
            Diagnosis diagnosis = new Diagnosis();
            diagnosis.setType(d);
            diagnosis.setDescription(DIAGNOSES.get(random.nextInt(DIAGNOSES.size())));
            diagnosis.setDate(new java.sql.Date(
                    faker.date().future(3, TimeUnit.DAYS, hospitalizationDate).getTime()
            ).toLocalDate());
            diagnosis.setMainDisease(faker.medical().diseaseName());
            diagnosis.setComplicationDisease(faker.medical().diseaseName());
            diagnosis.setConcomitantDisease(faker.medical().diseaseName());
            diagnosis.setHospitalization(hospitalization);

            diagnoses.add(diagnosis);
        }

        return diagnoses;
    }
}
