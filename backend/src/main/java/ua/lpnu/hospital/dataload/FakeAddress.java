package ua.lpnu.hospital.dataload;

import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.db.models.address.Address;
import ua.lpnu.hospital.db.models.address.City;
import ua.lpnu.hospital.db.models.address.Street;
import ua.lpnu.hospital.db.repos.address.AddressRepository;
import ua.lpnu.hospital.db.repos.address.CityRepository;
import ua.lpnu.hospital.db.repos.address.StreetRepository;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Component
@Slf4j
public class FakeAddress {
    private static final int CITIES = 10;
    private static final int STREETS = 10;
    private static final int ADDRESSES = 5;
    private final Faker faker;
    private final AddressRepository addressRepository;
    private final StreetRepository streetRepository;
    private final CityRepository cityRepository;

    @Autowired
    public FakeAddress(Faker faker,
                       AddressRepository addressRepository,
                       StreetRepository streetRepository,
                       CityRepository cityRepository
    ) {
        this.faker = faker;
        this.addressRepository = addressRepository;
        this.streetRepository = streetRepository;
        this.cityRepository = cityRepository;
    }

    public List<Address> fake() {
        List<Address> fakeAddresses = fakeCities().stream()
                .map(this::fakeStreets)
                .flatMap(List::stream)
                .map(this::fakeAddresses)
                .flatMap(List::stream)
                .collect(Collectors.toList());

        log.info("{} - addresses generated", fakeAddresses.size());
        return fakeAddresses;
    }

    private List<City> fakeCities() {
        Set<String> citiesNames = new HashSet<>(CITIES);
        while (citiesNames.size() < CITIES) {
            citiesNames.add(faker.address().cityName());
        }
        List<City> cities = citiesNames.stream().map(c -> {
            City city = new City();
            city.setName(c);
            return city;
        }).collect(Collectors.toList());

        return cityRepository.saveAll(cities);
    }

    private List<Street> fakeStreets(City city) {
        Set<String> streetNames = new HashSet<>();
        while (streetNames.size() < STREETS) {
            streetNames.add(faker.address().streetName());
        }
        List<Street> streets = streetNames.stream().map(s -> {
            Street street = new Street();
            street.setName(s);
            street.setCity(city);
            return street;
        }).collect(Collectors.toList());

        return streetRepository.saveAll(streets);
    }

    private List<Address> fakeAddresses(Street street) {
        List<Address> addresses = new ArrayList<>();
        for (int i = 0; i < ADDRESSES; i++) {
            Address address = new Address();
            address.setHouseNumber(Integer.parseInt(faker.address().buildingNumber()));
            address.setStreet(street);
            address.setZipCode(faker.address().zipCode());

            addresses.add(address);
        }

        return addressRepository.saveAll(addresses);
    }
}
