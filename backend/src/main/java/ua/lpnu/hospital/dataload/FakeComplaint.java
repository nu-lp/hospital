package ua.lpnu.hospital.dataload;

import com.github.javafaker.Faker;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.db.models.Complaint;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.models.OrganSystem;
import ua.lpnu.hospital.db.repos.ComplaintRepository;

import java.time.ZoneId;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

@Component
@Slf4j
public class FakeComplaint {
    private static final List<String> COMPLAINTS = List.of(
            "почуття тяжкості та переповнення в епігастральній ділянці",
            "відрижка повітрям з неприємним запахом",
            "нудота після прийому їжі",
            "здуття живота",
            "схильність до проносів",
            "зниження апетиту",
            "біль ниючого характеру в пілородуоденальній ділянці, виникаючий через 1,5 – 2 години після їжі і вночі ," +
                    " що проходить після прийому їжі та антацидів.",
            "кислий присмак в роті, загальнаслабкість",
            "тупі болі в правому підребір`ї, що іррадіюють у епігастрій та під праву лопатку, виникають через 40-90" +
                    " хвилин після їжі, особливо жирної, смаженої чи гострої",
            "гіркота у роті, нудота",
            "шлунковий дискомфорт, пожовтіння шкіри та склер",
            "дратівливість, безсоння, безпричинні носові кровотечі, зниження ваги",
            "збільшення живота в об’ємі",
            "запаморочення; головний біль у потилиці, який супроводжується мерехтінням перед очима, шумом у вухах, " +
                    "нудотою, іноді блюванням",
            "задишка при фізичному навантаженні; зниження апетиту",
            "постійний тупий біль в ділянці нирок, без іррадіації"
    );
    private static final int COMPLAINTS_CNT = 5;
    private final Random r = new Random();
    private final Faker faker;
    private final ComplaintRepository complaintRepository;

    @Autowired
    public FakeComplaint(Faker faker,
                         ComplaintRepository complaintRepository) {
        this.faker = faker;
        this.complaintRepository = complaintRepository;
    }

    public List<Complaint> fake(List<Hospitalization> hospitalizations, List<OrganSystem> organSystems) {
        List<Complaint> complaints = hospitalizations.stream()
                .map(h -> fakeComplaint(h, organSystems))
                .flatMap(List::stream)
                .collect(Collectors.toList());

        log.info("{} - generated complaints", complaints.size());
        return complaintRepository.saveAll(complaints);
    }

    private List<Complaint> fakeComplaint(Hospitalization hospitalization, List<OrganSystem> organSystems) {
        Date hospitalizationDate = Date.from(
                hospitalization.getHospitalizationDate().atStartOfDay(ZoneId.systemDefault()).toInstant()
        );
        List<Complaint> complaints = new LinkedList<>();

        for (int i = 0; i < COMPLAINTS_CNT; i++) {
            Complaint complaint = new Complaint();
            complaint.setStartDate(new java.sql.Date(
                    faker.date().past(30, TimeUnit.DAYS, hospitalizationDate).getTime()
            ).toLocalDate());
            complaint.setDescription(COMPLAINTS.get(r.nextInt(COMPLAINTS.size())));
            complaint.setHospitalization(hospitalization);
            complaint.setOrganSystem(organSystems.get(r.nextInt(organSystems.size())));

            complaints.add(complaint);
        }
        return complaints;
    }
}
