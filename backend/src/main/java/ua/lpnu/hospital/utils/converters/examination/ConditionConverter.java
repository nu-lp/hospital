package ua.lpnu.hospital.utils.converters.examination;

import ua.lpnu.hospital.db.models.examination.Condition;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ConditionConverter implements AttributeConverter<Condition, String> {
    @Override
    public String convertToDatabaseColumn(Condition attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.code;
    }

    @Override
    public Condition convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }
        return Condition.valueOfCode(dbData);
    }
}
