package ua.lpnu.hospital.utils.converters;

import ua.lpnu.hospital.db.models.AnamnesisType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class AnamnesisTypeConverter implements AttributeConverter<AnamnesisType, String> {
    @Override
    public String convertToDatabaseColumn(AnamnesisType attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.code;
    }

    @Override
    public AnamnesisType convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }
        return AnamnesisType.valueOfCode(dbData);
    }
}
