package ua.lpnu.hospital.utils.converters.examination;

import ua.lpnu.hospital.db.models.examination.Tone;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ToneConverter implements AttributeConverter<Tone, String> {
    @Override
    public String convertToDatabaseColumn(Tone attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.code;
    }

    @Override
    public Tone convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }
        return Tone.valueOfCode(dbData);
    }
}
