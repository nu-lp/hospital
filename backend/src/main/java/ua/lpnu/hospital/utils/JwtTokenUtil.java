package ua.lpnu.hospital.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.properties.JwtProperties;

import javax.servlet.http.HttpServletRequest;
import java.util.Base64;
import java.util.Date;
import java.util.function.Function;

@Component
public class JwtTokenUtil {
    private final JwtProperties jwtProperties;
    private final String encodedSecret;

    @Autowired
    public JwtTokenUtil(JwtProperties jwtProperties) {
        this.jwtProperties = jwtProperties;
        encodedSecret = Base64.getEncoder()
                .encodeToString(jwtProperties
                        .getSecret()
                        .getBytes()
                );
    }

    public <T> T getClaim(String token, Function<Claims, T> resolverClaim) {
        return resolverClaim.apply(getAllClaims(token));
    }

    public String getUsername(String token) {
        return getClaim(token, Claims::getSubject);
    }

    public Date getExpirationDate(String token) {
        return getClaim(token, Claims::getExpiration);
    }

    public boolean isTokenValid(String token, UserDetails userDetails) {
        String username = getUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    public String generateToken(UserDetails userDetails) {
        return generateToken(Jwts.claims(), userDetails.getUsername());
    }

    public String extractToken(HttpServletRequest request) {
        String token = request.getHeader(jwtProperties.getAuthHeader());
        if (token == null || !token.startsWith(jwtProperties.getPrefix())) {
            return null;
        }
        return token.substring(jwtProperties.getPrefix().length());
    }

    private Claims getAllClaims(String token) {
        return Jwts.parser()
                .setSigningKey(encodedSecret)
                .parseClaimsJws(token)
                .getBody();
    }

    private boolean isTokenExpired(String token) {
        Date expiresAt = getExpirationDate(token);
        return expiresAt.before(new Date());
    }

    private String generateToken(Claims claims, String subject) {
        Date issuedAt = new Date(System.currentTimeMillis());
        Date expiresAt = new Date(issuedAt.getTime() + jwtProperties.getValidity());
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(subject)
                .setIssuedAt(issuedAt)
                .setExpiration(expiresAt)
                .signWith(SignatureAlgorithm.HS512, encodedSecret)
                .compact();
    }
}
