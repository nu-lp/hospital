package ua.lpnu.hospital.utils.converters.examination;

import ua.lpnu.hospital.db.models.examination.Temperature;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class TemperatureConverter implements AttributeConverter<Temperature, String> {
    @Override
    public String convertToDatabaseColumn(Temperature attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.code;
    }

    @Override
    public Temperature convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }
        return Temperature.valueOfCode(dbData);
    }
}
