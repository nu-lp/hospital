package ua.lpnu.hospital.utils.converters;

import ua.lpnu.hospital.db.models.HospitalizationType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class HospitalizationTypeConverter implements AttributeConverter<HospitalizationType, String> {
    @Override
    public String convertToDatabaseColumn(HospitalizationType attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.code;
    }

    @Override
    public HospitalizationType convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }
        return HospitalizationType.valueOfCode(dbData);
    }
}
