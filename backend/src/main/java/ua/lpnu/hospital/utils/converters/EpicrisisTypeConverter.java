package ua.lpnu.hospital.utils.converters;

import ua.lpnu.hospital.db.models.EpicrisisType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class EpicrisisTypeConverter implements AttributeConverter<EpicrisisType, String> {
    @Override
    public String convertToDatabaseColumn(EpicrisisType attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.code;
    }

    @Override
    public EpicrisisType convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }
        return EpicrisisType.valueOfCode(dbData);
    }
}
