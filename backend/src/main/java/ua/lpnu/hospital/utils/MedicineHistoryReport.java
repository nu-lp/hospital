package ua.lpnu.hospital.utils;

import com.itextpdf.io.font.PdfEncodings;
import com.itextpdf.kernel.font.PdfFontFactory;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.*;
import com.itextpdf.layout.property.AreaBreakType;
import com.itextpdf.layout.property.ListNumberingType;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import ua.lpnu.hospital.db.models.*;
import ua.lpnu.hospital.db.models.examination.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.stream.Collectors;

public class MedicineHistoryReport {
    public static final String FONT_PATH = "fonts/roboto/Roboto.ttf";

    private static final Map<Condition, String> CONDITIONS = Map.of(
            Condition.MODERATE, "Середньої тяжкості",
            Condition.SATISFACTORY, "Задовільний",
            Condition.SERIOUS, "Тяжкий"
    );
    private static final Map<Consciousness, String> CONSCIOUSNESS = Map.of(
            Consciousness.CLEAR, "Ясна",
            Consciousness.CONFUSED, "Сплутана",
            Consciousness.STUPOR, "Ступор",
            Consciousness.SOPOR, "Ясна",
            Consciousness.COMA, "Кома",
            Consciousness.DELIRIUM, "Марення",
            Consciousness.HALLUCINATIONS, "Галюцинації"
    );
    private static final Map<Constitution, String> CONSTITUTIONS = Map.of(
            Constitution.ASTHENIC, "Астенічна",
            Constitution.NORMOSTENIC, "Нормостенічна",
            Constitution.HYPERSTENIC, "Гіперстенічна"
    );
    private static final Map<Tone, String> TONES = Map.of(
            Tone.INCREASED, "Підвищений",
            Tone.PRESERVED, "Збережений",
            Tone.REDUCED, "Понижений"
    );
    private static final Map<Temperature, String> TEMPERATURES = Map.of(
            Temperature.REDUCED, "Знижена",
            Temperature.NORMAL, "Нормальна",
            Temperature.SUBFEBRILE, "Субфебрильна",
            Temperature.FEBRILE, "Фебрильна",
            Temperature.PYRETIC, "Піретична",
            Temperature.HYPERPYREXIA, "Гіперпіретична"
    );

    private static final Map<HospitalizationType, String> HOSPITALIZATION_TYPES = Map.of(
            HospitalizationType.PLANNED, "Планова",
            HospitalizationType.URGENT, "Ургентна"
    );
    private static final Map<EpicrisisType, String> EPICRISIS_TYPES = Map.of(
            EpicrisisType.DEATH, "Посмертний",
            EpicrisisType.EXTRACT, "Виписний",
            EpicrisisType.REDIRECTION, "Перенаправлення"
    );

    static {
        PdfFontFactory.register(FONT_PATH);
    }

    public static byte[] generatePdf(Hospitalization h) {
        Map<OrganSystem, java.util.List<Complaint>> complaints = h.getComplaints()
                .stream()
                .collect(Collectors.groupingBy(Complaint::getOrganSystem));
        Map<OrganSystem, java.util.List<DetailedExamination>> detailedExaminations = h.getDetailedExaminations()
                .stream()
                .collect(Collectors.groupingBy(DetailedExamination::getOrganSystem));
        Map<DiagnosisType, Diagnosis> diagnosisMap = h.getDiagnoses()
                .stream()
                .collect(Collectors.toMap(Diagnosis::getType, d -> d));

        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PdfDocument pdfDoc = new PdfDocument(new PdfWriter(os));
        Document document = new Document(pdfDoc, PageSize.A4);

        initDocument(document);
        addTitlePage(document, h);
        addPassportPage(document, h);
        addComplaintsPage(document, complaints);
        addAnamnesesPage(document, h.getAnamneses());
        addExaminationPage(document, h.getGeneralExamination(), detailedExaminations);
        addDiagnosis(document, "ДІАГНОЗ ПРИ ПОСТУПЛЕННІ", diagnosisMap.get(DiagnosisType.ADMITTANCE));
        addDiagnosis(document, "ПОПЕРЕДНІЙ ДІАГНОЗ", diagnosisMap.get(DiagnosisType.PRELIMINARY));
        pageBreak(document);
        addAnalyses(document, h.getAnalyses());
        addDiagnosis(document, "КЛІНІЧНИЙ ДІАГНОЗ", diagnosisMap.get(DiagnosisType.FINAL));
        addTreatment(document, h.getTreatment());
        addDiary(document, h.getDiaries());
        addEpicrisis(document, h.getEpicrisis());
        addSignature(document);

        document.close();

        return os.toByteArray();
    }

    private static void initDocument(Document document) {
        try {
            document.setFont(PdfFontFactory.createFont(FONT_PATH, PdfEncodings.IDENTITY_H, true));
        } catch (IOException e) {
            e.printStackTrace();
        }
        document.setFontSize(14);
    }

    private static void addTitlePage(Document document, Hospitalization h) {
        Paragraph preface = new Paragraph();

        addEmptyLine(preface, 5);
        Paragraph title = new Paragraph("ІСТОРІЯ ХВОРОБИ\nСТАЦІОНАРНОГО ХВОРОГО");
        title.setWidth(UnitValue.createPercentValue(100f));
        title.setTextAlignment(TextAlignment.CENTER);
        title.setFontSize(18);
        preface.add(title);
        addEmptyLine(preface, 2);

        Patient p = h.getPatient();
        String midName = p.getMiddleName();
        addTextField(
                preface,
                "Прізвище, ім'я, по-батькові хворого ",
                String.format("%s %s %s", p.getLastName(), p.getFirstName(), midName != null ? midName : "")
        );

        preface.add(new Text("\nКлінічний діагноз\n"));

        Diagnosis fin = h.getDiagnoses()
                .stream()
                .filter(d -> d.getType() == DiagnosisType.FINAL)
                .findFirst()
                .orElseThrow();
        addTextField(
                preface,
                "Основне захворювання ",
                fin.getMainDisease()
        );

        addTextField(
                preface,
                "Ускладнення основного захворювання ",
                fin.getComplicationDisease()
        );

        addTextField(
                preface,
                "Супутнє захворювання ",
                fin.getConcomitantDisease()
        );

        addEmptyLine(preface, 1);
        Doctor d = h.getDoctor();
        midName = d.getMiddleName();
        addTextField(
                preface,
                "Куратор ",
                String.format(
                        "%s %s %s, %s",
                        d.getLastName(),
                        d.getFirstName(),
                        midName != null ? midName : "",
                        d.getSpecialization().getName()
                )
        );

        addTextField(
                preface,
                "Початок курації ",
                formatDate(h.getHospitalizationDate())
        );

        addTextField(
                preface,
                "Закінчення курації ",
                formatDate(h.getExtractDate())
        );

        document.add(preface);
        pageBreak(document);
    }

    private static void addPassportPage(Document document, Hospitalization h) {
        Paragraph passport = new Paragraph();
        addHeader(passport, "ПАСПОРТНІ ДАНІ");

        Patient p = h.getPatient();
        String fullName = p.getLastName() + ' ' + p.getFirstName();
        if (p.getMiddleName() != null) {
            fullName += ' ' + p.getMiddleName();
        }
        String address = String.format("%s, %s %d, %s",
                p.getAddress().getStreet().getCity().getName(),
                p.getAddress().getStreet().getName(),
                p.getAddress().getHouseNumber(),
                p.getAddress().getZipCode());
        long daysSpent = ChronoUnit.DAYS.between(h.getHospitalizationDate(), h.getExtractDate());
        addTextField(passport, "Прізвище, ім'я, по-батькові", fullName);
        addTextField(passport, "Номер телефону", p.getPhone());
        addTextField(passport, "Домашній телефон", p.getHomePhone());
        addTextField(passport, "Дата народження", formatDate(p.getBirthday()));
        addTextField(passport, "Стать", p.getIsMale() ? "чоловіча" : "жіноча");
        addTextField(passport, "Адреса проживання", address);
        addTextField(passport, "Дата госпіталізації", formatDate(h.getHospitalizationDate()));
        addTextField(passport, "Дата початку захворювання", formatDate(h.getDiseaseDate()));
        addTextField(passport, "Дата виписки", formatDate(h.getExtractDate()));
        addTextField(passport, "Проведено ліжко-днів", String.valueOf(daysSpent));
        addTextField(passport, "Місце роботи, посада (навчання)", h.getEmployment());
        addTextField(passport, "Тип госпіталізації", HOSPITALIZATION_TYPES.get(h.getType()));
        addTextField(passport, "Скеровано", h.getReferral());

        document.add(passport);
        pageBreak(document);
    }

    private static String formatDate(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }

    private static String formatDate(LocalDateTime dateTime) {
        return dateTime.format(DateTimeFormatter.ofPattern("dd.MM.yyyy"));
    }

    private static void addComplaintsPage(Document document, Map<OrganSystem, java.util.List<Complaint>> complaintsMap) {
        Paragraph complaints = new Paragraph();
        addHeader(complaints, "СКАРГИ");

        complaintsMap.forEach((o, list) -> {
            complaints.add(new Text(o.getName() + "\n").setBold());
            for (Complaint com : list) {
                Paragraph c = new Paragraph();
                c.add(new Text(formatDate(com.getStartDate()) + " - "));
                c.add(new Text(com.getDescription()));
                complaints.add(c);
                addEmptyLine(complaints, 1);
            }
        });

        document.add(complaints);
        pageBreak(document);
    }

    private static void addAnamnesesPage(Document document, java.util.List<Anamnesis> anamneses) {
        Paragraph diseaseAnamnesis = new Paragraph();
        addHeader(diseaseAnamnesis, "ІСТОРІЯ ЗАХВОРЮВАННЯ (ANAMNESIS MORBI)");
        diseaseAnamnesis.add(
                anamneses.stream()
                        .filter(a -> a.getType() == AnamnesisType.DISEASE)
                        .findFirst()
                        .orElseThrow()
                        .getDescription()
        );

        Paragraph lifeAnamnesis = new Paragraph();
        addHeader(lifeAnamnesis, "ІСТОРІЯ ЖИТТЯ (ANAMNESIS VITAE)");
        lifeAnamnesis.add(
                anamneses.stream()
                        .filter(a -> a.getType() == AnamnesisType.LIFE)
                        .findFirst()
                        .orElseThrow()
                        .getDescription()
        );

        Paragraph allergicAnamnesis = new Paragraph();
        addHeader(allergicAnamnesis, "АЛЕРГОЛОГІЧНИЙ АНАМНЕЗ");
        allergicAnamnesis.add(
                anamneses.stream()
                        .filter(a -> a.getType() == AnamnesisType.ALLERGIC)
                        .findFirst()
                        .orElseThrow()
                        .getDescription()
        );

        Paragraph epidemicAnamnesis = new Paragraph();
        addHeader(epidemicAnamnesis, "ЕПІДЕМІОЛОГІЧНИЙ АНАМНЕЗ");
        epidemicAnamnesis.add(
                anamneses.stream()
                        .filter(a -> a.getType() == AnamnesisType.EPIDEMIC)
                        .findFirst()
                        .orElseThrow()
                        .getDescription()
        );

        document.add(diseaseAnamnesis);
        document.add(lifeAnamnesis);
        document.add(allergicAnamnesis);
        document.add(epidemicAnamnesis);
        pageBreak(document);
    }

    private static void addExaminationPage(Document document,
                                           GeneralExamination ge,
                                           Map<OrganSystem, java.util.List<DetailedExamination>> de) {
        Paragraph examination = new Paragraph();
        addHeader(examination, "ОБ'ЄКТИВНЕ ОБСТЕЖЕННЯ");
        addExaminationField(examination, "Загальний стан", CONDITIONS.get(ge.getCondition()));
        addExaminationField(examination, "Свідомість", CONSCIOUSNESS.get(ge.getConsciousness()));
        addExaminationField(examination, "Положення", ge.getPosition());
        addExaminationField(examination, "Тіло пропорційне", ge.getIsBodyProportional() ? "так" : "ні");
        addExaminationField(examination, "Конституція", CONSTITUTIONS.get(ge.getConstitution()));
        addExaminationField(examination, "Зріст", ge.getHeight() + " см");
        addExaminationField(examination, "Вага", ge.getWeight() + " кг");
        addExaminationField(examination, "Тонус", TONES.get(ge.getTone()));
        addExaminationField(examination, "Температура", TEMPERATURES.get(ge.getTemperature()));
        addEmptyLine(examination, 1);

        Paragraph detExamination = new Paragraph();
        de.forEach((organSystem, detailedExaminations) -> {
            detExamination.add(new Text(organSystem.getName() + "\n").setBold());
            for (DetailedExamination d : detailedExaminations) {
                detExamination.add(d.getDescription());
                addEmptyLine(detExamination, 1);
            }
        });

        document.add(examination);
        document.add(detExamination);
        pageBreak(document);
    }

    private static void addDiagnosis(Document document, String type, Diagnosis d) {
        Paragraph diagnosis = new Paragraph();
        addHeader(diagnosis, type);
        diagnosis.add(formatDate(d.getDate()) + "\n");
        diagnosis.add(d.getDescription());
        diagnosis.add("\nОсновний: " + d.getMainDisease());
        diagnosis.add("\nУскладнення: " + d.getComplicationDisease());
        diagnosis.add("\nСупутнє захворювання: " + d.getConcomitantDisease());
        addEmptyLine(diagnosis, 1);
        document.add(diagnosis);
    }

    private static void addAnalyses(Document document, java.util.List<Analysis> analysisList) {
        Paragraph analyses = new Paragraph();
        addHeader(analyses, "ОБСТЕЖЕННЯ");

        List list = new List(ListNumberingType.DECIMAL);
        for (Analysis a : analysisList) {
            list.add(a.getType());
        }
        analyses.add(list);
        addEmptyLine(analyses, 1);
        analyses.add(new Paragraph("Результати").setItalic());
        addEmptyLine(analyses, 1);

        analysisList.stream()
                .filter(a -> a.getResult() != null)
                .forEach(a -> {
                    Paragraph result = new Paragraph(a.getType());
                    result.add(" від ");
                    result.add(formatDate(a.getResult().getDate()) + ": ");
                    result.add(a.getResult().getDescription());
                    addEmptyLine(result, 1);
                    result.add(a.getResult().getConclusion());
                    analyses.add(result);
                });

        document.add(analyses);
        pageBreak(document);
    }

    private static void addDiary(Document document, java.util.List<Diary> diaries) {
        Paragraph diary = new Paragraph();
        addHeader(diary, "ЩОДЕННИКИ");

        document.add(diary);

        Table table = new Table(UnitValue.createPercentArray(new float[]{20f, 80f}))
                .useAllAvailableWidth();

        for (Diary d : diaries) {
            Paragraph left = new Paragraph();
            left.add(formatDate(d.getDate()) + "\n");
            left.add("ЧСС - " + d.getHeartRate() + "'\n");
            left.add("АТ - " + d.getBloodPressureDia() + "/" + d.getBloodPressureSys() + "\nмм.рт.ст\n");
            left.add("ЧД - " + d.getRespiratoryRate() + "'\n");
            left.add("Т - " + d.getTemperature() + "°С\n");

            Paragraph right = new Paragraph();
            right.add("Стан: " + CONDITIONS.get(d.getCondition()));
            right.add("\nСкарги: " + d.getComplaints());
            right.add("\nОгляд: " + d.getExamination());

            table.addCell(left);
            table.addCell(right);
        }

        document.add(table);
        pageBreak(document);
    }

    private static void addEpicrisis(Document document, Epicrisis e) {
        Paragraph epicrisis = new Paragraph();
        addHeader(epicrisis, "ЕПІКРИЗ");
        epicrisis.add(new Paragraph("Тип: " + EPICRISIS_TYPES.get(e.getType())));
        addEmptyLine(epicrisis, 1);
        epicrisis.add(new Paragraph("Стан: " + CONDITIONS.get(e.getCondition())));
        addEmptyLine(epicrisis, 1);
        epicrisis.add(new Paragraph("Рекомендації: " + e.getRecommendation()));

        document.add(epicrisis);
    }

    private static void addSignature(Document document) {
        Paragraph paragraph = new Paragraph().setWidth(UnitValue.createPercentValue(100f));
        paragraph.setTextAlignment(TextAlignment.RIGHT);
        paragraph.add(new Text("Зав. відділення"));
        paragraph.add(new Text("                                            ").setUnderline());
        addEmptyLine(paragraph, 1);
        paragraph.add(new Text("Лікар"));
        paragraph.add(new Text("                                            ").setUnderline());
        addEmptyLine(paragraph, 1);

        paragraph.setFixedPosition(document.getLeftMargin(), document.getBottomMargin(), UnitValue.createPercentValue(100f));

        document.add(paragraph);
    }

    private static void addTreatment(Document document, Treatment t) {
        Paragraph treatment = new Paragraph();
        addHeader(treatment, "Лікування");

        treatment.add(t.getPrescribed());
        addEmptyLine(treatment, 1);
        treatment.add(t.getMedicines());

        document.add(treatment);
        pageBreak(document);
    }

    private static void addHeader(Paragraph paragraph, String name) {
        paragraph.add(
                new Paragraph(name)
                        .setWidth(UnitValue.createPercentValue(100f))
                        .setTextAlignment(TextAlignment.CENTER)
                        .setBold()
        );
        addEmptyLine(paragraph, 2);
    }

    private static void addExaminationField(Paragraph paragraph, String name, String value) {
        Text nameField = new Text(name + ": ");
        nameField.setBold();
        paragraph.add(nameField);
        paragraph.add(value + '\n');
    }

    private static void addTextField(Paragraph paragraph, String name, String value) {
        if (name == null) {
            name = "";
        }
        if (value == null) {
            value = "";
        }
        Text val = new Text(' ' + value + '\n');
        val.setUnderline();
        paragraph.add(name);
        paragraph.add(val);
    }

    private static void pageBreak(Document document) {
        document.add(new AreaBreak(AreaBreakType.NEXT_PAGE));
    }

    private static void addEmptyLine(Paragraph paragraph, int cnt) {
        for (int i = 0; i < cnt; i++) {
            paragraph.add(new Text("\n"));
        }
    }
}
