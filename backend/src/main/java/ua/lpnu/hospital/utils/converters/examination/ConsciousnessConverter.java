package ua.lpnu.hospital.utils.converters.examination;

import ua.lpnu.hospital.db.models.examination.Consciousness;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ConsciousnessConverter implements AttributeConverter<Consciousness, String> {
    @Override
    public String convertToDatabaseColumn(Consciousness attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.code;
    }

    @Override
    public Consciousness convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }
        return Consciousness.valueOfCode(dbData);
    }
}
