package ua.lpnu.hospital.utils.converters;

import ua.lpnu.hospital.db.models.DiagnosisType;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class DiagnosisTypeConverter implements AttributeConverter<DiagnosisType, String> {
    @Override
    public String convertToDatabaseColumn(DiagnosisType attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.code;
    }

    @Override
    public DiagnosisType convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }
        return DiagnosisType.valueOfCode(dbData);
    }
}
