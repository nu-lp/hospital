package ua.lpnu.hospital.utils.converters.examination;

import ua.lpnu.hospital.db.models.examination.Constitution;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class ConstitutionConverter implements AttributeConverter<Constitution, String> {
    @Override
    public String convertToDatabaseColumn(Constitution attribute) {
        if (attribute == null) {
            return null;
        }
        return attribute.code;
    }

    @Override
    public Constitution convertToEntityAttribute(String dbData) {
        if (dbData == null) {
            return null;
        }
        return Constitution.valueOfCode(dbData);
    }
}
