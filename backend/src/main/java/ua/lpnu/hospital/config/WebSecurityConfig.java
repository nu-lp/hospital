package ua.lpnu.hospital.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    private final JwtAuthenticationEntryPoint entryPoint;
    private final UserDetailsService userDetailsService;
    private final JwtRequestFilter filter;

    public WebSecurityConfig(JwtAuthenticationEntryPoint entryPoint,
                             @Qualifier("jwtUserDetailsService")
                                     UserDetailsService userDetailsService,
                             JwtRequestFilter filter) {
        this.entryPoint = entryPoint;
        this.userDetailsService = userDetailsService;
        this.filter = filter;
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userDetailsService)
                .passwordEncoder(passwordEncoder());
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/", "/**/*.js", "/**/*.css", "/**/*.json", "/**/*.svg", "/**/*.ico");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .cors()
                .and()
                .headers().frameOptions().disable()
                .and()
                .csrf().disable()
                .authorizeRequests()
                .antMatchers("/swagger-ui**").permitAll()
                .antMatchers("/swagger-ui/**").permitAll()
                .antMatchers("/h2-console/**").permitAll()
                .antMatchers("/v3/api-docs/**").permitAll()
                .antMatchers("/login").permitAll()
                .antMatchers("/doctors/patients").hasRole("DOCTOR")
                .antMatchers("/doctors/**").hasRole("MAIN_DOCTOR")
                .antMatchers("/analyses/**").hasRole("DOCTOR")
                .antMatchers("/diagnoses/**").hasRole("DOCTOR")
                .antMatchers("/diary/**").hasRole("DOCTOR")
                .antMatchers("/epicrises/**").hasRole("DOCTOR")
                .antMatchers("/hospitalizations/**").hasRole("DOCTOR")
                .antMatchers("/patients/**").hasRole("DOCTOR")
                .antMatchers("/reports/**").hasRole("MAIN_DOCTOR")
                .antMatchers("/treatments/**").hasRole("DOCTOR")
                .anyRequest().authenticated()
                .and()
                .exceptionHandling().authenticationEntryPoint(entryPoint)
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);

        http.addFilterBefore(filter, UsernamePasswordAuthenticationFilter.class);
    }
}
