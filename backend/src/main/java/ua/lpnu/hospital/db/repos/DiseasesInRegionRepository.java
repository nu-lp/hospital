package ua.lpnu.hospital.db.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.lpnu.hospital.db.models.DiseasesInRegionView;

import java.util.List;

@Repository
public interface DiseasesInRegionRepository extends JpaRepository<DiseasesInRegionView, String> {
    List<DiseasesInRegionView> findAllByOrderByCountDesc();
}
