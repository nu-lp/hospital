package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.db.models.DiaryTreatment;

@Mapper
public interface DiaryTreatmentMapper extends OneWayMapperDto<DiaryTreatment, String> {
    DiaryTreatmentMapper INSTANCE = Mappers.getMapper(DiaryTreatmentMapper.class);

    @Override
    default String map(DiaryTreatment from) {
        return from.getDescription();
    }
}
