package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.AddressDto;
import ua.lpnu.hospital.db.models.address.Address;

@Mapper
public interface AddressMapper extends OneWayMapperDto<Address, AddressDto> {
    AddressMapper INSTANCE = Mappers.getMapper(AddressMapper.class);

    @Mappings({
            @Mapping(source = "street.name", target = "street"),
            @Mapping(source = "street.city.name", target = "city")
    })
    @Override
    AddressDto map(Address from);
}
