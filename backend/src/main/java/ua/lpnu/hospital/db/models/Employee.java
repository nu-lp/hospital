package ua.lpnu.hospital.db.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import ua.lpnu.hospital.db.models.auth.User;

import javax.persistence.*;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "Employee")
@Inheritance(strategy = InheritanceType.JOINED)
public class Employee extends Person {
    @Column(name = "Code")
    @Generated(GenerationTime.INSERT)
    private String uuid;

    @OneToOne(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "User_Id")
    private User user;
}
