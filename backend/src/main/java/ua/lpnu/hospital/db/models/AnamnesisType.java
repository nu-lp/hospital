package ua.lpnu.hospital.db.models;

import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public enum AnamnesisType {
    DISEASE("DIS"),
    LIFE("LIF"),
    EPIDEMIC("EPI"),
    ALLERGIC("ALG");

    private static final Map<String, AnamnesisType> BY_CODE = new HashMap<>();

    static {
        for (AnamnesisType h : values()) {
            BY_CODE.put(h.code, h);
        }
    }

    public final String code;

    public static AnamnesisType valueOfCode(String code) {
        return BY_CODE.get(code);
    }
}
