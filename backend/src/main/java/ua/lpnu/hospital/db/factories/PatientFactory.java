package ua.lpnu.hospital.db.factories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.api.dto.patient.CreatePatientDto;
import ua.lpnu.hospital.api.dto.patient.UpdatePatientDto;
import ua.lpnu.hospital.db.models.Patient;
import ua.lpnu.hospital.db.models.address.Address;
import ua.lpnu.hospital.services.AddressService;

@Component
public class PatientFactory {
    private final AddressService addressService;

    @Autowired
    public PatientFactory(AddressService addressService) {
        this.addressService = addressService;
    }

    public Patient create(CreatePatientDto dto) {
        Address address = addressService.findOrCreateAddress(dto.getAddress());
        Patient patient = new Patient();
        patient.setFirstName(dto.getFirstName());
        patient.setLastName(dto.getLastName());
        patient.setMiddleName(dto.getMiddleName());
        patient.setPhone(dto.getPhone());
        patient.setBirthday(dto.getBirthday());
        patient.setHomePhone(dto.getHomePhone());
        patient.setIsMale(dto.getIsMale());
        patient.setAddress(address);

        return patient;
    }

    public Patient update(Patient patient, UpdatePatientDto dto) {
        Address address = addressService.findOrCreateAddress(dto.getAddress());
        patient.setPhone(dto.getPhone());
        patient.setHomePhone(dto.getHomePhone());
        patient.setAddress(address);

        return patient;
    }
}
