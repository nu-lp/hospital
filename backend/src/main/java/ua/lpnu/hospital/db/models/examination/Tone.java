package ua.lpnu.hospital.db.models.examination;

import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public enum Tone {
    INCREASED("INC"),
    PRESERVED("PRE"),
    REDUCED("RED");

    private static final Map<String, Tone> BY_CODE = new HashMap<>();

    static {
        for (Tone h : values()) {
            BY_CODE.put(h.code, h);
        }
    }

    public final String code;

    public static Tone valueOfCode(String code) {
        return BY_CODE.get(code);
    }
}
