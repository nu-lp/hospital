package ua.lpnu.hospital.db.models.address;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ua.lpnu.hospital.db.models.Patient;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "Address")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "HouseNumber")
    private Integer houseNumber;

    @Column(name = "ZipCode")
    private String zipCode;

    @ManyToOne
    @JoinColumn(name = "Street_Id")
    private Street street;

    @OneToMany(mappedBy = "address")
    private List<Patient> patients = new ArrayList<>();
}
