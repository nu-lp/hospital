package ua.lpnu.hospital.db.models.examination;

import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public enum Posture {
    STRAIGHT("STR"),
    SLOUGH("SLO"),
    CROOKED("CRO");

    private static final Map<String, Posture> BY_CODE = new HashMap<>();

    static {
        for (Posture h : values()) {
            BY_CODE.put(h.code, h);
        }
    }

    public final String code;

    public static Posture valueOfCode(String code) {
        return BY_CODE.get(code);
    }
}
