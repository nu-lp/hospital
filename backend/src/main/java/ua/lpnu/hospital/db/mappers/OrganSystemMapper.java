package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.OrganSystemDto;
import ua.lpnu.hospital.db.models.OrganSystem;

@Mapper
public interface OrganSystemMapper extends OneWayMapperDto<OrganSystem, OrganSystemDto> {
    OrganSystemMapper INSTANCE = Mappers.getMapper(OrganSystemMapper.class);
}
