package ua.lpnu.hospital.db.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.lpnu.hospital.db.models.DiaryTreatment;

@Repository
public interface DiaryTreatmentRepository extends JpaRepository<DiaryTreatment, Long> {
}
