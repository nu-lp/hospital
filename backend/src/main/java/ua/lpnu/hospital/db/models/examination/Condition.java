package ua.lpnu.hospital.db.models.examination;

import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public enum Condition {
    SATISFACTORY("STF"),
    MODERATE("MOD"),
    SERIOUS("SRS");

    private static final Map<String, Condition> BY_CODE = new HashMap<>();

    static {
        for (Condition h : values()) {
            BY_CODE.put(h.code, h);
        }
    }

    public final String code;

    public static Condition valueOfCode(String code) {
        return BY_CODE.get(code);
    }
}
