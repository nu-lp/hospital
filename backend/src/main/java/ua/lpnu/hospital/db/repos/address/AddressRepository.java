package ua.lpnu.hospital.db.repos.address;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.lpnu.hospital.db.models.address.Address;
import ua.lpnu.hospital.db.models.address.Street;

import java.util.Optional;

@Repository
public interface AddressRepository extends JpaRepository<Address, Long> {
    Optional<Address> findByHouseNumberAndStreet(Integer houseNumber, Street street);
}
