package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.analysis.AnalysisResultDto;
import ua.lpnu.hospital.db.models.AnalysisResult;

@Mapper(uses = DoctorMapper.class)
public interface AnalysisResultMapper extends OneWayMapperDto<AnalysisResult, AnalysisResultDto> {
    AnalysisResultMapper INSTANCE = Mappers.getMapper(AnalysisResultMapper.class);
}
