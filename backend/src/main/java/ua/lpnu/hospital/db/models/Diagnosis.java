package ua.lpnu.hospital.db.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ua.lpnu.hospital.utils.converters.DiagnosisTypeConverter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Diagnosis")
public class Diagnosis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Type")
    @Convert(converter = DiagnosisTypeConverter.class)
    private DiagnosisType type;

    @Column(name = "Description")
    private String description;

    @Column(name = "Date")
    private LocalDate date;

    @Column(name = "MainDisease")
    private String mainDisease;

    @Column(name = "ComplicationDisease")
    private String complicationDisease;

    @Column(name = "ConcomitantDisease")
    private String concomitantDisease;

    @ManyToOne
    @JoinColumn(name = "Hospitalization_Id")
    private Hospitalization hospitalization;
}
