package ua.lpnu.hospital.db.factories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.api.dto.complaint.CreateComplaintDto;
import ua.lpnu.hospital.api.exceptions.OrganSystemNotFoundException;
import ua.lpnu.hospital.db.models.Complaint;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.models.OrganSystem;
import ua.lpnu.hospital.db.repos.OrganSystemRepository;

@Component
public class ComplaintFactory {
    private final OrganSystemRepository organSystemRepository;

    @Autowired
    public ComplaintFactory(OrganSystemRepository organSystemRepository) {
        this.organSystemRepository = organSystemRepository;
    }

    Complaint create(Hospitalization hospitalization,
                     CreateComplaintDto dto) {
        OrganSystem os = organSystemRepository
                .findByName(dto.getOrganSystem())
                .orElseThrow(OrganSystemNotFoundException::new);
        Complaint complaint = new Complaint();
        complaint.setStartDate(dto.getStartDate());
        complaint.setDescription(dto.getDescription());
        complaint.setHospitalization(hospitalization);
        complaint.setOrganSystem(os);

        return complaint;
    }
}
