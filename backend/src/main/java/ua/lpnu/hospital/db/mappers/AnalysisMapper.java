package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.analysis.AnalysisDto;
import ua.lpnu.hospital.db.models.Analysis;

@Mapper(uses = AnalysisResultMapper.class)
public interface AnalysisMapper extends OneWayMapperDto<Analysis, AnalysisDto> {
    AnalysisMapper INSTANCE = Mappers.getMapper(AnalysisMapper.class);
}
