package ua.lpnu.hospital.db.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.lpnu.hospital.db.models.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
