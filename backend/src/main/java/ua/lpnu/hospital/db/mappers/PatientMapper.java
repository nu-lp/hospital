package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.patient.PatientDto;
import ua.lpnu.hospital.db.models.Patient;

@Mapper(uses = {AddressMapper.class})
public interface PatientMapper extends OneWayMapperDto<Patient, PatientDto> {
    PatientMapper INSTANCE = Mappers.getMapper(PatientMapper.class);
}
