package ua.lpnu.hospital.db.models.auth;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenerationTime;
import ua.lpnu.hospital.db.models.Employee;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "UserTbl")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Username")
    private String username;

    @Column(name = "PasswordHash")
    private String passwordHash;

    @Column(name = "CreateTime")
    @org.hibernate.annotations.Generated(GenerationTime.INSERT)
    private LocalDateTime createTime;

    @Column(name = "LastLogin")
    private LocalDateTime lastLogin;

    @Column(name = "IsActive")
    private Boolean isActive;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "User_has_Role",
            joinColumns = @JoinColumn(name = "User_Id", referencedColumnName = "Id"),
            inverseJoinColumns = @JoinColumn(name = "Role_Id", referencedColumnName = "Id")
    )
    private List<Role> roles = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "user")
    private Employee employee;
}
