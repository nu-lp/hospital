package ua.lpnu.hospital.db.models;

import lombok.Getter;
import lombok.Setter;
import ua.lpnu.hospital.db.models.examination.DetailedExamination;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@Entity(name = "OrganSystem")
public class OrganSystem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Name")
    private String name;

    @OneToMany(mappedBy = "organSystem")
    private List<DetailedExamination> detailedExaminations;

    @OneToMany(mappedBy = "organSystem")
    private List<Complaint> complaints;
}
