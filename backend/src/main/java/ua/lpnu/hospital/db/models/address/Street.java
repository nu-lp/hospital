package ua.lpnu.hospital.db.models.address;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "Street")
public class Street {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Name")
    private String name;

    @ManyToOne
    @JoinColumn(name = "City_Id")
    private City city;

    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "street", orphanRemoval = true)
    private List<Address> addresses = new ArrayList<>();
}
