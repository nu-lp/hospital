package ua.lpnu.hospital.db.factories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.api.dto.hospitalization.CreateHospitalizationDto;
import ua.lpnu.hospital.db.models.*;
import ua.lpnu.hospital.db.models.examination.DetailedExamination;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class HospitalizationFactory {
    private final DiagnosisFactory diagnosisFactory;
    private final ComplaintFactory complaintFactory;
    private final GeneralExaminationFactory generalExaminationFactory;
    private final AnamnesisFactory anamnesisFactory;
    private final DetailedExaminationFactory detailedExaminationFactory;
    private final AnalysisFactory analysisFactory;

    @Autowired
    public HospitalizationFactory(DiagnosisFactory diagnosisFactory,
                                  ComplaintFactory complaintFactory,
                                  GeneralExaminationFactory generalExaminationFactory,
                                  AnamnesisFactory anamnesisFactory,
                                  DetailedExaminationFactory detailedExaminationFactory,
                                  AnalysisFactory analysisFactory) {
        this.diagnosisFactory = diagnosisFactory;
        this.complaintFactory = complaintFactory;
        this.generalExaminationFactory = generalExaminationFactory;
        this.anamnesisFactory = anamnesisFactory;
        this.detailedExaminationFactory = detailedExaminationFactory;

        this.analysisFactory = analysisFactory;
    }

    public Hospitalization create(Doctor doctor,
                                  Patient patient,
                                  CreateHospitalizationDto dto) {
        Hospitalization h = new Hospitalization();
        h.setHospitalizationDate(dto.getHospitalizationDate());
        h.setDiseaseDate(dto.getDiseaseDate());
        h.setEmployment(dto.getEmployment());
        h.setType(dto.getType());
        h.setReferral(dto.getReferral());
        h.setDoctor(doctor);
        h.setPatient(patient);
        // Diagnoses
        List<Diagnosis> diagnoses = List.of(
                diagnosisFactory.create(h, dto.getAdmittanceDiagnosis(), DiagnosisType.ADMITTANCE),
                diagnosisFactory.create(h, dto.getPreliminaryDiagnosis(), DiagnosisType.PRELIMINARY)
        );
        h.setDiagnoses(diagnoses);
        // Complaints
        List<Complaint> complaints = dto.getComplaints().stream()
                .map(c -> complaintFactory.create(h, c))
                .collect(Collectors.toList());
        h.setComplaints(complaints);
        // Anamneses
        List<Anamnesis> anamneses = List.of(
                anamnesisFactory.create(h, AnamnesisType.DISEASE, dto.getDiseaseAnamnesis()),
                anamnesisFactory.create(h, AnamnesisType.LIFE, dto.getLifeAnamnesis()),
                anamnesisFactory.create(h, AnamnesisType.EPIDEMIC, dto.getEpidemicAnamnesis()),
                anamnesisFactory.create(h, AnamnesisType.ALLERGIC, dto.getAllergicAnamnesis())
        );
        h.setAnamneses(anamneses);

        h.setGeneralExamination(generalExaminationFactory.create(h, dto.getGeneralExamination()));
        // Detailed examinations
        List<DetailedExamination> detailedExaminations = dto.getDetailedExaminations().stream()
                .map(de -> detailedExaminationFactory.create(h, de))
                .collect(Collectors.toList());
        h.setDetailedExaminations(detailedExaminations);
        // Analyses
        List<Analysis> analyses = dto.getAnalyses().stream()
                .map(a -> analysisFactory.create(h, a))
                .collect(Collectors.toList());
        h.setAnalyses(analyses);

        return h;
    }
}
