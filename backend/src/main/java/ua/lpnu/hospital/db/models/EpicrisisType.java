package ua.lpnu.hospital.db.models;

import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public enum EpicrisisType {
    EXTRACT("EXT"),
    REDIRECTION("RED"),
    DEATH("DTH");

    private static final Map<String, EpicrisisType> BY_CODE = new HashMap<>();

    static {
        for (EpicrisisType h : values()) {
            BY_CODE.put(h.code, h);
        }
    }

    public final String code;

    public static EpicrisisType valueOfCode(String code) {
        return BY_CODE.get(code);
    }
}
