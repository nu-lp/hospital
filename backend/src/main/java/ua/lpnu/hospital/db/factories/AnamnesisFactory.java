package ua.lpnu.hospital.db.factories;

import org.springframework.stereotype.Component;
import ua.lpnu.hospital.api.dto.anamnesis.CreateAnamnesisDto;
import ua.lpnu.hospital.db.models.Anamnesis;
import ua.lpnu.hospital.db.models.AnamnesisType;
import ua.lpnu.hospital.db.models.Hospitalization;

@Component
public class AnamnesisFactory {
    Anamnesis create(Hospitalization hospitalization,
                     AnamnesisType type,
                     CreateAnamnesisDto dto) {
        Anamnesis anamnesis = new Anamnesis();
        anamnesis.setType(type);
        anamnesis.setDescription(dto.getDescription());
        anamnesis.setHospitalization(hospitalization);
        return anamnesis;
    }
}
