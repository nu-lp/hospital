package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.epicrisis.EpicrisisDto;
import ua.lpnu.hospital.db.models.Epicrisis;

@Mapper
public interface EpicrisisMapper extends OneWayMapperDto<Epicrisis, EpicrisisDto> {
    EpicrisisMapper INSTANCE = Mappers.getMapper(EpicrisisMapper.class);
}
