package ua.lpnu.hospital.db.models;

import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public enum DiagnosisType {
    PRELIMINARY("PRE"),
    FINAL("FIN"),
    ADMITTANCE("ADM");

    private static final Map<String, DiagnosisType> BY_CODE = new HashMap<>();

    static {
        for (DiagnosisType h : values()) {
            BY_CODE.put(h.code, h);
        }
    }

    public final String code;

    public static DiagnosisType valueOfCode(String code) {
        return BY_CODE.get(code);
    }
}
