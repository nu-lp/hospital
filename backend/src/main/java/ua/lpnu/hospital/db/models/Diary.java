package ua.lpnu.hospital.db.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenerationTime;
import ua.lpnu.hospital.db.models.examination.Condition;
import ua.lpnu.hospital.utils.converters.examination.ConditionConverter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Diary")
public class Diary {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @org.hibernate.annotations.Generated(GenerationTime.INSERT)
    @Column(name = "Date")
    private LocalDateTime date;

    @Column(name = "Condition")
    @Convert(converter = ConditionConverter.class)
    private Condition condition;

    @Column(name = "Temperature")
    private Float temperature;

    @Column(name = "RespiratoryRate")
    private Integer respiratoryRate;

    @Column(name = "HeartRate")
    private Integer heartRate;

    @Column(name = "BloodPressureDia")
    private Integer bloodPressureDia;

    @Column(name = "BloodPressureSys")
    private Integer bloodPressureSys;

    @Column(name = "Complaints")
    private String complaints;

    @Column(name = "Examination")
    private String examination;

    @ManyToOne
    @JoinColumn(name = "Hospitalization_Id")
    private Hospitalization hospitalization;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "diary", orphanRemoval = true)
    private List<DiaryTreatment> treatments = new ArrayList<>();
}
