package ua.lpnu.hospital.db.models.auth;

/**
 * Roles has to begin with ROLE_ prefix
 */
public enum UserRole {
    ROLE_ADMIN,
    ROLE_DOCTOR,
    ROLE_MAIN_DOCTOR,
}
