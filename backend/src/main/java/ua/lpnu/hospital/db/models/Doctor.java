package ua.lpnu.hospital.db.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "Doctor")
public class Doctor extends Employee {
    @ManyToOne
    @JoinColumn(name = "Specialization_Id")
    private Specialization specialization;

    @OneToMany(mappedBy = "doctor")
    private List<Hospitalization> hospitalizations = new ArrayList<>();

    @OneToMany(mappedBy = "doctor")
    private List<AnalysisResult> analysisResults = new ArrayList<>();
}
