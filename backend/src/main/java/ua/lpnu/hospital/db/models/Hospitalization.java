package ua.lpnu.hospital.db.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenerationTime;
import ua.lpnu.hospital.db.models.examination.DetailedExamination;
import ua.lpnu.hospital.db.models.examination.GeneralExamination;
import ua.lpnu.hospital.utils.converters.HospitalizationTypeConverter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Hospitalization")
public class Hospitalization {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Code")
    @org.hibernate.annotations.Generated(GenerationTime.INSERT)
    private String uuid;

    @Column(name = "HospitalizationDate")
    private LocalDate hospitalizationDate;

    @Column(name = "DiseaseDate")
    private LocalDate diseaseDate;

    @Column(name = "ExtractDate")
    private LocalDate extractDate;

    @Column(name = "Employment")
    private String employment;

    @Column(name = "Type")
    @Convert(converter = HospitalizationTypeConverter.class)
    private HospitalizationType type;

    @Column(name = "Referral")
    private String referral;

    @ManyToOne
    @JoinColumn(name = "Doctor_Id")
    private Doctor doctor;

    @ManyToOne
    @JoinColumn(name = "Patient_Id")
    private Patient patient;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hospitalization", orphanRemoval = true)
    private List<Diagnosis> diagnoses = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hospitalization", orphanRemoval = true)
    private List<Complaint> complaints = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hospitalization", orphanRemoval = true)
    private List<Anamnesis> anamneses = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "hospitalization", orphanRemoval = true)
    private GeneralExamination generalExamination;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hospitalization", orphanRemoval = true)
    private List<DetailedExamination> detailedExaminations = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hospitalization", orphanRemoval = true)
    private List<Analysis> analyses = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "hospitalization", orphanRemoval = true)
    private Treatment treatment;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "hospitalization", orphanRemoval = true)
    private List<Diary> diaries = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "hospitalization", orphanRemoval = true)
    private Epicrisis epicrisis;
}
