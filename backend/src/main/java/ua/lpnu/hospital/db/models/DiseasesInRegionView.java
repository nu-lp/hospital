package ua.lpnu.hospital.db.models;

import lombok.*;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "DiseasesInRegion")
@IdClass(PrimaryKey.class)
@Immutable
public class DiseasesInRegionView {
    @Id
    @Column(name = "City")
    private String city;

    @Id
    @Column(name = "Disease")
    private String disease;

    @Column(name = "Count")
    private Integer count;
}

@Data
class PrimaryKey implements Serializable {
    @Column(name = "City")
    private String city;

    @Column(name = "Disease")
    private String disease;
}
