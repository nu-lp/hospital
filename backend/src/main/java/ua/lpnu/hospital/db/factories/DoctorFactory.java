package ua.lpnu.hospital.db.factories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.api.dto.doctor.CreateDoctorDto;
import ua.lpnu.hospital.api.exceptions.SpecializationNotFoundException;
import ua.lpnu.hospital.db.models.Doctor;
import ua.lpnu.hospital.db.models.Specialization;
import ua.lpnu.hospital.db.models.auth.Role;
import ua.lpnu.hospital.db.models.auth.User;
import ua.lpnu.hospital.db.models.auth.UserRole;
import ua.lpnu.hospital.db.repos.RoleRepository;
import ua.lpnu.hospital.db.repos.SpecializationRepository;

@Component
public class DoctorFactory {
    private final SpecializationRepository specializationRepository;
    private final RoleRepository roleRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public DoctorFactory(SpecializationRepository specializationRepository,
                         RoleRepository roleRepository, PasswordEncoder passwordEncoder) {
        this.specializationRepository = specializationRepository;
        this.roleRepository = roleRepository;
        this.passwordEncoder = passwordEncoder;
    }

    public Doctor create(CreateDoctorDto dto) {
        Specialization specialization = specializationRepository
                .findByName(dto.getSpecialization())
                .orElseGet(() -> {
                    Specialization s = new Specialization();
                    s.setName(dto.getSpecialization());
                    return specializationRepository.save(s);
                });

        Doctor doc = new Doctor();

        doc.setFirstName(dto.getFirstName());
        doc.setLastName(dto.getLastName());
        doc.setMiddleName(dto.getMiddleName());
        doc.setPhone(dto.getPhone());
        doc.setSpecialization(specialization);
        doc.setUser(createUser(dto));

        return doc;
    }

    private User createUser(CreateDoctorDto dto) {
        Role doctorRole = roleRepository.findByName(UserRole.ROLE_DOCTOR)
                .orElseThrow(SpecializationNotFoundException::new);
        Role mainDocRole = roleRepository.findByName(UserRole.ROLE_MAIN_DOCTOR)
                .orElseThrow(SpecializationNotFoundException::new);

        User user = new User();
        user.setUsername(dto.getUsername());
        user.setPasswordHash(passwordEncoder.encode(dto.getPassword()));
        user.setIsActive(true);
        user.getRoles().add(doctorRole);
        if (dto.getIsMainDoc()) {
            user.getRoles().add(mainDocRole);
        }
        return user;
    }
}
