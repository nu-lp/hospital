package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.examination.DetailedExaminationDto;
import ua.lpnu.hospital.db.models.examination.DetailedExamination;

@Mapper
public interface DetailedExaminationMapper extends OneWayMapperDto<DetailedExamination, DetailedExaminationDto> {
    DetailedExaminationMapper INSTANCE = Mappers.getMapper(DetailedExaminationMapper.class);

    @Mapping(source = "organSystem.name", target = "organSystem")
    @Override
    DetailedExaminationDto map(DetailedExamination from);
}
