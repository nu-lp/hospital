package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.doctor.EmployeeDoctorDto;
import ua.lpnu.hospital.db.models.Doctor;

@Mapper
public interface EmployeeDoctorMapper extends OneWayMapperDto<Doctor, EmployeeDoctorDto> {
    EmployeeDoctorMapper INSTANCE = Mappers.getMapper(EmployeeDoctorMapper.class);

    @Override
    @Mapping(source = "specialization.name", target = "specialization")
    @Mapping(target = "isMainDoc", expression = "java(from.getUser().getRoles().stream().anyMatch(r -> r.getName() == ua.lpnu.hospital.db.models.auth.UserRole.ROLE_MAIN_DOCTOR))")
    @Mapping(target = "username", source = "user.username")
    @Mapping(target = "createTime", source = "user.createTime")
    @Mapping(target = "lastLoginTime", source = "user.lastLogin")
    @Mapping(target = "active", source = "user.isActive")
    EmployeeDoctorDto map(Doctor from);
}
