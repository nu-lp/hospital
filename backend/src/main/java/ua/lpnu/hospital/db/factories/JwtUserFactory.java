package ua.lpnu.hospital.db.factories;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import ua.lpnu.hospital.db.models.auth.JwtUser;
import ua.lpnu.hospital.db.models.auth.Role;
import ua.lpnu.hospital.db.models.auth.User;

import java.util.List;
import java.util.stream.Collectors;

public class JwtUserFactory {
    public static JwtUser create(User user) {
        return new JwtUser(
                user.getUsername(),
                user.getPasswordHash(),
                mapToAuthorities(user.getRoles()),
                user.getIsActive(),
                user.getEmployee().getFirstName(),
                user.getEmployee().getLastName(),
                user.getEmployee().getMiddleName(),
                user.getEmployee().getUuid());
    }

    private static List<? extends GrantedAuthority> mapToAuthorities(List<Role> roles) {
        return roles.stream()
                .map(r -> new SimpleGrantedAuthority(r.getName().name()))
                .collect(Collectors.toList());
    }
}
