package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.complaint.ComplaintDto;
import ua.lpnu.hospital.db.models.Complaint;

@Mapper
public interface ComplaintMapper extends OneWayMapperDto<Complaint, ComplaintDto> {
    ComplaintMapper INSTANCE = Mappers.getMapper(ComplaintMapper.class);

    @Mapping(source = "organSystem.name", target = "organSystem")
    @Override
    ComplaintDto map(Complaint from);
}
