package ua.lpnu.hospital.db.factories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lpnu.hospital.api.dto.examination.CreateDetailedExaminationDto;
import ua.lpnu.hospital.api.exceptions.OrganSystemNotFoundException;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.models.OrganSystem;
import ua.lpnu.hospital.db.models.examination.DetailedExamination;
import ua.lpnu.hospital.db.repos.OrganSystemRepository;

@Component
public class DetailedExaminationFactory {
    private final OrganSystemRepository organSystemRepository;

    @Autowired
    public DetailedExaminationFactory(OrganSystemRepository organSystemRepository) {
        this.organSystemRepository = organSystemRepository;
    }

    DetailedExamination create(Hospitalization hospitalization,
                               CreateDetailedExaminationDto dto) {
        OrganSystem os = organSystemRepository
                .findByName(dto.getOrganSystem())
                .orElseThrow(OrganSystemNotFoundException::new);
        DetailedExamination de = new DetailedExamination();
        de.setDescription(dto.getDescription());
        de.setHospitalization(hospitalization);
        de.setOrganSystem(os);

        return de;
    }
}
