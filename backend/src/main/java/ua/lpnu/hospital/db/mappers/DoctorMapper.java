package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.doctor.DoctorDto;
import ua.lpnu.hospital.db.models.Doctor;

@Mapper
public interface DoctorMapper extends OneWayMapperDto<Doctor, DoctorDto> {
    DoctorMapper INSTANCE = Mappers.getMapper(DoctorMapper.class);

    @Mapping(source = "specialization.name", target = "specialization")
    @Override
    DoctorDto map(Doctor from);
}
