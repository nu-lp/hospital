package ua.lpnu.hospital.db.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.lpnu.hospital.db.models.OrganSystem;

import java.util.Optional;

@Repository
public interface OrganSystemRepository extends JpaRepository<OrganSystem, Long> {
    Optional<OrganSystem> findByName(String name);

    Optional<OrganSystem> findById(Long id);
}
