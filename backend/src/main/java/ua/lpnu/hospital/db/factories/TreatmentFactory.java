package ua.lpnu.hospital.db.factories;

import org.springframework.stereotype.Component;
import ua.lpnu.hospital.api.dto.treatment.CreateTreatmentDto;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.models.Treatment;

@Component
public class TreatmentFactory {
    public Treatment create(Hospitalization hospitalization,
                            CreateTreatmentDto dto) {
        Treatment treatment = new Treatment();
        treatment.setPrescribed(dto.getPrescribed());
        treatment.setMedicines(dto.getMedicines());
        treatment.setHospitalization(hospitalization);

        return treatment;
    }
}
