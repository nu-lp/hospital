package ua.lpnu.hospital.db.models.examination;

import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public enum Consciousness {
    CLEAR("CLR"),
    CONFUSED("CNF"),
    STUPOR("STP"),
    SOPOR("SPR"),
    COMA("COM"),
    DELIRIUM("DEL"),
    HALLUCINATIONS("HAL"),
    ;

    private static final Map<String, Consciousness> BY_CODE = new HashMap<>();

    static {
        for (Consciousness h : values()) {
            BY_CODE.put(h.code, h);
        }
    }

    public final String code;

    public static Consciousness valueOfCode(String code) {
        return BY_CODE.get(code);
    }
}
