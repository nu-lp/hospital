package ua.lpnu.hospital.db.models.examination;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.models.OrganSystem;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "DetailedExamination")
public class DetailedExamination {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "Hospitalization_Id")
    private Hospitalization hospitalization;

    @ManyToOne
    @JoinColumn(name = "OrganSystem_Id")
    private OrganSystem organSystem;
}
