package ua.lpnu.hospital.db.factories;

import org.springframework.stereotype.Component;
import ua.lpnu.hospital.api.dto.epicrisis.CreateEpicrisisDto;
import ua.lpnu.hospital.db.models.Epicrisis;
import ua.lpnu.hospital.db.models.Hospitalization;

@Component
public class EpicrisisFactory {
    public Epicrisis create(Hospitalization hospitalization,
                            CreateEpicrisisDto dto) {
        Epicrisis epicrisis = new Epicrisis();
        epicrisis.setType(dto.getType());
        epicrisis.setCondition(dto.getCondition());
        epicrisis.setRecommendation(dto.getRecommendation());
        epicrisis.setHospitalization(hospitalization);

        return epicrisis;
    }
}
