package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.hospitalization.HospitalizationDto;
import ua.lpnu.hospital.db.models.Hospitalization;

@Mapper(uses = {
        DoctorMapper.class,
        DiagnosisMapper.class,
        ComplaintMapper.class,
        AnamnesisMapper.class,
        GeneralExaminationMapper.class,
        DetailedExaminationMapper.class,
        AnalysisMapper.class,
        TreatmentMapper.class,
        DiaryMapper.class,
        EpicrisisMapper.class
})
public interface HospitalizationMapper extends OneWayMapperDto<Hospitalization, HospitalizationDto> {
    HospitalizationMapper INSTANCE = Mappers.getMapper(HospitalizationMapper.class);
}
