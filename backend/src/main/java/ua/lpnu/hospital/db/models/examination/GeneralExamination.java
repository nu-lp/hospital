package ua.lpnu.hospital.db.models.examination;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.utils.converters.examination.*;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "GeneralExamination")
public class GeneralExamination {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Condition")
    @Convert(converter = ConditionConverter.class)
    private Condition condition;

    @Column(name = "Consciousness")
    @Convert(converter = ConsciousnessConverter.class)
    private Consciousness consciousness;

    @Column(name = "Position")
    private String position;

    @Column(name = "IsBodyProportional")
    private Boolean isBodyProportional;

    @Column(name = "Constitution")
    @Convert(converter = ConstitutionConverter.class)
    private Constitution constitution;

    @Column(name = "Height")
    private Float height;

    @Column(name = "Weight")
    private Float weight;

    @Column(name = "Tone")
    @Convert(converter = ToneConverter.class)
    private Tone tone;

    @Column(name = "Temperature")
    @Convert(converter = TemperatureConverter.class)
    private Temperature temperature;

    @OneToOne
    @JoinColumn(name = "Hospitalization_Id")
    private Hospitalization hospitalization;
}
