package ua.lpnu.hospital.db.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Analysis")
public class Analysis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Code")
    @org.hibernate.annotations.Generated(GenerationTime.INSERT)
    private String uuid;

    @Column(name = "Type")
    private String type;

    @ManyToOne
    @JoinColumn(name = "Hospitalization_Id")
    private Hospitalization hospitalization;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "analysis", orphanRemoval = true)
    private AnalysisResult result;
}
