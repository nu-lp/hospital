package ua.lpnu.hospital.db.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Complaint")
public class Complaint {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "StartDate")
    private LocalDate startDate;

    @Column(name = "Description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "Hospitalization_Id")
    private Hospitalization hospitalization;

    @ManyToOne
    @JoinColumn(name = "OrganSystem_Id")
    private OrganSystem organSystem;
}
