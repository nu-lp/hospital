package ua.lpnu.hospital.db.models.examination;

import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public enum Temperature {
    REDUCED("RED"),
    NORMAL("NRM"),
    SUBFEBRILE("SUB"),
    FEBRILE("FEB"),
    PYRETIC("PYR"),
    HYPERPYREXIA("HYP");

    private static final Map<String, Temperature> BY_CODE = new HashMap<>();

    static {
        for (Temperature h : values()) {
            BY_CODE.put(h.code, h);
        }
    }

    public final String code;

    public static Temperature valueOfCode(String code) {
        return BY_CODE.get(code);
    }
}
