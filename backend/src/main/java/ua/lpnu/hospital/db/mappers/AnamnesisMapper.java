package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.anamnesis.AnamnesisDto;
import ua.lpnu.hospital.db.models.Anamnesis;

@Mapper
public interface AnamnesisMapper extends OneWayMapperDto<Anamnesis, AnamnesisDto> {
    AnamnesisMapper INSTANCE = Mappers.getMapper(AnamnesisMapper.class);
}
