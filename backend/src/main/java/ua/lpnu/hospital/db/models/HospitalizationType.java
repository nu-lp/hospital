package ua.lpnu.hospital.db.models;

import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public enum HospitalizationType {
    PLANNED("PND"),
    URGENT("UGT");

    private static final Map<String, HospitalizationType> BY_CODE = new HashMap<>();

    static {
        for (HospitalizationType h : values()) {
            BY_CODE.put(h.code, h);
        }
    }

    public final String code;

    public static HospitalizationType valueOfCode(String code) {
        return BY_CODE.get(code);
    }
}
