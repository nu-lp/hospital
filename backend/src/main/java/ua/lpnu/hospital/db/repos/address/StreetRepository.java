package ua.lpnu.hospital.db.repos.address;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.lpnu.hospital.db.models.address.City;
import ua.lpnu.hospital.db.models.address.Street;

import java.util.Optional;

@Repository
public interface StreetRepository extends JpaRepository<Street, Long> {
    Optional<Street> findByNameIgnoreCaseAndCity(String name, City city);
}
