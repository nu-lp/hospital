package ua.lpnu.hospital.db.factories;

import org.springframework.stereotype.Component;
import ua.lpnu.hospital.api.dto.analysis.CreateAnalysisDto;
import ua.lpnu.hospital.api.dto.analysis.CreateAnalysisResultDto;
import ua.lpnu.hospital.db.models.Analysis;
import ua.lpnu.hospital.db.models.AnalysisResult;
import ua.lpnu.hospital.db.models.Doctor;
import ua.lpnu.hospital.db.models.Hospitalization;

@Component
public class AnalysisFactory {
    public Analysis create(Hospitalization hospitalization,
                           CreateAnalysisDto dto) {
        Analysis analysis = new Analysis();
        analysis.setType(dto.getType());
        analysis.setHospitalization(hospitalization);

        return analysis;
    }

    public AnalysisResult create(Analysis analysis,
                                 Doctor doctor,
                                 CreateAnalysisResultDto dto) {
        AnalysisResult result = new AnalysisResult();
        result.setDescription(dto.getDescription());
        result.setConclusion(dto.getConclusion());
        result.setAnalysis(analysis);
        result.setDoctor(doctor);

        return result;
    }
}
