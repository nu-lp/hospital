package ua.lpnu.hospital.db.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ua.lpnu.hospital.db.models.examination.Condition;
import ua.lpnu.hospital.utils.converters.EpicrisisTypeConverter;
import ua.lpnu.hospital.utils.converters.examination.ConditionConverter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Epicrisis")
public class Epicrisis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Type")
    @Convert(converter = EpicrisisTypeConverter.class)
    private EpicrisisType type;

    @Column(name = "Condition")
    @Convert(converter = ConditionConverter.class)
    private Condition condition;

    @Column(name = "Recommendation")
    private String recommendation;

    @OneToOne(cascade = CascadeType.MERGE)
    @JoinColumn(name = "Hospitalization_Id")
    private Hospitalization hospitalization;
}
