package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.diagnosis.DiagnosisDto;
import ua.lpnu.hospital.db.models.Diagnosis;

@Mapper
public interface DiagnosisMapper extends OneWayMapperDto<Diagnosis, DiagnosisDto> {
    DiagnosisMapper INSTANCE = Mappers.getMapper(DiagnosisMapper.class);
}
