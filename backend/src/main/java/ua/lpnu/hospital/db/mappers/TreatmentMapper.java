package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.treatment.TreatmentDto;
import ua.lpnu.hospital.db.models.Treatment;

@Mapper
public interface TreatmentMapper extends OneWayMapperDto<Treatment, TreatmentDto> {
    TreatmentMapper INSTANCE = Mappers.getMapper(TreatmentMapper.class);
}
