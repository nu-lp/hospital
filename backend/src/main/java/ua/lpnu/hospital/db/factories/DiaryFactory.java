package ua.lpnu.hospital.db.factories;

import org.springframework.stereotype.Component;
import ua.lpnu.hospital.api.dto.diary.CreateDiaryDto;
import ua.lpnu.hospital.db.models.Diary;
import ua.lpnu.hospital.db.models.DiaryTreatment;
import ua.lpnu.hospital.db.models.Hospitalization;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class DiaryFactory {
    public Diary create(Hospitalization hospitalization,
                        CreateDiaryDto dto) {
        Diary diary = new Diary();
        diary.setCondition(dto.getCondition());
        diary.setTemperature(dto.getTemperature());
        diary.setRespiratoryRate(dto.getRespiratoryRate());
        diary.setHeartRate(dto.getHeartRate());
        diary.setBloodPressureDia(dto.getBloodPressureDia());
        diary.setBloodPressureSys(dto.getBloodPressureSys());
        diary.setComplaints(dto.getComplaints());
        diary.setExamination(dto.getExamination());
        diary.setHospitalization(hospitalization);

        List<DiaryTreatment> treatments = dto.getTreatments().stream()
                .map(t -> {
                    DiaryTreatment diaryTreatment = new DiaryTreatment();
                    diaryTreatment.setDescription(t);
                    diaryTreatment.setDiary(diary);
                    return diaryTreatment;
                }).collect(Collectors.toList());
        diary.setTreatments(treatments);

        return diary;
    }
}
