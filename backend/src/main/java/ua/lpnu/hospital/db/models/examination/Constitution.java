package ua.lpnu.hospital.db.models.examination;

import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public enum Constitution {
    NORMOSTENIC("NOR"),
    HYPERSTENIC("HYP"),
    ASTHENIC("AST");

    private static final Map<String, Constitution> BY_CODE = new HashMap<>();

    static {
        for (Constitution h : values()) {
            BY_CODE.put(h.code, h);
        }
    }

    public final String code;

    public static Constitution valueOfCode(String code) {
        return BY_CODE.get(code);
    }
}
