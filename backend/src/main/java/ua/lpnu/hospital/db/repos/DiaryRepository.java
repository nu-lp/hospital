package ua.lpnu.hospital.db.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.lpnu.hospital.db.models.Diary;

@Repository
public interface DiaryRepository extends JpaRepository<Diary, Long> {
}
