package ua.lpnu.hospital.db.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Treatment")
public class Treatment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Prescribed")
    private String prescribed;

    @Column(name = "Medicines")
    private String medicines;

    @OneToOne
    @JoinColumn(name = "Hospitalization_Id")
    private Hospitalization hospitalization;
}
