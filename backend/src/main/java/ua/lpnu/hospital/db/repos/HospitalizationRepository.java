package ua.lpnu.hospital.db.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ua.lpnu.hospital.db.models.Hospitalization;

import java.util.Optional;

@Repository
public interface HospitalizationRepository extends JpaRepository<Hospitalization, Long> {
    Optional<Hospitalization> findByExtractDateIsNullAndPatient_UuidAndDoctor_Uuid(String patientUuid, String doctorUuid);

    Optional<Hospitalization> findByExtractDateIsNullAndPatient_Uuid(String patientUuid);

    Optional<Hospitalization> findByExtractDateIsNullAndUuidAndDoctor_Uuid(String uuid, String doctorUuid);

    Optional<Hospitalization> findByUuid(String uuid);
}
