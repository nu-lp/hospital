package ua.lpnu.hospital.db.factories;

import org.springframework.stereotype.Component;
import ua.lpnu.hospital.api.dto.diagnosis.CreateDiagnosisDto;
import ua.lpnu.hospital.db.models.Diagnosis;
import ua.lpnu.hospital.db.models.DiagnosisType;
import ua.lpnu.hospital.db.models.Hospitalization;

@Component
public class DiagnosisFactory {
    public Diagnosis create(Hospitalization hospitalization,
                            CreateDiagnosisDto dto,
                            DiagnosisType type) {
        Diagnosis diagnosis = new Diagnosis();
        diagnosis.setType(type);
        diagnosis.setDescription(dto.getDescription());
        diagnosis.setDate(dto.getDate());
        diagnosis.setMainDisease(dto.getMainDisease());
        diagnosis.setComplicationDisease(dto.getComplicationDisease());
        diagnosis.setConcomitantDisease(dto.getConcomitantDisease());
        diagnosis.setHospitalization(hospitalization);

        return diagnosis;
    }
}
