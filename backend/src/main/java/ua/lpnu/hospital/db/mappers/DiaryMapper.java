package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.diary.DiaryDto;
import ua.lpnu.hospital.db.models.Diary;

@Mapper(uses = DiaryTreatmentMapper.class)
public interface DiaryMapper extends OneWayMapperDto<Diary, DiaryDto> {
    DiaryMapper INSTANCE = Mappers.getMapper(DiaryMapper.class);
}
