package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.SpecializationDto;
import ua.lpnu.hospital.db.models.Specialization;

@Mapper
public interface SpecializationMapper extends OneWayMapperDto<Specialization, SpecializationDto> {
    SpecializationMapper INSTANCE = Mappers.getMapper(SpecializationMapper.class);
}
