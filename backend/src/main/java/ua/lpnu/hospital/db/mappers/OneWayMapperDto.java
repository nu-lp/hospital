package ua.lpnu.hospital.db.mappers;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public interface OneWayMapperDto<S, D> {
    D map(S from);

    default List<D> map(Collection<S> from) {
        return from.stream()
                .map(this::map)
                .collect(Collectors.toList());
    }
}
