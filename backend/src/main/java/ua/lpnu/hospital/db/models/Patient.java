package ua.lpnu.hospital.db.models;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Generated;
import org.hibernate.annotations.GenerationTime;
import ua.lpnu.hospital.db.models.address.Address;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@NoArgsConstructor
@Getter
@Setter
@Entity(name = "Patient")
public class Patient extends Person {
    @Column(name = "PatientCode")
    @Generated(GenerationTime.INSERT)
    private String uuid;

    @Column(name = "Birthday")
    private LocalDate birthday;

    @Column(name = "HomePhone")
    private String homePhone;

    @Column(name = "IsMale")
    private Boolean isMale;

    @ManyToOne
    @JoinColumn(name = "Address_Id")
    private Address address;

    @OneToMany(cascade = {CascadeType.ALL}, mappedBy = "patient", orphanRemoval = true)
    private List<Hospitalization> hospitalizations = new ArrayList<>();
}
