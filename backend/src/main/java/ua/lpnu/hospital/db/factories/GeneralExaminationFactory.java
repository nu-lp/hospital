package ua.lpnu.hospital.db.factories;

import org.springframework.stereotype.Component;
import ua.lpnu.hospital.api.dto.examination.GeneralExaminationDto;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.models.examination.GeneralExamination;

@Component
public class GeneralExaminationFactory {
    GeneralExamination create(Hospitalization hospitalization,
                              GeneralExaminationDto dto) {
        GeneralExamination ge = new GeneralExamination();
        ge.setCondition(dto.getCondition());
        ge.setConsciousness(dto.getConsciousness());
        ge.setPosition(dto.getPosition());
        ge.setIsBodyProportional(dto.getIsBodyProportional());
        ge.setConstitution(dto.getConstitution());
        ge.setHeight(dto.getHeight());
        ge.setWeight(dto.getWeight());
        ge.setTone(dto.getTone());
        ge.setTemperature(dto.getTemperature());
        ge.setHospitalization(hospitalization);

        return ge;
    }
}
