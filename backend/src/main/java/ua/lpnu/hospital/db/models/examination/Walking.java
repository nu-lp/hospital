package ua.lpnu.hospital.db.models.examination;

import lombok.AllArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@AllArgsConstructor
public enum Walking {
    FREE("FRE"),
    STIFF("STI"),
    SWINGING("SWI"),
    LIMPING("LIM"),
    ROOSTER("RST");

    private static final Map<String, Walking> BY_CODE = new HashMap<>();

    static {
        for (Walking h : values()) {
            BY_CODE.put(h.code, h);
        }
    }

    public final String code;

    public static Walking valueOfCode(String code) {
        return BY_CODE.get(code);
    }
}
