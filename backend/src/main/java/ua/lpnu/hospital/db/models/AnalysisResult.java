package ua.lpnu.hospital.db.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenerationTime;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "AnalysisResult")
public class AnalysisResult {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Description")
    private String description;

    @Column(name = "Conclusion")
    private String conclusion;

    @Column(name = "Date")
    @org.hibernate.annotations.Generated(GenerationTime.INSERT)
    private LocalDateTime date;

    @OneToOne
    @JoinColumn(name = "Analysis_Id")
    private Analysis analysis;

    @ManyToOne
    @JoinColumn(name = "Doctor_Id")
    private Doctor doctor;
}
