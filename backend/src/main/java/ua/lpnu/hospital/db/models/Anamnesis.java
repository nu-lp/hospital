package ua.lpnu.hospital.db.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ua.lpnu.hospital.utils.converters.AnamnesisTypeConverter;

import javax.persistence.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity(name = "Anamnesis")
public class Anamnesis {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "Id")
    private Long id;

    @Column(name = "Type")
    @Convert(converter = AnamnesisTypeConverter.class)
    private AnamnesisType type;

    @Column(name = "Description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "Hospitalization_Id")
    private Hospitalization hospitalization;
}
