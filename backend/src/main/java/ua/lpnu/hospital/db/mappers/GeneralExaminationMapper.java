package ua.lpnu.hospital.db.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import ua.lpnu.hospital.api.dto.examination.GeneralExaminationDto;
import ua.lpnu.hospital.db.models.examination.GeneralExamination;

@Mapper
public interface GeneralExaminationMapper extends OneWayMapperDto<GeneralExamination, GeneralExaminationDto> {
    GeneralExaminationMapper INSTANCE = Mappers.getMapper(GeneralExaminationMapper.class);
}
