package ua.lpnu.hospital.properties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@Getter
@AllArgsConstructor
@ConfigurationProperties("jwt")
@ConstructorBinding
public class JwtProperties {
    private final String secret;
    private final long validity;
    private final String prefix;
    private final String authHeader;
}
