package ua.lpnu.hospital.properties;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.ConstructorBinding;

@Getter
@AllArgsConstructor
@ConfigurationProperties(value = "main-admin")
@ConstructorBinding
public class UserProperties {
    private final String username;
    private final String password;
    private final String firstName;
    private final String middleName;
    private final String lastName;
    private final String phone;
}
