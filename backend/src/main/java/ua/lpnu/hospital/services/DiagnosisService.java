package ua.lpnu.hospital.services;

import org.springframework.transaction.annotation.Transactional;
import ua.lpnu.hospital.api.dto.diagnosis.CreateDiagnosisDto;
import ua.lpnu.hospital.api.dto.diagnosis.DiagnosisDto;

public interface DiagnosisService {
    @Transactional
    DiagnosisDto createFinalDiagnosis(String doctorUuid, CreateDiagnosisDto dto);
}
