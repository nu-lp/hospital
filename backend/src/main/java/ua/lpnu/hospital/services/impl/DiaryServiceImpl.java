package ua.lpnu.hospital.services.impl;

import org.springframework.stereotype.Service;
import ua.lpnu.hospital.api.dto.diary.CreateDiaryDto;
import ua.lpnu.hospital.api.dto.diary.DiaryDto;
import ua.lpnu.hospital.api.exceptions.HospitalizationNotFoundException;
import ua.lpnu.hospital.db.factories.DiaryFactory;
import ua.lpnu.hospital.db.mappers.DiaryMapper;
import ua.lpnu.hospital.db.models.Diary;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.repos.DiaryRepository;
import ua.lpnu.hospital.db.repos.HospitalizationRepository;
import ua.lpnu.hospital.services.DiaryService;

@Service
public class DiaryServiceImpl implements DiaryService {
    private final DiaryRepository repository;
    private final DiaryFactory factory;
    private final HospitalizationRepository hospitalizationRepository;

    public DiaryServiceImpl(DiaryRepository repository,
                            DiaryFactory factory,
                            HospitalizationRepository hospitalizationRepository) {
        this.repository = repository;
        this.factory = factory;
        this.hospitalizationRepository = hospitalizationRepository;
    }

    @Override
    public DiaryDto create(String doctorUuid, CreateDiaryDto dto) {
        Hospitalization hospitalization = hospitalizationRepository
                .findByExtractDateIsNullAndUuidAndDoctor_Uuid(dto.getHospitalizationUuid(), doctorUuid)
                .orElseThrow(HospitalizationNotFoundException::new);
        Diary diary = factory.create(hospitalization, dto);
        
        return DiaryMapper.INSTANCE.map(repository.save(diary));
    }
}
