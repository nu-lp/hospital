package ua.lpnu.hospital.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import ua.lpnu.hospital.api.dto.PageDto;
import ua.lpnu.hospital.api.dto.doctor.CreateDoctorDto;
import ua.lpnu.hospital.api.dto.doctor.EmployeeDoctorDto;
import ua.lpnu.hospital.api.dto.patient.PatientDto;
import ua.lpnu.hospital.api.exceptions.DoctorNotFoundException;
import ua.lpnu.hospital.db.factories.DoctorFactory;
import ua.lpnu.hospital.db.mappers.EmployeeDoctorMapper;
import ua.lpnu.hospital.db.mappers.PatientMapper;
import ua.lpnu.hospital.db.models.Doctor;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.models.Patient;
import ua.lpnu.hospital.db.models.auth.Role;
import ua.lpnu.hospital.db.models.auth.UserRole;
import ua.lpnu.hospital.db.repos.DoctorRepository;
import ua.lpnu.hospital.db.repos.RoleRepository;
import ua.lpnu.hospital.services.DoctorService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DoctorServiceImpl implements DoctorService {
    private final DoctorRepository repository;
    private final DoctorFactory factory;
    private final RoleRepository roleRepository;

    @Autowired
    public DoctorServiceImpl(DoctorRepository repository,
                             DoctorFactory factory, RoleRepository roleRepository) {
        this.repository = repository;
        this.factory = factory;
        this.roleRepository = roleRepository;
    }

    @Override
    public List<PatientDto> getPatients(String doctorUuid) {
        Doctor doctor = repository.findByUuid(doctorUuid)
                .orElseThrow(DoctorNotFoundException::new);
        List<Patient> patients = doctor.getHospitalizations().stream()
                .filter(h -> h.getExtractDate() == null)
                .map(Hospitalization::getPatient)
                .collect(Collectors.toList());

        return PatientMapper.INSTANCE.map(patients);
    }

    @Override
    public EmployeeDoctorDto createDoctor(CreateDoctorDto doctorDto) {
        Doctor doc = factory.create(doctorDto);

        return EmployeeDoctorMapper.INSTANCE.map(repository.save(doc));
    }

    @Override
    public PageDto<EmployeeDoctorDto> getAll(int pageIndex, int pageSize) {
        PageRequest r = PageRequest.of(pageIndex, pageSize);
        Page<Doctor> doctors = repository.findAllBy(r);

        return new PageDto<>(
                doctors.getTotalElements(),
                EmployeeDoctorMapper.INSTANCE.map(doctors.getContent())
        );
    }

    @Override
    public void changeMainDocStatus(String uuid, Boolean value) {
        Doctor doctor = repository.findByUuid(uuid)
                .orElseThrow(DoctorNotFoundException::new);
        List<Role> roles = doctor.getUser().getRoles();

        if (value) {
            Role mainDoc = roleRepository.findByName(UserRole.ROLE_MAIN_DOCTOR)
                    .orElseThrow();
            doctor.getUser().getRoles().add(mainDoc);
            roleRepository.save(mainDoc);
        } else {
            doctor.getUser().setRoles(
                    roles.stream()
                            .filter(r -> r.getName() != UserRole.ROLE_MAIN_DOCTOR)
                            .collect(Collectors.toList())
            );
            repository.save(doctor);
        }
    }

    @Override
    public void changeActive(String uuid, Boolean value) {
        Doctor doctor = repository.findByUuid(uuid)
                .orElseThrow(DoctorNotFoundException::new);
        doctor.getUser().setIsActive(value);
        repository.save(doctor);
    }
}
