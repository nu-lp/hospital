package ua.lpnu.hospital.services.impl;

import org.springframework.stereotype.Service;
import ua.lpnu.hospital.api.dto.epicrisis.CreateEpicrisisDto;
import ua.lpnu.hospital.api.dto.epicrisis.EpicrisisDto;
import ua.lpnu.hospital.api.exceptions.HospitalizationNotFoundException;
import ua.lpnu.hospital.db.factories.EpicrisisFactory;
import ua.lpnu.hospital.db.mappers.EpicrisisMapper;
import ua.lpnu.hospital.db.models.Epicrisis;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.repos.EpicrisisRepository;
import ua.lpnu.hospital.db.repos.HospitalizationRepository;
import ua.lpnu.hospital.services.EpicrisisService;

@Service
public class EpicrisisServiceImpl implements EpicrisisService {
    private final EpicrisisRepository repository;
    private final EpicrisisFactory factory;
    private final HospitalizationRepository hospitalizationRepository;

    public EpicrisisServiceImpl(EpicrisisRepository repository,
                                EpicrisisFactory factory,
                                HospitalizationRepository hospitalizationRepository) {
        this.repository = repository;
        this.factory = factory;
        this.hospitalizationRepository = hospitalizationRepository;
    }

    @Override
    public EpicrisisDto createEpicrisis(String doctorUuid, CreateEpicrisisDto dto) {
        Hospitalization hospitalization = hospitalizationRepository
                .findByExtractDateIsNullAndUuidAndDoctor_Uuid(dto.getHospitalizationUuid(), doctorUuid)
                .orElseThrow(HospitalizationNotFoundException::new);
        Epicrisis epicrisis = factory.create(hospitalization, dto);

        return EpicrisisMapper.INSTANCE.map(repository.save(epicrisis));
    }
}
