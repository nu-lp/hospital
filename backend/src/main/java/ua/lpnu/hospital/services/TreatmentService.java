package ua.lpnu.hospital.services;

import org.springframework.transaction.annotation.Transactional;
import ua.lpnu.hospital.api.dto.treatment.CreateTreatmentDto;
import ua.lpnu.hospital.api.dto.treatment.TreatmentDto;

public interface TreatmentService {
    @Transactional
    TreatmentDto create(String doctorUuid, CreateTreatmentDto dto);
}
