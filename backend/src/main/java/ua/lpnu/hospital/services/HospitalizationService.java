package ua.lpnu.hospital.services;

import org.springframework.transaction.annotation.Transactional;
import ua.lpnu.hospital.api.dto.hospitalization.CreateHospitalizationDto;
import ua.lpnu.hospital.api.dto.hospitalization.HospitalizationDto;

public interface HospitalizationService {
    @Transactional
    HospitalizationDto createHospitalization(String doctorUuid,
                                             CreateHospitalizationDto hospitalizationDto);

    byte[] generateReport(String uuid);
}
