package ua.lpnu.hospital.services.impl;

import org.springframework.stereotype.Service;
import ua.lpnu.hospital.api.dto.diagnosis.CreateDiagnosisDto;
import ua.lpnu.hospital.api.dto.diagnosis.DiagnosisDto;
import ua.lpnu.hospital.api.exceptions.HospitalizationNotFoundException;
import ua.lpnu.hospital.db.factories.DiagnosisFactory;
import ua.lpnu.hospital.db.mappers.DiagnosisMapper;
import ua.lpnu.hospital.db.models.Diagnosis;
import ua.lpnu.hospital.db.models.DiagnosisType;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.repos.DiagnosisRepository;
import ua.lpnu.hospital.db.repos.HospitalizationRepository;
import ua.lpnu.hospital.services.DiagnosisService;

@Service
public class DiagnosisServiceImpl implements DiagnosisService {
    private final HospitalizationRepository hospitalizationRepository;
    private final DiagnosisRepository repository;
    private final DiagnosisFactory factory;

    public DiagnosisServiceImpl(HospitalizationRepository hospitalizationRepository,
                                DiagnosisRepository repository,
                                DiagnosisFactory factory) {
        this.hospitalizationRepository = hospitalizationRepository;
        this.repository = repository;
        this.factory = factory;
    }

    @Override
    public DiagnosisDto createFinalDiagnosis(String doctorUuid, CreateDiagnosisDto dto) {
        Hospitalization hospitalization = hospitalizationRepository
                .findByExtractDateIsNullAndUuidAndDoctor_Uuid(dto.getHospitalizationUuid(), doctorUuid)
                .orElseThrow(HospitalizationNotFoundException::new);
        Diagnosis diagnosis = factory.create(hospitalization, dto, DiagnosisType.FINAL);

        return DiagnosisMapper.INSTANCE.map(repository.save(diagnosis));
    }
}
