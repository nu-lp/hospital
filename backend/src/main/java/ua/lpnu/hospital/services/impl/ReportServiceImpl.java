package ua.lpnu.hospital.services.impl;

import org.springframework.stereotype.Service;
import ua.lpnu.hospital.db.models.DiseasesInRegionView;
import ua.lpnu.hospital.db.repos.DiseasesInRegionRepository;
import ua.lpnu.hospital.services.ReportService;

import java.util.List;

@Service
public class ReportServiceImpl implements ReportService {
    private final DiseasesInRegionRepository diseasesInRegionRepository;

    public ReportServiceImpl(DiseasesInRegionRepository diseasesInRegionRepository) {
        this.diseasesInRegionRepository = diseasesInRegionRepository;
    }

    @Override
    public List<DiseasesInRegionView> getDiseasesInRegionReport() {
        return diseasesInRegionRepository.findAllByOrderByCountDesc();
    }
}
