package ua.lpnu.hospital.services;

import org.springframework.transaction.annotation.Transactional;
import ua.lpnu.hospital.api.dto.analysis.AnalysisDto;
import ua.lpnu.hospital.api.dto.hospitalization.HospitalizationDto;
import ua.lpnu.hospital.api.dto.patient.CreatePatientDto;
import ua.lpnu.hospital.api.dto.patient.PatientDto;
import ua.lpnu.hospital.api.dto.patient.UpdatePatientDto;

import java.util.List;

public interface PatientService {
    HospitalizationDto getHospitalization(String patientUuid, String doctorUuid);

    List<PatientDto> getAll();

    List<PatientDto> findAllByName(String lastName, String firstName, String middleName);

    List<HospitalizationDto> getHospitalizations(String patientUuid);

    @Transactional
    PatientDto updatePatient(UpdatePatientDto updatePatientDto);

    List<AnalysisDto> getPendingAnalyses(String patientUuid);

    @Transactional
    PatientDto createPatient(CreatePatientDto patientDto);

    PatientDto getByUuid(String uuid);

    @Transactional
    PatientDto updatePatient(CreatePatientDto patientDto, String uuid);
}
