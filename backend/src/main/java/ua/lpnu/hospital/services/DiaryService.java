package ua.lpnu.hospital.services;

import org.springframework.transaction.annotation.Transactional;
import ua.lpnu.hospital.api.dto.diary.CreateDiaryDto;
import ua.lpnu.hospital.api.dto.diary.DiaryDto;

public interface DiaryService {
    @Transactional
    DiaryDto create(String doctorUuid, CreateDiaryDto dto);
}
