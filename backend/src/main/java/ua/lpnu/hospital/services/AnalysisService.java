package ua.lpnu.hospital.services;

import org.springframework.transaction.annotation.Transactional;
import ua.lpnu.hospital.api.dto.analysis.AnalysisDto;
import ua.lpnu.hospital.api.dto.analysis.CreateAnalysisDto;
import ua.lpnu.hospital.api.dto.analysis.CreateAnalysisResultDto;

public interface AnalysisService {
    @Transactional
    AnalysisDto createAnalysis(String doctorUuid, CreateAnalysisDto dto);

    @Transactional
    AnalysisDto createResult(String doctorUuid, CreateAnalysisResultDto dto);
}
