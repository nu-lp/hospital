package ua.lpnu.hospital.services;

import org.springframework.transaction.annotation.Transactional;
import ua.lpnu.hospital.api.dto.PageDto;
import ua.lpnu.hospital.api.dto.doctor.CreateDoctorDto;
import ua.lpnu.hospital.api.dto.doctor.EmployeeDoctorDto;
import ua.lpnu.hospital.api.dto.patient.PatientDto;

import java.util.List;

public interface DoctorService {
    List<PatientDto> getPatients(String doctorUuid);

    @Transactional
    EmployeeDoctorDto createDoctor(CreateDoctorDto doctorDto);

    PageDto<EmployeeDoctorDto> getAll(int pageIndex, int pageSize);

    void changeMainDocStatus(String uuid, Boolean value);

    void changeActive(String uuid, Boolean value);
}
