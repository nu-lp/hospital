package ua.lpnu.hospital.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.lpnu.hospital.api.dto.AddressDto;
import ua.lpnu.hospital.db.models.address.Address;
import ua.lpnu.hospital.db.models.address.City;
import ua.lpnu.hospital.db.models.address.Street;
import ua.lpnu.hospital.db.repos.address.AddressRepository;
import ua.lpnu.hospital.db.repos.address.CityRepository;
import ua.lpnu.hospital.db.repos.address.StreetRepository;
import ua.lpnu.hospital.services.AddressService;

import java.util.Optional;

@Service
public class AddressServiceImpl implements AddressService {
    private final CityRepository cityRepository;
    private final StreetRepository streetRepository;
    private final AddressRepository addressRepository;

    @Autowired
    public AddressServiceImpl(CityRepository cityRepository,
                              StreetRepository streetRepository,
                              AddressRepository addressRepository) {
        this.cityRepository = cityRepository;
        this.streetRepository = streetRepository;
        this.addressRepository = addressRepository;
    }

    @Override
    public Address findOrCreateAddress(AddressDto addressDto) {
        City city = findOrCreateCity(addressDto.getCity());
        Street street = findOrCreateStreet(addressDto.getStreet(), city);
        Optional<Address> address = addressRepository.findByHouseNumberAndStreet(addressDto.getHouseNumber(), street);
        if (address.isEmpty()) {
            Address a = new Address();
            a.setStreet(street);
            a.setHouseNumber(addressDto.getHouseNumber());
            a.setZipCode(addressDto.getZipCode());

            return addressRepository.save(a);
        }

        return address.get();
    }

    private City findOrCreateCity(String cityName) {
        Optional<City> city = cityRepository.findByNameIgnoreCase(cityName);

        if (city.isEmpty()) {
            City c = new City();
            c.setName(cityName);

            return cityRepository.save(c);
        }

        return city.get();
    }

    private Street findOrCreateStreet(String streetName, City city) {
        Optional<Street> street = streetRepository.findByNameIgnoreCaseAndCity(streetName, city);

        if (street.isEmpty()) {
            Street s = new Street();
            s.setName(streetName);
            s.setCity(city);

            return streetRepository.save(s);

        }

        return street.get();
    }
}
