package ua.lpnu.hospital.services;

import org.springframework.transaction.annotation.Transactional;
import ua.lpnu.hospital.api.dto.epicrisis.CreateEpicrisisDto;
import ua.lpnu.hospital.api.dto.epicrisis.EpicrisisDto;

public interface EpicrisisService {
    @Transactional
    EpicrisisDto createEpicrisis(String doctorUuid, CreateEpicrisisDto dto);
}
