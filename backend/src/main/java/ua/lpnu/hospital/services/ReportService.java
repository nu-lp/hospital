package ua.lpnu.hospital.services;

import ua.lpnu.hospital.db.models.DiseasesInRegionView;

import java.util.List;

public interface ReportService {
    List<DiseasesInRegionView> getDiseasesInRegionReport();
}
