package ua.lpnu.hospital.services;

import ua.lpnu.hospital.api.dto.AddressDto;
import ua.lpnu.hospital.db.models.address.Address;

public interface AddressService {
    Address findOrCreateAddress(AddressDto addressDto);
}
