package ua.lpnu.hospital.services.impl;

import org.springframework.stereotype.Service;
import ua.lpnu.hospital.api.dto.treatment.CreateTreatmentDto;
import ua.lpnu.hospital.api.dto.treatment.TreatmentDto;
import ua.lpnu.hospital.api.exceptions.HospitalizationNotFoundException;
import ua.lpnu.hospital.db.factories.TreatmentFactory;
import ua.lpnu.hospital.db.mappers.TreatmentMapper;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.models.Treatment;
import ua.lpnu.hospital.db.repos.HospitalizationRepository;
import ua.lpnu.hospital.db.repos.TreatmentRepository;
import ua.lpnu.hospital.services.TreatmentService;

@Service
public class TreatmentServiceImpl implements TreatmentService {
    private final TreatmentRepository repository;
    private final TreatmentFactory factory;
    private final HospitalizationRepository hospitalizationRepository;

    public TreatmentServiceImpl(TreatmentRepository repository,
                                TreatmentFactory factory,
                                HospitalizationRepository hospitalizationRepository) {
        this.repository = repository;
        this.factory = factory;
        this.hospitalizationRepository = hospitalizationRepository;
    }

    @Override
    public TreatmentDto create(String doctorUuid, CreateTreatmentDto dto) {
        Hospitalization hospitalization = hospitalizationRepository
                .findByExtractDateIsNullAndUuidAndDoctor_Uuid(dto.getHospitalizationUuid(), doctorUuid)
                .orElseThrow(HospitalizationNotFoundException::new);
        Treatment treatment = factory.create(hospitalization, dto);

        return TreatmentMapper.INSTANCE.map(repository.save(treatment));
    }
}
