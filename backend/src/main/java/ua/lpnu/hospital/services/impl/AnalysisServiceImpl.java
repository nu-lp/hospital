package ua.lpnu.hospital.services.impl;

import org.springframework.stereotype.Service;
import ua.lpnu.hospital.api.dto.analysis.AnalysisDto;
import ua.lpnu.hospital.api.dto.analysis.CreateAnalysisDto;
import ua.lpnu.hospital.api.dto.analysis.CreateAnalysisResultDto;
import ua.lpnu.hospital.api.exceptions.AnalysisNotFoundException;
import ua.lpnu.hospital.api.exceptions.DoctorNotFoundException;
import ua.lpnu.hospital.api.exceptions.HospitalizationNotFoundException;
import ua.lpnu.hospital.db.factories.AnalysisFactory;
import ua.lpnu.hospital.db.mappers.AnalysisMapper;
import ua.lpnu.hospital.db.models.Analysis;
import ua.lpnu.hospital.db.models.AnalysisResult;
import ua.lpnu.hospital.db.models.Doctor;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.repos.AnalysisRepository;
import ua.lpnu.hospital.db.repos.DoctorRepository;
import ua.lpnu.hospital.db.repos.HospitalizationRepository;
import ua.lpnu.hospital.services.AnalysisService;

@Service
public class AnalysisServiceImpl implements AnalysisService {
    private final HospitalizationRepository hospitalizationRepository;
    private final DoctorRepository doctorRepository;
    private final AnalysisRepository repository;
    private final AnalysisFactory factory;

    public AnalysisServiceImpl(HospitalizationRepository hospitalizationRepository,
                               DoctorRepository doctorRepository,
                               AnalysisRepository repository,
                               AnalysisFactory factory) {
        this.hospitalizationRepository = hospitalizationRepository;
        this.doctorRepository = doctorRepository;
        this.repository = repository;
        this.factory = factory;
    }

    @Override
    public AnalysisDto createAnalysis(String doctorUuid, CreateAnalysisDto dto) {
        Hospitalization hospitalization = hospitalizationRepository
                .findByExtractDateIsNullAndUuidAndDoctor_Uuid(dto.getHospitalizationUuid(), doctorUuid)
                .orElseThrow(HospitalizationNotFoundException::new);
        Analysis analysis = factory.create(hospitalization, dto);

        return AnalysisMapper.INSTANCE.map(repository.save(analysis));
    }

    @Override
    public AnalysisDto createResult(String doctorUuid,
                                    CreateAnalysisResultDto dto) {
        Doctor doctor = doctorRepository.findByUuid(doctorUuid)
                .orElseThrow(DoctorNotFoundException::new);
        Analysis analysis = repository.findByUuidAndResultIsNull(dto.getAnalysisUuid())
                .orElseThrow(AnalysisNotFoundException::new);
        AnalysisResult result = factory.create(analysis, doctor, dto);
        analysis.setResult(result);

        return AnalysisMapper.INSTANCE.map(repository.save(analysis));
    }
}
