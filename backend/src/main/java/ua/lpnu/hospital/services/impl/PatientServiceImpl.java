package ua.lpnu.hospital.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.lpnu.hospital.api.dto.analysis.AnalysisDto;
import ua.lpnu.hospital.api.dto.hospitalization.HospitalizationDto;
import ua.lpnu.hospital.api.dto.patient.CreatePatientDto;
import ua.lpnu.hospital.api.dto.patient.PatientDto;
import ua.lpnu.hospital.api.dto.patient.UpdatePatientDto;
import ua.lpnu.hospital.api.exceptions.HospitalizationNotFoundException;
import ua.lpnu.hospital.api.exceptions.PatientNotFoundException;
import ua.lpnu.hospital.db.factories.PatientFactory;
import ua.lpnu.hospital.db.mappers.AnalysisMapper;
import ua.lpnu.hospital.db.mappers.HospitalizationMapper;
import ua.lpnu.hospital.db.mappers.PatientMapper;
import ua.lpnu.hospital.db.models.Analysis;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.models.Patient;
import ua.lpnu.hospital.db.repos.HospitalizationRepository;
import ua.lpnu.hospital.db.repos.PatientRepository;
import ua.lpnu.hospital.services.PatientService;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class PatientServiceImpl implements PatientService {
    private final PatientRepository repository;
    private final HospitalizationRepository hospitalizationRepository;
    private final PatientFactory factory;

    @Autowired
    public PatientServiceImpl(PatientRepository repository,
                              HospitalizationRepository hospitalizationRepository,
                              PatientFactory factory) {
        this.repository = repository;
        this.hospitalizationRepository = hospitalizationRepository;
        this.factory = factory;
    }

    @Override
    public HospitalizationDto getHospitalization(String patientUuid, String doctorUuid) {
        Hospitalization hospitalization = hospitalizationRepository
                .findByExtractDateIsNullAndPatient_UuidAndDoctor_Uuid(patientUuid, doctorUuid)
                .orElseThrow(HospitalizationNotFoundException::new);

        return HospitalizationMapper.INSTANCE.map(hospitalization);
    }

    @Override
    public List<PatientDto> getAll() {
        return PatientMapper.INSTANCE.map(repository.findAll());
    }

    @Override
    public List<PatientDto> findAllByName(String lastName, String firstName, String middleName) {
        if (lastName == null) {
            lastName = "";
        }
        if (firstName == null) {
            firstName = "";
        }
        if (middleName == null) {
            middleName = "";
        }
        return PatientMapper.INSTANCE.map(
                repository.findAllByFirstNameStartingWithIgnoreCaseAndMiddleNameStartingWithIgnoreCaseAndLastNameStartingWithIgnoreCase(
                        firstName,
                        middleName,
                        lastName
                )
        );
    }

    @Override
    public List<HospitalizationDto> getHospitalizations(String patientUuid) {
        Patient patient = repository.findByUuid(patientUuid)
                .orElseThrow(PatientNotFoundException::new);

        return HospitalizationMapper.INSTANCE.map(patient.getHospitalizations());
    }

    @Override
    public List<AnalysisDto> getPendingAnalyses(String patientUuid) {
        Hospitalization hospitalization = hospitalizationRepository.findByExtractDateIsNullAndPatient_Uuid(patientUuid)
                .orElseThrow(HospitalizationNotFoundException::new);
        List<Analysis> analyses = hospitalization.getAnalyses().stream()
                .filter(a -> a.getResult() == null)
                .collect(Collectors.toList());

        return AnalysisMapper.INSTANCE.map(analyses);
    }

    @Override
    public PatientDto createPatient(CreatePatientDto patientDto) {
        Patient patient = factory.create(patientDto);

        return PatientMapper.INSTANCE.map(repository.save(patient));
    }

    @Override
    public PatientDto updatePatient(UpdatePatientDto updatePatientDto) {
        Patient patient = repository.findByUuid(updatePatientDto.getUuid())
                .orElseThrow(PatientNotFoundException::new);

        patient = factory.update(patient, updatePatientDto);
        return PatientMapper.INSTANCE.map(repository.save(patient));
    }

    @Override
    public PatientDto getByUuid(String uuid) {
        Patient patient = repository
                .findByUuid(uuid)
                .orElseThrow(() -> new PatientNotFoundException(
                        String.format("No patient with UUID[%s]", uuid)
                ));

        return PatientMapper.INSTANCE.map(patient);
    }

    @Override
    public PatientDto updatePatient(CreatePatientDto patientDto, String uuid) {
        Patient existed = repository
                .findByUuid(uuid)
                .orElseThrow(() -> new PatientNotFoundException(
                        String.format("No patient with UUID[%s]", uuid)
                ));
        Patient patient = factory.create(patientDto);
        patient.setId(existed.getId());
        patient.setUuid(existed.getUuid());

        return PatientMapper.INSTANCE.map(repository.save(patient));
    }
}
