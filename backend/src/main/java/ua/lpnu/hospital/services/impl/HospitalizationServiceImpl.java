package ua.lpnu.hospital.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ua.lpnu.hospital.api.dto.hospitalization.CreateHospitalizationDto;
import ua.lpnu.hospital.api.dto.hospitalization.HospitalizationDto;
import ua.lpnu.hospital.api.exceptions.DoctorNotFoundException;
import ua.lpnu.hospital.api.exceptions.HospitalizationNotFoundException;
import ua.lpnu.hospital.api.exceptions.PatientNotFoundException;
import ua.lpnu.hospital.db.factories.HospitalizationFactory;
import ua.lpnu.hospital.db.mappers.HospitalizationMapper;
import ua.lpnu.hospital.db.models.Doctor;
import ua.lpnu.hospital.db.models.Hospitalization;
import ua.lpnu.hospital.db.models.Patient;
import ua.lpnu.hospital.db.repos.DoctorRepository;
import ua.lpnu.hospital.db.repos.HospitalizationRepository;
import ua.lpnu.hospital.db.repos.PatientRepository;
import ua.lpnu.hospital.services.HospitalizationService;
import ua.lpnu.hospital.utils.MedicineHistoryReport;

@Service
public class HospitalizationServiceImpl implements HospitalizationService {
    private final PatientRepository patientRepository;
    private final DoctorRepository doctorRepository;
    private final HospitalizationRepository repository;
    private final HospitalizationFactory factory;

    @Autowired
    public HospitalizationServiceImpl(PatientRepository patientRepository,
                                      DoctorRepository doctorRepository,
                                      HospitalizationRepository repository,
                                      HospitalizationFactory factory) {
        this.patientRepository = patientRepository;
        this.doctorRepository = doctorRepository;
        this.repository = repository;
        this.factory = factory;
    }

    @Override
    public HospitalizationDto createHospitalization(String doctorUuid,
                                                    CreateHospitalizationDto hospitalizationDto) {
        Patient patient = patientRepository.findByUuid(hospitalizationDto.getPatientUuid())
                .orElseThrow(PatientNotFoundException::new);
        Doctor doctor = doctorRepository.findByUuid(doctorUuid)
                .orElseThrow(DoctorNotFoundException::new);

        if (patient.getHospitalizations().stream().anyMatch(h -> h.getExtractDate() == null)) {
            throw new PatientNotFoundException();
        }

        Hospitalization hospitalization = factory.create(doctor, patient, hospitalizationDto);

        return HospitalizationMapper.INSTANCE.map(repository.save(hospitalization));
    }

    @Override
    public byte[] generateReport(String uuid) {
        Hospitalization hospitalization = repository.findByUuid(uuid)
                .orElseThrow(HospitalizationNotFoundException::new);
        return MedicineHistoryReport.generatePdf(hospitalization);
    }
}
