package ua.lpnu.hospital.dataload;

import com.github.javafaker.Faker;
import org.junit.jupiter.api.Test;

import java.util.Locale;

class FakerTest {
    @Test
    void faker() {
        Faker faker = new Faker(new Locale("uk"));

        for (int i = 0; i < 100; i++) {
            System.out.println(
                    faker.internet().password(6, 15)
            );
        }
    }
}
